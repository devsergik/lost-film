package com.mult.film.repositories

import com.mult.film.common.*
import com.mult.film.common.api.RestApi
import com.mult.film.data.db.DataBaseRoom
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import com.mult.film.data.db.model.user.FavoriteBase
import ru.gildor.coroutines.retrofit.Result
import ru.gildor.coroutines.retrofit.awaitResult


/**
 * Created by user on 19.09.2017.
 */
object EventsRepository {

    private val requestListeners = mutableListOf<RequestCompleteListener>()

    fun addRequestCompleteListener(listener: RequestCompleteListener) {
        requestListeners.add(listener)
    }

    val requestItemsCount = mutableMapOf(
            TAB_MULT to 0,
            "FAVORITE" to 0
    )


    fun changeLikeStatus(id: Int, liked: Boolean, callback: Action = {}, status : ((Boolean) -> Unit)) {

        RestApi.fav(if(liked) "add" else "delete", id)
                .map{
                    val result =
                            if (it.containsKey("result")) it["result"] as Boolean else false
                    if(result) {
                        val data_fav = DataBaseRoom.getInstance().favoritesUserDao()
                        data_fav.deleteFavUserID(id)
                        if (liked)
                            data_fav.insertFavUser(FavoriteBase(id))
                        UserParamsUpdate.update()
                    }
                    result
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    requestListeners.forEach {
                        it.onRequestComplete(1, id)
                    }
                    callback()
                    status(it)

                }, {status(false)
                    it.printStackTrace() })
    }


    suspend fun loadFromServerByKey(key: String, page: Int = 0) =
            RestApi
                    .getEventsByKey(key, page)
                    .awaitResult()
                    .let {
                        when (it) {
                            is Result.Ok -> {
                                requestItemsCount[key] = it.value.size
                                requestListeners.forEach {
                                    it.onRequestComplete()
                                }
                                it.value
                            }
                            is Result.Error -> {
                                requestItemsCount[key] = 0
                                requestListeners.forEach {
                                    it.onRequestComplete()
                                }
                                throw it.exception
                            }
                            is Result.Exception -> {
                                requestItemsCount[key] = 0
                                requestListeners.forEach {
                                    it.onRequestComplete()
                                }
                                throw it.exception
                            }
                        }
                    }

    fun loadFavoriteList() =
            RestApi.getFavorites()
                    .doOnSuccess {
                        requestItemsCount["FAVORITE"] = it.size
                        requestListeners.forEach {
                            it.onRequestComplete()
                        }
                    }
                    .doOnError {
                        requestItemsCount["FAVORITE"] = 0
                        requestListeners.forEach {
                            it.onRequestComplete()
                        }
                    }

    fun loadEventInfo(id: Int) = RestApi.getEvent(id)
}

interface RequestCompleteListener {
    fun onRequestComplete(request: Int = 0, eventID: Int = -1)
}