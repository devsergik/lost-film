package com.mult.film.subscriptions.view

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.mult.film.data.db.Event

/**
 * Created by user on 12.09.2017.
 */
@StateStrategyType(OneExecutionStateStrategy::class)
interface SubscriptionsView : MvpView {
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun setData(events: List<Event>)

    fun eventUnFollowSuccess(eventId: Int)
    fun eventUnFollowFailed(eventId: Int)
    fun emptyContent()
    fun loadFailed()

}