package com.mult.film.subscriptions.adapter

import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.mult.film.R
import com.mult.film.common.api.RestApi
import com.mult.film.common.glide.GlideApp
import com.mult.film.data.db.Event

/**
 * Created by user on 12.09.2017.
 */
class SubscriptionsAdapter(
        private val changeLikeState: (Int, Boolean) -> Unit,
        private val showDetails: (Int, String) -> Unit
) : RecyclerView.Adapter<SubscriptionsAdapter.EventViewHolder>() {
    private val items = mutableListOf<Event>()

    override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount() = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventViewHolder {
        return EventViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.item_subscription, parent, false)
        )
    }

    fun setData(data: List<Event>) {

        val diffCallBack = EventDiffCallback(items.map { it }, data)

        val diffResult = DiffUtil.calculateDiff(diffCallBack, true)

        items.clear()
        items.addAll(data)

        diffResult.dispatchUpdatesTo(this)

    }

    private inner class EventDiffCallback(val old: List<Event>, val new: List<Event>) : DiffUtil.Callback() {
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
                old[oldItemPosition].id == new[newItemPosition].id

        override fun getOldListSize() = old.size

        override fun getNewListSize() = new.size

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
                old[oldItemPosition].title == new[newItemPosition].title

        override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int)
                = "period"
    }

    inner class EventViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        internal val logo = view.findViewById<ImageView>(R.id.logo)
        internal val nameFilm = view.findViewById<TextView>(R.id.nameFilm)
        internal val category = view.findViewById<TextView>(R.id.category)
        val follow = view.findViewById<ImageView>(R.id.follow)
        val count_movies = view.findViewById<TextView>(R.id.count_movies)

        fun bind(event: Event) {
            event.let { item ->
                nameFilm.text = item.title
                category.text = item.category?.name

                val size_movies = item.movies?.size
                count_movies.visibility = if(size_movies!! > 1) View.VISIBLE else View.GONE
                count_movies.text = size_movies.toString()

                GlideApp.with(view.context)
                        .load(RestApi.base + "/mult/assets/images/"+ item.movies?.get(0) +"/0.png")
                        .error(R.drawable.shape_grey_rect)
                        .into(logo)

                follow.isActivated = true

                follow.setOnClickListener {
                    val checked = !follow.isActivated
                    changeLikeState(item.id.id, checked)
                }

                itemView.setOnClickListener { showDetails(item.id.id, item.title!!) }
            }
        }
    }

    fun removeEvent(eventId: Int) {
        val index = items.indexOfFirst { it.id.id == eventId }
        if (index != -1) {
            items.removeAt(index)
            notifyItemRemoved(index)
        }
    }

    fun getName(eventId: Int): String {
        return items.find { it.id.id == eventId }?.title ?: ""
    }
}