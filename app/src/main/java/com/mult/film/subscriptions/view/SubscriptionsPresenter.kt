package com.mult.film.subscriptions.view

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter

/**
 * Created by user on 12.09.2017.
 */
@InjectViewState
class SubscriptionsPresenter: MvpPresenter<SubscriptionsView>()