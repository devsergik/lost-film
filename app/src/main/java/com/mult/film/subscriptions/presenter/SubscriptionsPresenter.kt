package com.mult.film.subscriptions.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.mult.film.common.plusAssign
import com.mult.film.repositories.EventsRepository
import com.mult.film.repositories.RequestCompleteListener
import com.mult.film.subscriptions.view.SubscriptionsView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * Created by user on 12.09.2017.
 */
@InjectViewState
class SubscriptionsPresenter : MvpPresenter<SubscriptionsView>(), RequestCompleteListener {
    override fun onRequestComplete(request: Int, eventID: Int) {
        if (request != 0)
            loadFavoriteList()
    }

    private var disposables = CompositeDisposable()

    init {
        /*disposables += realm.where(Event::class.java)
                .equalTo("liked", true)
                .findAll()
                .asFlowable()
                .map { realm.copyFromRealm(it) }
                .subscribe({
                    viewState.setData(it)
                }, {
                    it.printStackTrace()
                })*/
        EventsRepository.addRequestCompleteListener(this)

    }

    override fun onDestroy() {
        disposables.dispose()
        super.onDestroy()
    }

    override fun attachView(view: SubscriptionsView?) {
        super.attachView(view)
        disposables = CompositeDisposable()
    }


    fun loadFavoriteList() {
        disposables += EventsRepository.loadFavoriteList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    viewState.setData(it)
                    if (it.isEmpty())
                        viewState.emptyContent()
                }, {
                    it.printStackTrace(); viewState.loadFailed()
                })
    }

    fun unFollow(id: Int) {
        EventsRepository.changeLikeStatus(id, false) {
            viewState.eventUnFollowSuccess(id)
        }
        /* RestApi.unFollow(id)
                 .subscribeOn(Schedulers.io())
                 .observeOn(AndroidSchedulers.mainThread())
                 .subscribe({
                     viewState.eventUnFollowSuccess(id)
                     Realm.getDefaultInstance().use { r ->
                         r.where(Event::class.java)
                                 .equalTo("id", id)
                                 .findFirst()
                                 ?.let { event ->
                                     r.executeTransaction {
                                         event.favorite = false
                                     }
                                 }
                     }
                 }, {
                     it.printStackTrace()
                     viewState.eventUnFollowFailed(id)
                 })*/
    }
}