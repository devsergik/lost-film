package com.mult.film.subscriptions.view

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.mult.film.R
import com.mult.film.account.activity.LoginEmailActivity
import com.mult.film.common.Reloadable
import com.mult.film.common.UserInfo
import com.mult.film.data.db.Event
import com.mult.film.subscriptions.adapter.SubscriptionsAdapter
import com.mult.film.subscriptions.presenter.SubscriptionsPresenter
import kotlinx.android.synthetic.main.fragment_subcriptions.*
import com.mult.film.eventDetails.view.EventDetailsActivity
import org.jetbrains.anko.support.v4.startActivity
import org.jetbrains.anko.support.v4.startActivityForResult
import org.jetbrains.anko.support.v4.toast

/**
 * Created by user on 12.09.2017.
 */
class SubscriptionsFragment : MvpAppCompatFragment(), SubscriptionsView, Reloadable {
    override fun reload() {
        tryLoad()
    }

    override fun eventUnFollowSuccess(eventId: Int) {
        adapter.removeEvent(eventId)
        //tryLoad()
    }

    override fun eventUnFollowFailed(eventId: Int) {
        toast("Не удалось отменить подписку на ${adapter.getName(eventId)}").show()
    }

    override fun emptyContent() {
        refresh.isRefreshing = false

        group.visibility = View.VISIBLE
        login.visibility = View.GONE
        drawable.visibility = View.VISIBLE

        title.text = getString(R.string.add_ico_to_favorite)
        description.text = getString(R.string.save_interesting_ico)
    }


    companion object {
        private val cache = makeInstance()

        fun getInstance() = cache

        private fun makeInstance(): Fragment = SubscriptionsFragment()

    }

    private val adapter = SubscriptionsAdapter(
            changeLikeState = { id, checked ->
                //EventsRepository.changeLikeStatus(id, liked = checked)
                presenter.unFollow(id)
            },
            showDetails = { id, title ->
                startActivity<EventDetailsActivity>(
                        "EVENT_ID" to id,
                        "EVENT_NAME" to (title ?: "")
                )
            }
    )

    @InjectPresenter
    lateinit var presenter: SubscriptionsPresenter

    override fun setData(events: List<Event>) {
        refresh.visibility = View.VISIBLE
        group.visibility = View.GONE
        drawable.visibility = View.GONE
        login.visibility = View.GONE

        refresh.isRefreshing = false

        adapter.setData(events)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
            inflater.inflate(R.layout.fragment_subcriptions, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        subscriptions.layoutManager = LinearLayoutManager(context)
        subscriptions.adapter = adapter

        //tryLoad()
        login.setOnClickListener {
            startActivityForResult<LoginEmailActivity>(222) //TODO social SocialLoginActivity
        }

        refresh.setOnRefreshListener {
            tryLoad()
        }
    }

    private fun tryLoad() {
        if (UserInfo.registered) {
            presenter.loadFavoriteList()
        } else {
            //presenter.loadFavoriteList()
            setData(emptyList())
            group.visibility = View.VISIBLE
            login.visibility = View.VISIBLE
            drawable.visibility = View.GONE

            title.text = getString(R.string.sign_in_or_register)
            description.text = getString(R.string.description_favorite_need_auth)
        }

    }

    override fun loadFailed() {
        refresh.isRefreshing = false
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 222 && resultCode == 333) {
            presenter.loadFavoriteList()
        }
    }

    override fun onResume() {
        super.onResume()
        tryLoad()
    }
}