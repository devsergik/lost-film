package com.mult.film.common

import com.chibatching.kotpref.KotprefModel
import com.chibatching.kotpref.enumpref.enumValuePref
import java.util.*

/**
 * Created by user on 09.10.2017.
 */
object UserInfo : KotprefModel() {
    /*
    *  Языковые пакеты
    * */
    var language by enumValuePref(UserLanguage.UNDEFINED)

    var registered by booleanPref(false)

    var id by intPref(-1)
    var token by stringPref()

    var bountyFilter by booleanPref(false)

    var name by stringPref()
    var email by stringPref()

    var tempFeedback by stringPref()

    var sawOnboarding by booleanPref(false)
    var needHighlightFilter by booleanPref(false)
    var oldFavoriteInfoAlreadyShowed by booleanPref(false)

    var installInfoSent by booleanPref(false)

    private var feedbackCompleted by booleanPref(false)
    private var feedbackLastShowTime by longPref()
    var appStartedCount by intPref()

    var feedbackUncompletedCount = 0

    var sysLocal by stringPref()

    fun feedbackCompleted() {
        feedbackCompleted = true
        feedbackLastShowTime = System.currentTimeMillis()
    }

    fun feedbackUnCompleted() {
        feedbackCompleted = false
        feedbackLastShowTime = System.currentTimeMillis()
    }

    private fun feedbackShowedToday(): Boolean {
        val lastShowMills = feedbackLastShowTime
        val lastShowTime = Date(lastShowMills.takeIf { it != 0L } ?: 0)
        val now = Date(System.currentTimeMillis())
        val lastCalender = Calendar.getInstance().apply { time = lastShowTime }
        val nowCalender = Calendar.getInstance().apply { time = now }

        return nowCalender.get(Calendar.DAY_OF_YEAR) == lastCalender.get(Calendar.DAY_OF_YEAR)
    }

    fun needToShowFeedbackDialog(): Boolean {
        appStartedCount++
        val byAppStarted = appStartedCount == (if (registered) 10 else 15)
        val isCompleted = feedbackCompleted

        val lastShowMills = feedbackLastShowTime
        val lastShowTime = Date(lastShowMills.takeIf { it != 0L } ?: System.currentTimeMillis())
        val now = Date(System.currentTimeMillis())
        val lastCalender = Calendar.getInstance().apply { time = lastShowTime }
        val nowCalender = Calendar.getInstance().apply { time = now }

        val dayPassed = nowCalender.get(Calendar.DAY_OF_YEAR) - lastCalender.get(Calendar.DAY_OF_YEAR)

        return ((byAppStarted && !feedbackShowedToday()) || (!isCompleted && dayPassed >= (if (registered) 30 else 14)))
                && feedbackUncompletedCount <= 5
    }

    var showAdditionalMenu by booleanPref(false)
    var additionalMenuName by stringPref()
    var additionalMenuTitle by stringPref()
    var additionalMenuLink by stringPref()
    var additionalMenuIcon by stringPref()

}

enum class UserLanguage(val full_name: String, val literal: String, val code: Int) {
    RUSSIAN("Russian","ru", 1),
    ENGLISH("English", "en",2),
    UNDEFINED("-", "-", 1)
}