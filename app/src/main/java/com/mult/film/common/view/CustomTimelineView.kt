package com.mult.film.common.view

import android.content.Context
import android.content.res.Resources
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.util.TypedValue
import com.github.vipulasri.timelineview.TimelineView
import com.mult.film.common.viewUtils.px

/**
 * Created by user on 23.11.2017.
 */
class CustomTimelineView(context: Context, attrs: AttributeSet) : TimelineView(context, attrs) {

    private val textDrawable = TextDrawable("")


    private fun initTextDrawable() {
        val pLeft = paddingLeft
        val pRight = paddingRight
        val pTop = paddingTop
        val pBottom = paddingBottom

        val width = width// Width of current custom view
        val height = height

        val cWidth = width - pLeft - pRight// Circle width
        val cHeight = height - pTop - pBottom

        val markSize = Math.min(24.px, Math.min(cWidth, cHeight))

        textDrawable.setBounds(pLeft, pTop, pLeft + markSize, pTop + markSize)

    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        textDrawable.draw(canvas)
        // canvas.drawText("LOL", 0f, 20f, paint)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        initTextDrawable()
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        initTextDrawable()
    }
}