package com.mult.film.common

import android.content.Context
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.content.pm.Signature
import android.util.Base64
import com.mult.film.app.MarketApp
import java.io.UnsupportedEncodingException

import java.security.MessageDigest
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

/**
 * Created by user on 10.10.2017.
 */

object Hex {
    fun e(k: String):String {
        var e= ""
        val h = k.trim { it <= ' ' }
        if (!h.isEmpty()) {
            try {
                val r = h.toByteArray(charset("UTF-8"))
                val j = Base64.encodeToString(r, Base64.DEFAULT)
                e = j
            } catch (e: UnsupportedEncodingException) {
                e.printStackTrace()
            }

        }
        return e
    }

    fun d(k: String): String {
        var e= ""
        val s = k.trim { it <= ' ' }
        if (!s.isEmpty()) {
            try {

                val d = Base64.decode(s, Base64.DEFAULT)
                val f = String(d, charset("UTF-8"))
                e = f
            } catch (e: UnsupportedEncodingException) {
                e.printStackTrace()
            }
        }
        return e
    }

}
