package com.mult.film.common.view

import android.content.res.Resources
import android.graphics.*
import android.graphics.Paint.Align
import android.graphics.drawable.Drawable
import android.util.TypedValue


/**
 * Created by user on 23.11.2017.
 */
class TextDrawable(var text: String) : Drawable() {
    val paint = Paint().apply {
        color = Color.WHITE
        isAntiAlias = true
        isFakeBoldText = true
        setShadowLayer(6f, 0f, 0f, Color.BLACK)
        style = Paint.Style.FILL
        textAlign = Paint.Align.CENTER
    }

    override fun draw(canvas: Canvas) {
        canvas.drawText(text, bounds.exactCenterX(), bounds.exactCenterY() + bounds.exactCenterY() / 2.5f, paint)
    }

    override fun setAlpha(alpha: Int) {
        paint.alpha = alpha
    }

    override fun setColorFilter(cf: ColorFilter?) {
        paint.colorFilter = cf
    }

    override fun getOpacity(): Int {
        return PixelFormat.TRANSLUCENT
    }

}