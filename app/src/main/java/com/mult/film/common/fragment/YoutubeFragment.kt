package com.mult.film.common.fragment

import android.os.Bundle
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import com.google.android.youtube.player.YouTubePlayerFragment
import com.google.android.youtube.player.YouTubePlayerSupportFragment
import com.mult.film.R

/**
 * Created by user on 25.10.2017.
 */
class YoutubeFragment : YouTubePlayerFragment(), YouTubePlayer.OnInitializedListener {

    private var player: YouTubePlayer? = null
    private var videoId = ""

    override fun onCreate(savedInstance: Bundle?) {
        super.onCreate(savedInstance)
        initialize(getString(R.string.youtube_api_key), this)
    }

    override fun onDestroy() {
        player?.release()
        super.onDestroy()
    }

    fun setVideoId(videoId: String?) {
        if (videoId != null && videoId != this.videoId) {
            this.videoId = videoId
            player?.cueVideo(videoId)
        }
    }

    fun pause() {
            player?.pause()
    }

    override fun onInitializationSuccess(provider: YouTubePlayer.Provider, p1ayer: YouTubePlayer?, restored: Boolean) {
        this.player = player
        player?.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_CUSTOM_LAYOUT)
        player?.setOnFullscreenListener(activity as YouTubePlayer.OnFullscreenListener)
        if (!restored && videoId.isNotEmpty()) {
            player?.cueVideo(videoId)
        }
    }

    override fun onInitializationFailure(provider: YouTubePlayer.Provider, result: YouTubeInitializationResult) {
        player = null
    }
}