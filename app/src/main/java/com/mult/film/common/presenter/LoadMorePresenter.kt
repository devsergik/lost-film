package com.mult.film.common.presenter

import com.arellomobile.mvp.MvpPresenter
import com.mult.film.common.view.LoadMoreView
import com.mult.film.data.models.ListResponse
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.*
import retrofit2.HttpException
import java.net.ProtocolException
import java.net.SocketException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

/**
 * Created by user on 04.10.2017.
 */
abstract class LoadMorePresenter<T, V : LoadMoreView<T>> : MvpPresenter<V>() {
    private var disposables = CompositeDisposable()
    protected var lastLoadedPage = -1
    private var maxPage = -1
    private var loadingStarted = false

    protected var currentScroll = 0

    fun loadData(page: Int = 0) {
        GlobalScope.launch(Dispatchers.IO) {
        if (page == 0) {
            //viewState.hideErrorFooter()
            withContext(Dispatchers.Main) {
            onFirstPage()
            }
        }
        viewState.showFooter()

        if (!loadingStarted) {
            loadingStarted = true

                try {
                    val response = loadDataByPage(page)
                    onResponseRetrive(response)

                    withContext(Dispatchers.Main){
                    viewState.hideFooter()
                    viewState.apply {
                        if (page > 0) appendData(response.items)
                        else setData(response.items)
                    }
                    }
                    lastLoadedPage = response.page!!
                    maxPage = response.pages!!
                    onDataRetrieve(response)
                    /*if (response.items.isNotEmpty() && false)
                        AmplitudeHelper.pagination(response.items.size, response.page + 1, whatIsIt)*/

                    loadingStarted = false
                } catch (exception: Exception) {
                    exception.printStackTrace()
                    withContext(Dispatchers.Main) {
                        when (exception) {
                            is HttpException -> when (exception.code()) {
                                500 -> {
                                    viewState.showErrorFooter()
                                    viewState.setServerError(true)
                                    onHttp500()
                                }
                                404 -> {
                                    viewState.hideFooter()
                                    onHttp404()
                                }
                                400 -> {
                                    emptyData()
                                    onHttp400()
                                }
                            }
                            is ProtocolException, is NoSuchElementException, is NullPointerException -> {
                                // 204 -> {
                                emptyData()
                                viewState.showEmptyContentError()
                                onProtocolException()
                                //}
                            }
                            is UnknownHostException, is SocketTimeoutException, is SocketException -> {
                                if (page in 0..1) {
                                    emptyData()
                                    viewState.internetConnectionError()
                                } else {
                                    viewState.setServerError(false)
                                    viewState.showErrorFooter()
                                }
                                onUnknownException()
                            }
                            else -> {
                                emptyData()
                                viewState.showEmptyContentError()
                                onProtocolException()
                            }
                        }
                        loadingStarted = false
                    }
                }
            }

        }
    }

    open protected fun onDataRetrieve(response: ListResponse<T>?) {

    }

    open protected fun onFirstPage() {

    }

    open protected fun onHttp400() {}
    open protected fun onHttp404() {}
    open protected fun onHttp500() {}
    open protected fun onProtocolException() {}
    open protected fun onUnknownException() {}


    open protected fun onResponseRetrive(response: ListResponse<T>) {

    }

    fun emptyData() {
        viewState.hideFooter()
        viewState.hideErrorFooter()
        viewState.setServerError(false)
        viewState.setData(emptyList())
    }


    override fun onDestroy() {
        disposables.dispose()
        super.onDestroy()
    }

    fun loadMore(page: Int) {
        if (page < maxPage) {
            loadData(page)
        }
    }

    fun reloadLastPage() {
        viewState.hideErrorFooter()
        viewState.hideError()
        loadData(lastLoadedPage)
    }

    fun reloadNextPage() {
        viewState.hideErrorFooter()
        viewState.hideError()
        loadData(lastLoadedPage + 1)
    }

    abstract fun updateDB(items: List<T>)

    abstract suspend protected fun loadDataByPage(page: Int): ListResponse<T>

    fun setScrollPosition(position: Int) {
        currentScroll = position
    }

    override fun attachView(view: V) {
        super.attachView(view)
        disposables = CompositeDisposable()
        // viewState.setScrollPosition(currentScroll)
    }
}