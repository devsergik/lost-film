package com.mult.film.common.customtabs

import android.annotation.TargetApi
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import com.mult.film.R
import com.mult.film.common.LanguageContextWrapper
import com.mult.film.common.UserInfo
import kotlinx.android.synthetic.main.activity_article.*
import java.net.MalformedURLException
import java.net.URL

/**
 * Created by user on 27.10.2017.
 */
class BrowserActivity : AppCompatActivity() {

    var link: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_article)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_close_white)
        toolbar.setNavigationOnClickListener { onBackPressed() }
        link = intent.getStringExtra("website")
        var btn_url = intent.getStringExtra("btnURL") //url ссылка кнопки
        // val website = "http://btcnew.ru/news/ref.php"
        // fromNotification = intent.getBooleanExtra(FROM_NOTIFICATION,false)

        if (intent.hasExtra("name"))
            supportActionBar?.title = intent.getStringExtra("name")

        //Загрузчик страницы
        var loading_bar = ProgressDialog(this)
        loading_bar.setCancelable(false)
        loading_bar.getWindow().setBackgroundDrawableResource(android.R.color.transparent)
        loading_bar.show()


        webView.settings.builtInZoomControls = true
        webView.settings.displayZoomControls = false
        webView.settings.javaScriptEnabled = true
        webView.settings.builtInZoomControls = true
        val extraHeaders = HashMap<String, String>()
        // extraHeaders.put("Referer", APP_REFERRER)
        supportActionBar?.title = getHost(link).takeIf { it != "www.similarweb.com" } ?: getString(R.string.syte_analytic)


        //Загружаем страницу
        webView.loadUrl(link, extraHeaders)
        webView.webViewClient = getClient(loading_bar)
        // proceedWeb.visibility = View.GONE4
        proceedWeb.visibility = if (btn_url == null) View.INVISIBLE else View.VISIBLE


        proceedWeb.setOnClickListener {
            loading_bar.show()
            link = btn_url
            supportActionBar?.title = getHost(btn_url).takeIf { it != "www.similarweb.com" } ?: getString(R.string.syte_analytic)
            webView.loadUrl(btn_url, extraHeaders)
        }
    }

    private fun getHost(link: String): String {
        return try {
            URL(link).host
        } catch (e: Exception) {
            ""
        }
    }



    private fun getClient(loading: ProgressDialog): WebViewClient {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return object : WebViewClient() {
                @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                    if (request?.url.toString().startsWith("tg:")) {
                        val intent = Intent(Intent.ACTION_VIEW, request?.url)
                        try {
                            startActivity(intent)
                        } catch (e: Exception) {
                        }
                        return true
                    } else
                        try {
                            val host = request?.url?.host
                            supportActionBar?.title = host
                            view?.loadUrl(request?.url?.toString())
                            return false
                        } catch (e: MalformedURLException) {

                        }
                    return super.shouldOverrideUrlLoading(view, request)
                }

                override fun onPageFinished(view: WebView?, url: String?) {
                    super.onPageFinished(view, url)
                    loading.dismiss()
                    webView?.loadUrl("javascript:document.body.style.margin=\"4%\"; void 0")
                }

                @TargetApi(android.os.Build.VERSION_CODES.M)
                override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError) {
                    Toast.makeText(this@BrowserActivity, R.string.server_dialog_message, Toast.LENGTH_SHORT).show()
                    loading.dismiss()
                    if (error.errorCode in arrayOf(ERROR_TIMEOUT)) {
                        errorTitle.visibility = View.VISIBLE
                        errorDescription.visibility = View.VISIBLE
                    } else if (error.errorCode == WebViewClient.ERROR_UNSUPPORTED_SCHEME &&
                            request?.url.toString().startsWith("tg:")) {
                        val intent = Intent(Intent.ACTION_VIEW, request?.url)

                        try {
                            startActivity(intent)
                        } catch (e: Exception) {
                        }


                    }
                }

                @SuppressWarnings("deprecation")
                override fun onReceivedError(view: WebView?, errorCode: Int, description: String?, failingUrl: String?) {

                    if (errorCode in arrayOf(ERROR_TIMEOUT)) {
                        errorTitle.visibility = View.VISIBLE
                        errorDescription.visibility = View.VISIBLE
                    }
                }
            }
        } else {
            return object : WebViewClient() {
                override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                    if (url.toString().startsWith("tg:")) {
                        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                        try {
                            startActivity(intent)
                        } catch (e: Exception) {
                        }
                        return true
                    } else
                        try {
                            supportActionBar?.title = URL(url).host
                            view?.loadUrl(url)
                            return false
                        } catch (e: MalformedURLException) {

                        }
                    return super.shouldOverrideUrlLoading(view, url)
                }

                override fun onPageFinished(view: WebView?, url: String?) {
                    loading.dismiss()
                    super.onPageFinished(view, url)
                    webView?.loadUrl("javascript:document.body.style.margin=\"4%\"; void 0")
                }

                @SuppressWarnings("deprecation")
                override fun onReceivedError(view: WebView?, errorCode: Int, description: String?, failingUrl: String?) {
                    Toast.makeText(this@BrowserActivity, R.string.server_dialog_message, Toast.LENGTH_SHORT).show()
                    loading.dismiss()
                    if (errorCode in arrayOf(ERROR_TIMEOUT)) {
                        errorTitle.visibility = View.VISIBLE
                        errorDescription.visibility = View.VISIBLE
                    } else if (errorCode == WebViewClient.ERROR_UNSUPPORTED_SCHEME &&
                            failingUrl.toString().startsWith("tg:")) {
                        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(failingUrl))

                        try {
                            startActivity(intent)
                        } catch (e: Exception) {
                        }
                    }
                }
            }
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_browser, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val int = Intent(Intent.ACTION_VIEW, Uri.parse(link))
        try {
            startActivity(int)
            return true
        } catch (e: Exception) {
            return false
        }
    }
}

