package com.mult.film.common

import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer

/**
 * Created by user on 25.10.2017.
 */
object YoutubeInitializedListener: YouTubePlayer.OnInitializedListener {
    override fun onInitializationSuccess(provider: YouTubePlayer.Provider, player: YouTubePlayer, bool: Boolean) {

    }

    override fun onInitializationFailure(provider: YouTubePlayer.Provider, errorReason: YouTubeInitializationResult) {

    }
}