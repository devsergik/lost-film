package com.mult.film.common

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.support.v4.app.NotificationCompat
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.mult.film.R
import com.mult.film.eventDetails.view.EventDetailsActivity

/**
 * Created by user on 10.10.2017.
 */

class MarketFirebaseMessagingService : FirebaseMessagingService() {
    override fun onMessageReceived(message: RemoteMessage?) {
        super.onMessageReceived(message)
        message?.let {
            Log.i("FireBase", message.toString())
            message.data.takeIf { it.containsKey("types") }
                    ?.let {
                        when (it["types"]) {
                            "alert" -> {
                                val i = Intent(this, EventDetailsActivity::class.java)
                                val eventId = it["eventId"]?.toInt() ?: -1
                                val icoName = if (it.containsKey("eventName"))
                                    it["eventName"] else ""


                                i.putExtra("EVENT_ID", eventId)
                                i.putExtra("EVENT_NAME", icoName)
                                i.putExtra("PAGE", 0)
                                i.putExtra("FROM_NOTIFICATION", true)
                                sendNotification(message.notification?.body ?: "", i)
                            }
                            "count" -> {
                                val i = Intent(this, EventDetailsActivity::class.java)
                                val eventId = it["eventId"]?.toInt() ?: -1
                                val icoName = if (it.containsKey("eventName"))
                                    it["eventName"] else ""


                                i.putExtra("EVENT_ID", eventId)
                                i.putExtra("EVENT_NAME", icoName)
                                i.putExtra("PAGE", 1)
                                i.putExtra("FROM_NOTIFICATION", true)
                                sendNotification(message.notification?.body ?: "", i)

                                /*val articleId = if (it.containsKey("articleId"))
                                    it["articleId"].toString().toInt() else 0

                                val id = if (it.containsKey("id"))
                                    it["id"].toString().toInt() else 0

                                val articleType = if (it.containsKey("types"))
                                    it["articleType"].toString() else "link"

                                val link = if (it.containsKey("link"))
                                    it["link"].toString() else ""

                                val articleSource = if (it.containsKey("articleSource"))
                                    it["articleSource"].toString() else ""

                                if (articleId != 0) {
                                    startActivity<ArticleActivity>(
                                            "article_id" to articleId,
                                            "article_type" to articleType,
                                            "article_link" to link,
                                            "article_source" to articleSource
                                    )
                                    val id = eventId
                                    AmplitudeHelper.newsItemTap(
                                            id,
                                            EventsRepository.lastLoadedEventName,
                                            link,
                                            articleType.capitalize(), "News"
                                    )
                                    RestApi.viewed(id)
                                }*/
                            }
                            else -> {
                                sendNotification(message.notification?.body ?: "", null)
                            }
                        }
                    }
        }

    }

    fun sendNotification(title: String, intent: Intent?) {


        val pendingIntent =
                if (intent != null)
                    PendingIntent.getActivity(
                            this,
                            0 /* Request code */,
                            intent, PendingIntent.FLAG_ONE_SHOT)
                else PendingIntent.getActivity(
                        this,
                        0,
                        Intent(), // add this
                        PendingIntent.FLAG_UPDATE_CURRENT)


        val channelId = "fcm_default_channel"
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setContentText(title)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build())
    }
}
