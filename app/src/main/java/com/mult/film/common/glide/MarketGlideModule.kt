package com.mult.film.common.glide

import android.content.Context
import com.bumptech.glide.Glide
import com.bumptech.glide.Registry
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule
import com.mult.film.data.db.Event
import java.io.InputStream

/**
 * Created by user on 13.11.2017.
 */
@GlideModule
class MarketGlideModule:AppGlideModule(){
    override fun registerComponents(context: Context, glide: Glide, registry: Registry) {
        registry?.append(Event::class.java, InputStream::class.java, EventModelLoader.Companion.Factory())
    }
}