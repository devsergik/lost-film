package com.mult.film.common.view

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.AppCompatImageView
import android.util.AttributeSet
import com.mult.film.R
import kotlin.properties.Delegates

/**
 * Created by user on 13.09.2017.
 */
class ScoreIndicator : AppCompatImageView {

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)


    private var colors: ScoreColors = ScoreColors.Normal()

    var isInverse: Boolean = false
        set(value) {
            field = value
            colors = when (field) {
                false -> ScoreColors.Normal()
                true -> ScoreColors.Inverse()
            }
            state = state
        }

    enum class State {
        TBA, LOW, MEDIUM, HIGH
    }

    var state: State by Delegates.observable(State.TBA) { _, _, new ->
        clearColorFilter()
        setColorFilter(ContextCompat.getColor(context, colors.getByState(new)), android.graphics.PorterDuff.Mode.MULTIPLY)
        invalidate()
    }

    val color: Int
        get() = colors.getByState(state)

    override fun toString() = when (state) {
        ScoreIndicator.State.LOW -> context.getString(R.string.indicator_low)
        ScoreIndicator.State.MEDIUM -> context.getString(R.string.indicator_medium)
        ScoreIndicator.State.HIGH -> context.getString(R.string.indicator_high)
        ScoreIndicator.State.TBA -> context.getString(R.string.indicator_tba)
    }

    private sealed class ScoreColors {
        protected val MEDIUM: Int
            get() = R.color.yellow

        protected val TBA: Int
            get() = R.color.grey

        protected abstract val LOW: Int
        protected abstract val HIGH: Int

        open fun getByState(state: State): Int {
            return when (state) {
                ScoreIndicator.State.LOW -> LOW
                ScoreIndicator.State.MEDIUM -> MEDIUM
                ScoreIndicator.State.HIGH -> HIGH
                ScoreIndicator.State.TBA -> TBA
            }
        }

        class Normal : ScoreColors() {
            override val LOW: Int
                get() = R.color.red

            override val HIGH: Int
                get() = R.color.green


        }

        class Inverse : ScoreColors() {
            override val LOW: Int
                get() = R.color.green
            override val HIGH: Int
                get() = R.color.red
        }
    }
}