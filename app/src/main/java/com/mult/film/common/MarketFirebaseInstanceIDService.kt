package com.mult.film.common

import android.util.Log
import com.appsflyer.AppsFlyerLib
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService

/**
 * Created by user on 10.10.2017.
 */

class MarketFirebaseInstanceIDService : FirebaseInstanceIdService() {
    override fun onTokenRefresh() {
        super.onTokenRefresh()
        Log.i("FireBaseToken", FirebaseInstanceId.getInstance().token)
        FirebaseInstanceId.getInstance()?.let { instance ->
            val token = instance.token
            AppsFlyerLib.getInstance().updateServerUninstallToken(applicationContext, token)
        }
    }
}
