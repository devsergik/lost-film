package com.mult.film.common

import android.net.Uri
import android.view.Gravity
import com.mult.film.account.models.AlertInfoItem
import com.mult.film.data.models.*
import com.mult.film.data.db.Event

/**
 * Created by user on 02.10.2017.
 */
sealed class ListItem {
    class AlertItem(val alert: AlertInfoItem) : ListItem()
    class TitleItem(val title: String, val align: Int = (Gravity.START)) : ListItem()

    class TitleWithButtonItem(
            val title: String,
            val align: Int = (Gravity.START),
            val buttonTitle: String,
            val buttonIcon: Uri,
            val action: Action = {}
    ) : ListItem()
    class EventItem(val event: Event) : ListItem()
    class FooterItem(var waitingMore: Boolean, var serverError: Boolean) : ListItem() {
        operator fun component1() = waitingMore
        operator fun component2() = serverError
    }
    class RatingTextInfoItem(val ratingInfo: String) : ListItem()
    class NewsItem(val newsItem: com.mult.film.data.models.NewsItem) : ListItem()
    class ReviewItem(val reviewItem: VideoModel) : ListItem()
}