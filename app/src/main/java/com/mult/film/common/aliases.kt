package com.mult.film.common

import android.view.View

/**
 * Created by user on 11.09.2017.
 */
typealias Action = () -> Unit
typealias ViewAction = (View) -> Unit
typealias ItemClickListener = (Int) -> Unit
