package com.mult.film.common

interface ParamsChanged {
    fun OnChanged()
}

object UserParamsUpdate {
    fun update() {
            for (l in listeners) {
                l.OnChanged()
            }
        }
    private val listeners = ArrayList<ParamsChanged>()

    fun addListener(l: ParamsChanged) {
        listeners.add(l)
    }
}