package com.mult.film.common.viewUtils

import android.view.View

/**
 * Created by user on 27.10.2017.
 */
class GroupHelper(vararg views: View) {

    val list = views

    var visibility: Int = 0
        set(value) {
            list.forEach { it.visibility = value }
            field = value
        }
}