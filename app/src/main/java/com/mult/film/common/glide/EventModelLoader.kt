package com.mult.film.common.glide

import com.bumptech.glide.load.Options
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.ModelLoader
import com.bumptech.glide.load.model.ModelLoaderFactory
import com.bumptech.glide.load.model.MultiModelLoaderFactory
import com.bumptech.glide.load.model.stream.BaseGlideUrlLoader
import com.mult.film.data.db.Event
import java.io.InputStream
import com.bumptech.glide.load.model.ModelCache



/**
 * Created by user on 13.11.2017.
 */
class EventModelLoader : BaseGlideUrlLoader<Event> {

    companion object {
        class Factory: ModelLoaderFactory<Event, InputStream> {
            private val modelCache = ModelCache<Event, GlideUrl>(500)


            override fun build(multiFactory: MultiModelLoaderFactory): ModelLoader<Event, InputStream> {
                return EventModelLoader(multiFactory.build(GlideUrl::class.java, InputStream::class.java), modelCache)
            }

            override fun teardown() {
            }
        }
    }

    constructor(concreteLoader: ModelLoader<GlideUrl, InputStream>?, modelCache: ModelCache<Event, GlideUrl>) : super(concreteLoader, modelCache)


    constructor(concreteLoader: ModelLoader<GlideUrl, InputStream>?) : super(concreteLoader)

    override fun handles(model: Event) = true
    override fun getUrl(model: Event?, width: Int, height: Int, options: Options?)
            = model?.id?.id.toString()

    override fun getAlternateUrls(model: Event?, width: Int, height: Int, options: Options?): MutableList<String> {
        return mutableListOf(model?.id?.id.toString())
    }
}