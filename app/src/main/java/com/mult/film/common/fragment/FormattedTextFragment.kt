package com.mult.film.common.fragment

import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.graphics.drawable.DrawableCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import com.mult.film.R
import com.mult.film.common.*
import com.mult.film.common.viewUtils.px
import kotlinx.android.synthetic.main.fragment_formatted_text.*


/**
 * Created by user on 04.10.2017.
 */
open class FormattedTextFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_formatted_text)
    }

    private var onViewCreated: List<(() -> Any)> = listOf()


    open fun setText(text: CharSequence) {
        onViewCreated += {
            /*formattedText.text = text
        formattedText.movementMethod = LinkMovementMethod.getInstance()*/
            //webView.settings.javaScriptEnabled = true
            webView.loadDataWithBaseURL("", text.toString(), "text/html", "UTF-8", "")

            /*webView.webViewClient = object : WebViewClient() {
                override fun onPageFinished(view: WebView, url: String) {
                    webView?.loadUrl("javascript:document.body.style.padding=\"4%\"; void 0")
                }
            }*/
            Unit
        }

        onViewCreated.forEach { it.tryInvoke() }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        webView.isVerticalScrollBarEnabled = false
        webView.isHorizontalScrollBarEnabled = false
        onViewCreated.forEach { it.tryInvoke() }
    }

    fun setSourceLink(link: String) {
        onViewCreated += {
            button.text = getString(R.string.open_bounty_source)
            context?.let { ctx ->
                ctx.setBackgroundTintWithCorners(button, ContextCompat.getColor(ctx, android.R.color.white))
            }

            button.visibility = View.VISIBLE
            bottomShadow.visibility = View.VISIBLE
            button.setOnClickListener {
                openSite(link,null)
            }
            Unit
        }

        onViewCreated.forEach { it.tryInvoke() }
    }
}