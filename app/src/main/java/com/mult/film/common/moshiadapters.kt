package com.mult.film.common

import com.mult.film.data.db.DataBaseRoom
import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonQualifier
import com.squareup.moshi.ToJson
import com.mult.film.data.db.model.settings.CategoriesBase
import com.mult.film.data.models.CategorySetting
import com.mult.film.data.models.ParamsModel

@Retention(AnnotationRetention.RUNTIME)
@JsonQualifier
annotation class CategoryParams

class CategoryAdapter {
    private val categoryMap: MutableList<CategoriesBase> = mutableListOf()

    init {
        val user_base = DataBaseRoom.getInstance()
        val category_list = user_base.categoriesDao().getAllCategories()
        categoryMap.clear()
        categoryMap.addAll(category_list)
    }

    @FromJson
    @CategoryParams
    fun fromJson(id: Int): CategorySetting {
        return categoryMap.find{it.id == id}.let { CategorySetting(it?.id,it?.name, it?.checked) }
    }


    @ToJson
    fun toJson(@CategoryParams category: CategorySetting): Int {
        return category.id!!
    }
}


@Retention(AnnotationRetention.RUNTIME)
@JsonQualifier
annotation class FavParams

class FavAdapter {
    private val fav_list: MutableList<Int> = mutableListOf()
    private val user_base = DataBaseRoom.getInstance()

    init {
        initParams()
        UserParamsUpdate.addListener(object : ParamsChanged {
            override fun OnChanged() {
                initParams()
            }
        })
    }

    fun initParams() {
        val fav_dao  = user_base.favoritesUserDao().getFavTypesAll()
        fav_list.clear()
        fav_list.addAll(fav_dao)
    }

    @FromJson
    @FavParams
    fun fromJson(id: Int): ParamsModel {
        val is_fav = fav_list.any { it == id }
        return ParamsModel(id,  is_fav)
    }



    @ToJson
    fun toJson(@FavParams paramsModel: ParamsModel): Int {
        return paramsModel.id
    }
}