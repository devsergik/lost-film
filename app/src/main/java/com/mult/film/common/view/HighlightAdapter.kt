package com.mult.film.common.view

import android.graphics.Color

/**
 * Created by user on 21.11.2017.
 */
interface HighlightAdapter{
    fun needHighlight(position: Int): Boolean?
    fun getHighlightColor(position: Int): Int = Color.WHITE
}