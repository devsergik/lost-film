package com.mult.film.common.view

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

/**
 * Created by user on 04.10.2017.
 */
@StateStrategyType(OneExecutionStateStrategy::class)
interface LoadMoreView<in T> : MvpView {
    @StateStrategyType(AddToEndSingleStrategy::class)
    fun appendData(data: List<T>)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun setData(data: List<T>)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showFooter()

    fun hideFooter()

    fun showErrorFooter()
    fun hideErrorFooter()

    fun setServerError(error: Boolean)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showEmptyContentError()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun internetConnectionError() {

    }

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun unknownException() {

    }

    fun hideError() {

    }

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun setScrollPosition(currentScroll: Int) {

    }
}