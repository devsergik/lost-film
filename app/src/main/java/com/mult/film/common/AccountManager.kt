package com.mult.film.common

import com.chibatching.kotpref.bulk
import com.mult.film.common.api.RestApi
import com.mult.film.data.db.DataBaseRoom
import com.mult.film.data.db.model.user.FavoriteBase

/**
 * Created by user on 23.11.2017.
 */
object AccountManager {

    private val signingListeners = mutableListOf<SigningListener>()


    fun addSigningListener(listener: SigningListener) {
        if (!signingListeners.contains(listener))
            signingListeners.add(listener)
    }

    fun removeSigningListener(listener: SigningListener) {
        signingListeners.remove(listener)
    }

    fun logout() =  RestApi.logout()

    fun register(email: String, password: String) =
            RestApi.register(email = email, password = password)
                    .doAfterSuccess {
                        signingListeners.forEach {
                            try {
                                it.proceed(true)
                            } catch (e: Exception) {
                            }
                        }
                    }

    fun auth(type: String = "", socialToken: String = "", socialSecret: String = "", email: String = "", password: String = "") =
            RestApi.auth(type, socialToken, socialSecret, email, password)
                    .map{
                        var id_user = 0
                        var token_response = ""
                        var email_response = ""

                        val isAuthorization = it.result!!
                        if (isAuthorization) {
                            id_user = it.user?.id!!
                            token_response = it.user.token!!
                            email_response = it.user.email!!
                            val user_dao = DataBaseRoom.getInstance()
                            val fav_user_dao = user_dao.favoritesUserDao()
                            fav_user_dao.deleteFavUser()
                            val fav = it.user.favorites?.map{
                                FavoriteBase(it)
                            }.orEmpty()
                            fav_user_dao.insertFavUser(fav)
                            UserParamsUpdate.update()
                        }

                        UserInfo.bulk {
                            registered = isAuthorization
                            id = id_user
                            this.token = token_response
                            this.email = email_response
                        }
                        it
                    }
                    .doAfterSuccess {
                        signingListeners.forEach {
                            try {
                                it.proceed(true)
                            } catch (e: Exception) {
                            }
                        }
                    }

}

interface SigningListener {
    fun proceed(registered: Boolean)
}