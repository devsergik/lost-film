package com.mult.film.common

import android.app.Activity
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.content.res.Resources
import android.graphics.*
import android.graphics.drawable.*
import android.net.Uri
import android.support.v4.app.Fragment
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.mult.film.R
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import java.util.*
import android.os.Build
import android.provider.Settings
import android.support.annotation.AnyRes
import android.support.v4.app.DialogFragment
import android.support.v4.view.MarginLayoutParamsCompat
import android.util.TypedValue
import android.widget.FrameLayout
import android.support.v4.app.FragmentManager
import android.support.v4.content.ContextCompat
import android.support.v4.graphics.ColorUtils
import android.support.v4.graphics.drawable.DrawableCompat
import android.support.v7.widget.RecyclerView
import android.text.SpannableStringBuilder
import android.util.Patterns
import android.util.StateSet
import android.webkit.URLUtil
import com.mult.film.BuildConfig
import com.mult.film.app.MarketApp
import com.mult.film.common.customtabs.BrowserActivity
import com.mult.film.common.viewUtils.px
import com.mult.film.dialogs.DialogFeedback
import com.mult.film.dialogs.DialogYesNo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.jetbrains.anko.*
import org.jetbrains.anko.db.asSequence
import java.net.URLEncoder
import java.security.MessageDigest
import java.security.Signature
import java.text.SimpleDateFormat
import java.util.concurrent.ConcurrentHashMap


/**
 * Created by user on 12.09.2017.
 */

operator fun CompositeDisposable.plusAssign(subscribe: Disposable?) {
    subscribe?.let { add(it) }
}

fun ViewGroup.inflate(layoutRes: Int): View {
    return context.layoutInflater.inflate(layoutRes, this, false)
}

fun <T> List<T>.shuffle(): List<T> {
    val result = mutableListOf<T>()
            .apply { addAll(this@shuffle) }
    Collections.shuffle(result)
    return result
}

interface Title {
    val title: String
}

interface Reloadable {
    fun reload()
}

const val DAY_RATIO = (24 * 60 * 60)
const val HOURS_RATIO = (60 * 60)
const val MINUTES_RATIO = 60
const val SECONDS_RATIO = 1
const val TIME_MASK = "%02d"

fun initTimer(begin: Int, end: Int): Array<String> {
    val now = (Calendar.getInstance().timeInMillis / 1000).toInt()
    val time: Int
    time = if (begin > now) begin else end
    val delta = time - now

    return getTimerM(delta)
}




fun initEndTimer(end: Long): Array<String> {
    val now = (System.currentTimeMillis() / 1000).toInt()
    val delta = end - now

    return getTimerM(delta.toInt())
}


fun getTimer(d: Int): Array<String> {

    var delta = d
    val days = delta / DAY_RATIO
    delta -= days * DAY_RATIO
    val hours = delta / HOURS_RATIO
    delta -= hours * HOURS_RATIO
    val minutes = delta / MINUTES_RATIO
    val seconds = delta - minutes * MINUTES_RATIO
    val sDays = if (days < 10) {
        String.format(TIME_MASK, days)
    } else {
        days.toString()
    }


    return arrayOf(sDays,
            String().format(TIME_MASK, hours),
            String().format(TIME_MASK, minutes),
            String().format(TIME_MASK, seconds))
}

fun Int.dig2Str() = if (this < 10) {
    "0$this"
} else {
    this.toString()
}

val digestMap = ConcurrentHashMap<Int, String>(mapOf(
        0 to "00",
        1 to "01",
        2 to "02",
        3 to "03",
        4 to "04",
        5 to "05",
        6 to "06",
        7 to "07",
        8 to "08",
        9 to "09",
        10 to "10",
        11 to "11",
        12 to "12",
        13 to "13",
        14 to "14",
        15 to "15",
        16 to "16",
        17 to "17",
        18 to "18",
        19 to "19",
        20 to "20",
        21 to "21",
        22 to "22",
        23 to "23",
        24 to "24",
        25 to "25",
        26 to "26",
        27 to "27",
        28 to "28",
        29 to "29",
        30 to "30",
        31 to "31",
        32 to "32",
        33 to "33",
        34 to "34",
        35 to "35",
        36 to "36",
        37 to "37",
        38 to "38",
        39 to "39",
        40 to "40",
        41 to "41",
        42 to "42",
        43 to "43",
        44 to "44",
        45 to "45",
        46 to "46",
        47 to "47",
        48 to "48",
        49 to "49",
        50 to "50",
        51 to "51",
        52 to "52",
        53 to "53",
        54 to "54",
        55 to "55",
        56 to "56",
        57 to "57",
        58 to "58",
        59 to "59"
))



fun getTimerM(d: Int): Array<String> {

    var delta = d
    val days = delta / DAY_RATIO
    delta -= days * DAY_RATIO
    val hours = delta / HOURS_RATIO
    delta -= hours * HOURS_RATIO
    val minutes = delta / MINUTES_RATIO
    val seconds = delta - minutes * MINUTES_RATIO
    val sDays = if (days < 60) {
        digestMap[days] ?: ""
    } else {
        days.toString()
    }

    return arrayOf(
            sDays,
            digestMap[hours] ?: "",
            digestMap[minutes] ?: "",
            digestMap[seconds] ?: ""
    )
}

fun Context.showUrlIntent(url: String) {
    val i = Intent(Intent.ACTION_VIEW)
    i.data = Uri.parse(url)
    if (i != null)
        startActivity(i)
}

fun Activity.sayNeedFinish() {
    val resultIntent = Intent()
    resultIntent.putExtra("some_key", "String data")
    setResult(Activity.RESULT_OK, resultIntent)
    finish()
}

fun Activity.sayNeedRecreate() {
    val resultIntent = Intent()
    resultIntent.putExtra("some_key", "String data")
    setResult(444, resultIntent)
    finish()
}

fun Activity.sayNeedReload() {
    val resultIntent = Intent()
    resultIntent.putExtra("some_key", "String data")
    setResult(333, resultIntent)
}

fun Context.shareText(shareBody: String) {
    val sharingIntent = Intent(android.content.Intent.ACTION_SEND)
    sharingIntent.type = "text/plain"
    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody)
    startActivity(Intent.createChooser(sharingIntent, resources.getString(R.string.share_ico)))
}

fun Context.openSite(link: String?, btnURL: String?) {
    if (!Patterns.WEB_URL.matcher(link).matches() || link == null) return

    if (link.endsWith(".pdf")) {
        val int = Intent(Intent.ACTION_VIEW, Uri.parse(link))
        try {
            startActivity(int)
        } catch (e: Exception) {
        }
    } else
        startActivity<BrowserActivity>(
                "website" to link,
                "btnURL" to btnURL
        )
}

fun isValidUrl(link: String?): Boolean {
    return Patterns.WEB_URL.matcher(link).matches()
}

fun Fragment.openSite(link: String?,btnURL: String?) {
    this.context?.openSite(link, btnURL)
}

fun <R> (() -> R)?.tryInvoke() {
    try {
        this?.invoke()
    } catch (t: Throwable) {
        Log.i("TIN", "Can't invoke method")
        t.printStackTrace()
    }
}

fun DialogFragment.showAllowingStateLoss(manager: FragmentManager, tag: String = "") {
    /*mDismissed = false
    mShownByMe = true
    */
    val ft = manager.beginTransaction()
    ft.add(this, tag)
    ft.commitAllowingStateLoss()
}

fun Context.checkScreenSize(rootView: ViewGroup, background: Int = Color.WHITE) {
    if (resources.configuration.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK == Configuration.SCREENLAYOUT_SIZE_LARGE ||
            resources.configuration.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK == Configuration.SCREENLAYOUT_SIZE_XLARGE) {
        val tv = TypedValue()
        val actionBarHeight = if (theme.resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            TypedValue.complexToDimensionPixelSize(tv.data, resources.displayMetrics)
        } else 0


        val display = (this as Activity).windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        val width = size.x

        val padding = (width - actionBarHeight * 8) / 2


        rootView.apply {
            val lp = layoutParams as ViewGroup.MarginLayoutParams
            MarginLayoutParamsCompat.setMarginEnd(lp, padding)
            MarginLayoutParamsCompat.setMarginStart(lp, padding)
            layoutParams = lp
            invalidate()
        }

        (rootView.parent as ViewGroup).background = ColorDrawable(background)
    }
}

val Context.windowWidth: Int
    get() {
        val display = windowManager.defaultDisplay
        val point = Point()
        display.getSize(point)
        return point.x
    }

fun View.setLayoutSize(width: Int, height: Int) {
    val params = layoutParams
    params.width = width
    params.height = height
    layoutParams = params
}

fun View.setLayoutSizeAndGravity(width: Int, height: Int, gravity: Int) {
    val params = layoutParams as FrameLayout.LayoutParams
    params.width = width
    params.height = height
    params.gravity = gravity
    layoutParams = params
}

/**
 * get uri to any resource types
 * @param this@getUriToResource - context
 * @param resId - resource id
 * @throws Resources.NotFoundException if the given ID does not exist.
 * @return - Uri to resource by given id
 */
fun Context.uriToResource(@AnyRes resId: Int): Uri {
    /** Return a Resources instance for your application's package. */
    val res = getResources()
    /**
     * Creates a Uri which parses the given encoded URI string.
     * @param uriString an RFC 2396-compliant, encoded URI
     * @throws NullPointerException if uriString is null
     * @return Uri for this given uri string
     */
    val resUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE +
            "://" + res.getResourcePackageName(resId)
            + '/' + res.getResourceTypeName(resId)
            + '/' + res.getResourceEntryName(resId))
    /** return uri */
    return resUri
}

fun FragmentManager.themedAlert(title: CharSequence? = null, applyFunc: DialogYesNo.() -> Unit = {}) {
    DialogYesNo().apply {
        setTitle(title.toString())
        applyFunc()
    }.show(this, "")
}

fun FragmentManager.feedbackDialog(applyFunc: DialogFeedback.() -> Unit = {}) {
    DialogFeedback().apply {
        applyFunc()
    }.show(this, "")
}

val TextView.light: Typeface
    get() = Typeface.createFromAsset(context.assets, "fonts/Roboto-Light.ttf")
val TextView.regular: Typeface
    get() = Typeface.createFromAsset(context.assets, "fonts/Roboto-Regular.ttf")
val TextView.medium: Typeface
    get() = Typeface.createFromAsset(context.assets, "fonts/Roboto-Medium.ttf")
val TextView.bold: Typeface
    get() = Typeface.createFromAsset(context.assets, "fonts/Roboto-Bold.ttf")

fun Context.setBackgroundTint(view: View, tint: Int) {
    if (Build.VERSION.SDK_INT <= 21)
        view.background = ContextCompat.getDrawable(this, R.drawable.background_bottom)
    view.background.colorFilter =
            PorterDuffColorFilter(tint, PorterDuff.Mode.MULTIPLY)
    DrawableCompat.setTint(view.background, tint)
}

fun Context.setBackgroundTintWithCorners(view: View, tint: Int) {
    if (Build.VERSION.SDK_INT <= 21) {
        view.background = ContextCompat.getDrawable(this, R.drawable.background_card)
        view.setPadding(24.px, 4.px, 24.px, 4.px)
    }
    view.background.colorFilter =
            PorterDuffColorFilter(tint, PorterDuff.Mode.MULTIPLY)
    DrawableCompat.setTint(view.background, tint)
}

fun Context.setBackgroundTintWithCornersSate(view: View, tint: Int) {
    if (Build.VERSION.SDK_INT < 21) {
        val background =
                ContextCompat.getDrawable(this, R.drawable.background_card)?.mutate()

        ((background as LayerDrawable).findDrawableByLayerId(R.id.content) as GradientDrawable)
                .setColor(tint)

        val backgroundPressed =
                ContextCompat.getDrawable(this, R.drawable.background_card)?.mutate()
        ((backgroundPressed as LayerDrawable).findDrawableByLayerId(R.id.content) as GradientDrawable)
                .setColor(darknessColor(tint))

        val stateDrawable = StateListDrawable()
        stateDrawable.addState(intArrayOf(android.R.attr.state_pressed), backgroundPressed)
        stateDrawable.addState(StateSet.WILD_CARD, background)

        view.background = stateDrawable

        view.setPadding(24.px, 4.px, 24.px, 4.px)
    } else
        DrawableCompat.setTint(view.background, tint)
}

fun darknessColor(color: Int): Int {
    val hsl = FloatArray(3)
    ColorUtils.colorToHSL(color ?: 0, hsl)

    if (hsl[2] - 0.07f >= 0.0)
        hsl[2] -= 0.07f
    else hsl[2] = 0.0f

    return ColorUtils.HSLToColor(hsl)
}


fun RecyclerView.ViewHolder.getString(resId: Int) = itemView.context.getString(resId)


fun getDeviceInfo(): String {
    return URLEncoder.encode(Build.BRAND + " " + Build.MODEL, "UTF-8")
}

fun getDeviceID() = Settings.Secure.getString(MarketApp.applicationContext().getContentResolver(), Settings.Secure.ANDROID_ID)
