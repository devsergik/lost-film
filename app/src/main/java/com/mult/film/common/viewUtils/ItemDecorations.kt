package com.mult.film.common.viewUtils

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View
import com.mult.film.common.view.HighlightAdapter
import org.jetbrains.anko.childrenSequence


/**
 * Created by user on 26.10.2017.
 */
/**
 * Created by user on 26.10.2017.
 */
class BackgroundItemDecoration : RecyclerView.ItemDecoration() {
    private val mPaint = Paint().apply {
        color = Color.parseColor("#00c853")
    }

    override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        c.save()
        parent.childrenSequence().forEach { child ->
            val position = parent.getChildAdapterPosition(child)
            if (parent.adapter is HighlightAdapter) {
                val adapter = parent.adapter as HighlightAdapter
                if (adapter.needHighlight(position)!!) {
                    mPaint.color = adapter.getHighlightColor(position)
                    val bounds = Rect()
                    parent.getDecoratedBoundsWithMargins(child, bounds)
                    c.drawRect(bounds, mPaint)
                }
            }
        }
        c.restore()
    }

}

/*
class StickyFooterItemDecoration : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        val adapterItemCount = parent.adapter?.itemCount
        if (isFooter(parent, view, adapterItemCount)) {
            if (view.height == 0 && state.didStructureChange()) {
                hideFooterAndUpdate(outRect, view, parent)
            } else {
                outRect.set(0, calculateTopOffset(parent, view, adapterItemCount), 0, 0)
            }
        }
    }

    private fun hideFooterAndUpdate(outRect: Rect, footerView: View, parent: RecyclerView) {
        outRect.set(0, OFF_SCREEN_OFFSET, 0, 0)
        footerView.post { parent.adapter.notifyDataSetChanged() }
    }

    private fun calculateTopOffset(parent: RecyclerView, footerView: View, itemCount: Int): Int {
        val topOffset = parent.height - visibleChildsHeightWithFooter(parent, footerView, itemCount)
        return if (topOffset < 0) 0 else topOffset
    }

    private fun visibleChildsHeightWithFooter(parent: RecyclerView, footerView: View, itemCount: Int): Int {
        var totalHeight = 0
        val onScreenItemCount = Math.min(parent.childCount, itemCount)
        for (i in 0 until onScreenItemCount - 1) {
            totalHeight += parent.getChildAt(i).height
        }
        return totalHeight + footerView.getHeight()
    }

    private fun isFooter(parent: RecyclerView, view: View, itemCount: Int): Boolean {
        return parent.getChildAdapterPosition(view) == itemCount - 1
    }

    companion object {

        private val OFF_SCREEN_OFFSET = 5000
    }
}*/
