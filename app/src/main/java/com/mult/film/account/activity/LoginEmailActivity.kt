package com.mult.film.account.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Patterns
import com.mult.film.R
import com.mult.film.common.*
import com.mult.film.dialogs.DialogPreloader
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_login_email.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.startActivityForResult
import org.jetbrains.anko.toast

/**
 * Created by user on 10.10.2017.
 */

class LoginEmailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_email)

        emailRegistration.setOnClickListener {
            startActivityForResult<RegistrationEmailActivity>(666)
            sayNeedFinish()
        }

        forgotPassword.setOnClickListener {
            startActivity<ResetPasswordActivity>()
            sayNeedFinish()
        }

        email.setCompoundDrawablesWithIntrinsicBounds(
                ContextCompat.getDrawable(this, R.drawable.ic_mail),
                null, null, null
        )
        password.setCompoundDrawablesWithIntrinsicBounds(
                ContextCompat.getDrawable(this, R.drawable.ic_lock_outline),
                null, null, null
        )

        setBackgroundTintWithCornersSate(apply, ContextCompat.getColor(this, android.R.color.white))

        apply.setOnClickListener {

            if (email.text?.isEmpty()!! || !Patterns.EMAIL_ADDRESS.matcher(email.text).matches()) {
                toast(getString(R.string.correct_address_required))
                return@setOnClickListener
            }

            if (password.text?.isEmpty()!!) {
                toast(getString(R.string.password_required))
                return@setOnClickListener
            }

            apply.isEnabled = false

            clickApplyListener()
        }


/*        socialLogin.setOnClickListener { TODO Social
            startActivity<SocialLoginActivity>()
            sayNeedFinish()
        }*/

        cancel.setOnClickListener { onBackPressed() }
    }



fun clickApplyListener() {
    GlobalScope.launch(Dispatchers.IO) {
        val preloader = DialogPreloader()
        preloader.show(supportFragmentManager, "")
        AccountManager.auth(type = "app", email = email.text.toString(), password = password.text.toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doAfterTerminate{
                    apply.isEnabled = true
                    preloader.dismissAllowingStateLoss()
                }
                .subscribe({
                    if (it.result!!) {
                        sayNeedFinish()
                        finish()
                    } else {
                        val message_error = if (it?.message?.isEmpty()!!) getString(R.string.error_auth) else it.message
                        toast(message_error)
                    }
                    apply.isEnabled = true
                    preloader.dismissAllowingStateLoss()
                }, {
                    toast(getString(R.string.error_auth))
                    it.printStackTrace()
                })
    }
}

override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    super.onActivityResult(requestCode, resultCode, data)
    if (requestCode == 666) {
        if (resultCode == Activity.RESULT_OK) {
            sayNeedFinish()
            finish()
        }
    }
}


}
