package com.mult.film.account.delegates

/**
 * Created by user on 29.09.2017.
 */
interface CommonItemsDelegate {
    fun aboutUs()
    fun offerIco()
    fun rateApp()
    fun feedback()
    fun disclaimer()
    fun privacyPolicy()
    fun advertising()
    fun additional()
}