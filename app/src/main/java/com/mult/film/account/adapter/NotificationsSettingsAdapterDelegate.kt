package com.mult.film.account.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import com.mult.film.R
import com.mult.film.account.models.AlertInfoItem
import com.mult.film.common.ListItem
import com.mult.film.common.glide.GlideApp
import kotlinx.android.synthetic.main.item_notification_setting.view.*
import org.jetbrains.anko.layoutInflater

/**
 * Created by user on 02.10.2017.
 */
class NotificationsSettingsAdapterDelegate(private val itemClickListener: (Int) -> Unit) : AdapterDelegate<MutableList<ListItem>>() {
    override fun isForViewType(items: MutableList<ListItem>, position: Int)
            = items[position] is ListItem.AlertItem

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return AlertViewHolder( parent.context.layoutInflater.inflate(R.layout.item_notification_setting, parent, false), itemClickListener)
    }

    override fun onBindViewHolder(items: MutableList<ListItem>, position: Int, holder: RecyclerView.ViewHolder, payloads: MutableList<Any>)
            = (holder as AlertViewHolder).bind((items[position] as ListItem.AlertItem).alert)

    private class AlertViewHolder(val view: View, val itemClickListener: (Int) -> Unit) : RecyclerView.ViewHolder(view) {
        private lateinit var alert: AlertInfoItem

        fun bind(alert: AlertInfoItem) {
            this.alert = alert

            view.setOnClickListener {
                itemClickListener(alert.eventID)
            }

            view.name.text = alert.eventName

            GlideApp.with(view.context)
                    .load(alert.logoLink)
                    .into(view.logo)
        }
    }
}