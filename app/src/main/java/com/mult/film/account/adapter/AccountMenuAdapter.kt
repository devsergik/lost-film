package com.mult.film.account.adapter

import android.graphics.drawable.ColorDrawable
import android.support.annotation.DrawableRes
import android.support.annotation.StringRes
import android.support.v4.content.ContextCompat
import android.support.v4.view.MarginLayoutParamsCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.mult.film.R
import com.mult.film.account.delegates.AuthenticatedDelegate
import com.mult.film.account.delegates.CommonItemsDelegate
import com.mult.film.account.delegates.UnauthenticatedDelegate
import com.mult.film.common.Action
import com.mult.film.common.UserInfo
import com.mult.film.common.glide.GlideApp
import com.mult.film.common.inflate
import com.mult.film.common.viewUtils.px
import org.jetbrains.anko.find
import org.jetbrains.anko.textSizeDimen

/**
 * Created by user on 28.09.2017.
 */
class AccountMenuAdapter(
        commonItemsDelegate: CommonItemsDelegate,
        authenticatedDelegate: AuthenticatedDelegate,
        unauthenticatedDelegate: UnauthenticatedDelegate, authenticated: Boolean = false)
    : RecyclerView.Adapter<AccountMenuAdapter.ViewHolder>(),
        CommonItemsDelegate by commonItemsDelegate,
        AuthenticatedDelegate by authenticatedDelegate,
        UnauthenticatedDelegate by unauthenticatedDelegate {

    private val commonItems = mutableListOf(
            AccountMenuItem.TextDivider(R.string.contacts_colon),
            AccountMenuItem.Divider(),
            AccountMenuItem.AboutUs(this::aboutUs),
            AccountMenuItem.Feedback(this::feedback),
            AccountMenuItem.RateApp(this::rateApp),
            AccountMenuItem.PrivacyPolicy(this::privacyPolicy),
            AccountMenuItem.Disclaimer(this::disclaimer)
    )

    private val authenticatedItems = listOf(
            AccountMenuItem.TextDivider(R.string.account_settings,
                    UserInfo.email.takeIf { it.isNotEmpty() } ?: UserInfo.name),
            /*AccountMenuItem.Language(this::language),*/
            AccountMenuItem.Logout(this::logout)
    )

    private val unauthenticatedItems = listOf(
            AccountMenuItem.TextDivider(R.string.account_settings),
            /*AccountMenuItem.Language(this::language),*/
            AccountMenuItem.Login(this::login),
            AccountMenuItem.PrivacyPolicy(this::privacyPolicy)
    )

    private val menuItems = (if (authenticated) authenticatedItems else unauthenticatedItems) + //commonItems +
            if (UserInfo.showAdditionalMenu) listOf<AccountMenuAdapter.AccountMenuItem>(AccountMenuItem.Additional(this::additional)) else listOf()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_account_menu, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(menuItems[position])
    }

    override fun getItemCount(): Int {
        return menuItems.size
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val title = itemView.find<TextView>(R.id.title)
        val icon = itemView.find<ImageView>(R.id.icon)

        fun bind(item: AccountMenuItem) {
            when (item) {
                is AccountMenuItem.TextDivider -> {
                    title.text = if (item.userInfo.isNotEmpty()) {
                        itemView.context.getString(R.string.signed_in_as) + " " + item.userInfo
                    } else
                        itemView.context.getText(item.title)

                    title.textSizeDimen = R.dimen.subtitle_size

                    title.apply {
                        setPadding(0, 16.px, 0, 16.px)

                        val lp = layoutParams as ViewGroup.MarginLayoutParams
                        MarginLayoutParamsCompat.setMarginEnd(lp, 0)
                        MarginLayoutParamsCompat.setMarginStart(lp, 0)
                        layoutParams = lp
                    }

                    icon.visibility = View.GONE
                    itemView.background = ColorDrawable(ContextCompat.getColor(itemView.context, R.color.menu_text_divider))
                }
                !is AccountMenuItem.Divider -> {
                    title.text = itemView.context.getText(item.title)

                    icon.setImageDrawable(ContextCompat.getDrawable(itemView.context, item.icon))
                    itemView.setOnClickListener { item.onClick() }

                    if (item is AccountMenuItem.Additional) {
                        title.text = UserInfo.additionalMenuName
                        GlideApp.with(itemView.context)
                                .load(UserInfo.additionalMenuIcon)
                                .into(icon)
                    }
                }
                else -> {
                    itemView.layoutParams = RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, 1.px)
                    itemView.background = ColorDrawable(ContextCompat.getColor(itemView.context, R.color.menu_divider))
                }
            }
        }
    }

    sealed class AccountMenuItem(
            @StringRes val title: Int,
            @DrawableRes val icon: Int,
            val onClick: () -> Unit
    ) {

        class Logout(onClick: () -> Unit) : AccountMenuItem(
                R.string.logout,
                R.drawable.ic_exit,
                onClick
        )

        class Login(onClick: () -> Unit) : AccountMenuItem(
                R.string.login,
                R.drawable.ic_exit,
                onClick
        )

        class Divider : AccountMenuItem(-1, -1, {})

        class TextDivider(title: Int, val userInfo: String = "") : AccountMenuItem(title, -1, {})


        class AboutUs(onClick: () -> Unit) : AccountMenuItem(
                R.string.about_us,
                R.drawable.ic_about,
                onClick
        )

        class Additional(action: Action) : AccountMenuItem(
                R.string.about_us,
                R.drawable.ic_follow_on,
                action
        )

       class RateApp(onClick: () -> Unit) : AccountMenuItem(
                R.string.rate_app,
                R.drawable.ic_rate_app,
                onClick
        )

        class Feedback(onClick: () -> Unit) : AccountMenuItem(
                R.string.feedback,
                R.drawable.ic_mail,
                onClick
        )

        class Disclaimer(onClick: () -> Unit) : AccountMenuItem(
                R.string.disclaimer,
                R.drawable.ic_disclaimer,
                onClick
        )

        class PrivacyPolicy(onClick: () -> Unit) : AccountMenuItem(
                R.string.privacy_policy,
                R.drawable.ic_lock_outline,
                onClick
        )

    }

}
