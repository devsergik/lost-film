package com.mult.film.account.delegates

/**
 * Created by user on 02.10.2017.
 */
interface UnauthenticatedDelegate {
    fun login()
}