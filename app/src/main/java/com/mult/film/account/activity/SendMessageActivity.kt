package com.mult.film.account.activity

import android.content.Context
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Patterns
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.mult.film.R
import com.mult.film.common.*
import com.mult.film.common.api.RestApi
import com.mult.film.common.viewUtils.GroupHelper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_send_message.*
import org.jetbrains.anko.toast

/**
 * Created by user on 26.09.2017.
 */
class SendMessageActivity : AppCompatActivity() {

    var type = "feedback"
    var eventId = -1


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_send_message)

        val group = GroupHelper(info_label, send, messageInputLayout,
                emailInputLayout, icon_message, icon_email, icon_subject,
                subjectSpinner, fake_line, fake_subject_hint,
                icon_phone, phoneInputLayout)
        val successResult = GroupHelper(senResult, thanks)

        type = intent.getStringExtra("TYPE") ?: "other"
        eventId = intent.getIntExtra("EVENT_ID", -1)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        toolbar.title = when (type) {
            "sendErrorIco" -> getString(R.string.title_report_mistake)
            "scamTV" -> getString(R.string.title_report_scam)
            "addNews" -> getString(R.string.title_suggest_news)
            "addVideo" -> getString(R.string.title_suggest_video)
            else -> getString(R.string.feedback)
        }

        info_label.text = when (type) {
            "sendErrorIco" -> getString(R.string.label_info_report_mistake)
            "scamTV" -> getString(R.string.label_info_report_scam)
            "addNews" -> getString(R.string.label_info_suggest_news)
            "addVideo" -> getString(R.string.label_info_suggest_video)
            else -> getString(R.string.label_info_feedback)
        }

        done.visibility = View.GONE
        successResult.visibility = View.GONE
        group.visibility = View.VISIBLE
        GroupHelper(icon_phone, phoneInputLayout).visibility =
                if (type != "addIco") View.GONE else View.VISIBLE

        setBackgroundTintWithCornersSate(send, ContextCompat.getColor(this, R.color.green))

        if (UserInfo.email.isNotEmpty()) emailInput.setText(UserInfo.email)

        send.setOnClickListener {

            if (!Patterns.EMAIL_ADDRESS.matcher(emailInput.text).matches()) {
                toast(getString(R.string.correct_address_required))
                return@setOnClickListener
            }

            if (messageInput.text?.isEmpty()!!) {
                toast(getString(R.string.message_required))
                return@setOnClickListener
            }

            send.isEnabled = false
            RestApi.sendForm(
                    type = type,
                    name = ""/*nameInput.text.toString()*/,
                    email = emailInput.text.toString(),
                    message = messageInput.text.toString(),
                    eventId = eventId,
                    tel = phoneInput.text.toString()
            )
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe { group.visibility = View.GONE; progress.visibility = View.VISIBLE }
                    .doOnTerminate { progress.visibility = View.GONE }
                    .subscribe({
                        successResult.visibility = View.VISIBLE
                        done.visibility = View.VISIBLE
                        UserInfo.tempFeedback = ""
                        UserInfo.tempFeedback = ""
                        send.isEnabled = true
                    }, {
                        successResult.visibility = View.VISIBLE
                        done.visibility = View.GONE

                        thanks.text = getString(R.string.something_wrong)
                        senResult.text = getString(R.string.error_description)
                        send.isEnabled = true
                    })
        }

        subjectSpinner.adapter = ArrayAdapter<String>(
                this, R.layout.spinner_list_item,
                resources.getStringArray(R.array.subject)
        ).also { it.setDropDownViewResource(android.R.layout.simple_list_item_1) }

        if (type == "other") {
            subjectSpinner.setSelection(2)

            val keys = resources.getStringArray(R.array.subject_keys)
            type = keys[2]
            subjectSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                    type = keys[position] ?: keys.last()
                    GroupHelper(icon_phone, phoneInputLayout).visibility =
                            if (type != "addIco") View.GONE else View.VISIBLE
                }

            }
        } else {
            GroupHelper(
                    icon_subject, subjectSpinner, fake_line,
                    fake_subject_hint, icon_phone, phoneInputLayout
            ).visibility = View.GONE
        }
    }


}