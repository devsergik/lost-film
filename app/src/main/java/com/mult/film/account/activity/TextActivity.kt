package com.mult.film.account.activity

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.BulletSpan
import android.text.style.RelativeSizeSpan
import com.mult.film.R
import com.mult.film.common.LanguageContextWrapper
import com.mult.film.common.UserInfo
import com.mult.film.common.api.RestApi
import com.mult.film.common.viewUtils.px
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_formatted_text.*
import org.jetbrains.anko.Bold
import org.jetbrains.anko.append
import org.jetbrains.anko.appendln
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader

/**
 * Created by user on 06.10.2017.
 */
class TextActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_formatted_text)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener { onBackPressed() }


        val stringExtra = intent.getStringExtra("what")
        val what = when (stringExtra) {
            "ABOUT_US" -> "about_us"
            "PRIVACY_POLICY" -> "privacy_policy"
            "DISCLAIMER" -> "disclaimer"
            else -> ""
        }

        toolbar.title = when (stringExtra) {
            "ABOUT_US" -> getString(R.string.about_us)
            "PRIVACY_POLICY" -> getString(R.string.privacy_policy)
            "DISCLAIMER" -> getString(R.string.disclaimer)
            else -> ""
        }

        if (what in arrayOf("privacy_policy", "about_us", "disclaimer"))
            loadMarkdownFromAssets("text/${what}")
    }


    private fun loadMarkdownFromAssets(assetsFilePath: String) = try {
        val stream = this.assets.open(assetsFilePath)
        val reader = BufferedReader(InputStreamReader(stream, "UTF-8"))

        val lineSequence = reader.lineSequence()
        performText(lineSequence)
    } catch (e: IOException) {
        e.printStackTrace()
    }

    private fun performText(lineSequence: Sequence<String>) {
        val reg = Regex("\\*\\*(.*?)\\*\\*")
        val sp = builder {
            lineSequence.forEach { line ->
                when {
                    line.startsWith("* ") -> {
                        appendln(line.replace("* ", ""), BulletSpan(16.px))
                    }
                    line.startsWith("## ") -> {
                        append(line.replace("## ", ""), Bold, RelativeSizeSpan(2f))
                        appendln()
                    }
                    line.startsWith("### ") -> {
                        append(line.replace("### ", ""), Bold, RelativeSizeSpan(1.5f))
                        appendln()
                    }
                    else -> {
                        val ranges = reg.findAll(line).toList().map { it.groups[1]?.range }

                        if (ranges.isNotEmpty()) {
                            var lastIndex = 0
                            ranges.forEachIndexed { index, range ->
                                range?.let {
                                    append(line.subSequence(lastIndex, it.first - 2))
                                    append(line.subSequence(it.first, it.last + 1), Bold)
                                    lastIndex = it.last + 3
                                }
                            }
                            appendln(line.subSequence(lastIndex, line.length))
                        } else appendln(line)
                    }
                }
            }
        }


        setText(sp)
    }

    private fun setText(buf: Spanned) {
        text.text = buf
    }

    private inline fun builder(f: SpannableStringBuilder.() -> Unit): SpannableStringBuilder {
        return SpannableStringBuilder().apply(f)
    }

}