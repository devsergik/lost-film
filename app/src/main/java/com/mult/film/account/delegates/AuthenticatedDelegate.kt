package com.mult.film.account.delegates

/**
 * Created by user on 29.09.2017.
 */
interface AuthenticatedDelegate {
    fun logout()
}