package com.mult.film.account.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import com.google.android.gms.auth.GoogleAuthUtil
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.common.Scopes
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.GoogleApiClient
import com.mult.film.R
import com.mult.film.common.*
import com.mult.film.dialogs.DialogPreloader
import com.twitter.sdk.android.core.Callback
import com.twitter.sdk.android.core.Result
import com.twitter.sdk.android.core.TwitterException
import com.twitter.sdk.android.core.TwitterSession
import com.vk.api.sdk.VK
import com.vk.api.sdk.auth.VKAccessToken
import com.vk.api.sdk.auth.VKAuthCallback
import com.vk.api.sdk.auth.VKScope
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_social_logins.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.startActivityForResult
import org.jetbrains.anko.toast


/**
 * Created by user on 02.10.2017.
 */
class SocialLoginActivity : AppCompatActivity(), GoogleApiClient.OnConnectionFailedListener {

    private val TAG = "SLActivity"

    private val callbackManager = CallbackManager.Factory.create()
    private val RC_SIGN_IN = 9001

    private val mGoogleApiClient: GoogleApiClient by lazy {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build()

        GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_social_logins)



        googleFakeButton.setOnClickListener { signInGoogle() }
        setBackgroundTintWithCorners(googleFakeButton, Color.parseColor("#FFFFFF"))

        facebookFakeButton.setOnClickListener { facebookLoginButton.performClick() }
        setBackgroundTintWithCorners(facebookFakeButton, Color.parseColor("#3b5998"))

        twitterFakeButton.setOnClickListener { twitterLoginButton.performClick() }
        setBackgroundTintWithCorners(twitterFakeButton, Color.parseColor("#55acee"))

        setBackgroundTint(bottom, Color.WHITE)

        vkFakeButton.setOnClickListener {
            VK.login(this, arrayListOf(VKScope.OFFLINE))
        }
        setBackgroundTintWithCorners(vkFakeButton, Color.parseColor("#5181b8"))

        twitterLoginButton.callback = object : Callback<TwitterSession>() {
            override fun failure(exception: TwitterException?) {
                toast("Cannot login with twitter")
            }

            override fun success(result: Result<TwitterSession>?) {
                val session = result?.data
                session?.let { session ->

                    //handleTwitterSession(session)

                    val authToken = session.authToken
                    val token = authToken.token
                    val secret = authToken.secret

                    tryLogin(token, "twitter", secret)

                    Log.i("SL_TWITTER", "authToken: $authToken\ntoken: $token\nsecret: $secret")
                }
            }

        }

        facebookLoginButton.setReadPermissions("email")

        facebookLoginButton.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onError(error: FacebookException?) {
                toast("Cannot login with facebook")
            }

            override fun onSuccess(result: LoginResult?) {
                result?.let {
                    val authToken = it.accessToken
                    val token = authToken.token
                    tryLogin(token, "facebook")
                }
            }

            override fun onCancel() {
                Log.i("SL_FACEBOOK", "canceled")
            }
        })

        googleSignInButton.setOnClickListener { }


        loginEmail.setOnClickListener {
            startActivityForResult<LoginEmailActivity>(666)
            sayNeedFinish()
        }

        signInEmail.setOnClickListener {
            startActivityForResult<RegistrationEmailActivity>(666)
            sayNeedFinish()
        }
        cancel.setOnClickListener { onBackPressed() }
        privacyPolicy.setOnClickListener {
            startActivity<TextActivity>(
                    "what" to "PRIVACY_POLICY"
            )
        }


    }

    private fun tryLogin(social_token: String, type: String, social_secret: String = "", social_email: String = "") {
        val preloader = DialogPreloader()
        preloader.show(supportFragmentManager, "")
        GlobalScope.launch {
            AccountManager.auth(type = type, email = social_email, socialSecret = social_secret, socialToken = social_token)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        if (it.result!!) {
                            sayNeedFinish()
                            finish()
                        } else {
                            var message_error = it.message
                            if (message_error == null) {
                                message_error = getString(R.string.error_auth)
                            }
                            message_error?.let { toast(it) }
                        }
                        preloader.dismissAllowingStateLoss()
                    }, {
                        preloader.dismissAllowingStateLoss()
                        it.printStackTrace()
                    })
        }
    }


    private fun signInGoogle() {
        val availability = GoogleApiAvailability.getInstance()
        val status = availability.isGooglePlayServicesAvailable(this)
        if (status == ConnectionResult.SUCCESS) {
            val signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient)
            startActivityForResult(signInIntent, RC_SIGN_IN)
        } else {
            if (availability.isUserResolvableError(status)) {
                availability.getErrorDialog(this, status, 2404).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val callback = object : VKAuthCallback {
            override fun onLogin(token: VKAccessToken) {
                tryLogin(token.accessToken, "vk")
            }

            override fun onLoginFailed(errorCode: Int) {
                toast("Cannot login with VK")
            }

        }

        if (!VK.onActivityResult(requestCode, resultCode, data!!, callback)) {
            super.onActivityResult(requestCode, resultCode, data)
            twitterLoginButton.onActivityResult(requestCode, resultCode, data)
            callbackManager.onActivityResult(requestCode, resultCode, data)

            if (requestCode == RC_SIGN_IN) {
                //val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
                try {
                    val task = GoogleSignIn.getSignedInAccountFromIntent(data)
                    val account = task.getResult(ApiException::class.java)
                    firebaseAuthWithGoogle(account!!)
                } catch (e: ApiException) {
                    e.printStackTrace()
                    toast("Cannot login with Google")
                }
            }

            if (requestCode == 666) {
                if (resultCode == Activity.RESULT_OK) {
                    sayNeedReload()
                    finish()
                }
            }
        }
    }

    // [START auth_with_google]
    private fun firebaseAuthWithGoogle(acct: GoogleSignInAccount) {
        Log.d(TAG, "firebaseAuthWithGoogle:${acct.id}")

        Single.create<String> {
            it.onSuccess(
                    GoogleAuthUtil.getToken(
                            this, acct.account, "oauth2:${Scopes.EMAIL} ${Scopes.PROFILE}"
                    )
            )
        }.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    tryLogin(it, "google")
                }, {
                    it.printStackTrace()
                })
    }

    override fun onConnectionFailed(result: ConnectionResult) {
        println("Connection failed: $result")
    }

}