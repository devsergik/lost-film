package com.mult.film.account.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Patterns
import com.chibatching.kotpref.bulk
import com.mult.film.R
import com.mult.film.common.*
import com.mult.film.dialogs.DialogPreloader
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_registration_email.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.startActivityForResult
import org.jetbrains.anko.toast

/**
 * Created by user on 10.10.2017.
 */

class RegistrationEmailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration_email)

        email.setCompoundDrawablesWithIntrinsicBounds(
                ContextCompat.getDrawable(this, R.drawable.ic_mail),
                null, null, null
        )
        password.setCompoundDrawablesWithIntrinsicBounds(
                ContextCompat.getDrawable(this, R.drawable.ic_lock_outline),
                null, null, null
        )

        setBackgroundTintWithCornersSate(apply, ContextCompat.getColor(this, android.R.color.white))


        apply.setOnClickListener {

            if (email.text?.isEmpty()!! || !Patterns.EMAIL_ADDRESS.matcher(email.text).matches()) {
                toast(getString(R.string.correct_address_required))
                return@setOnClickListener
            }

            if (password.text?.isEmpty()!!) {
                toast(getString(R.string.name_required))
                return@setOnClickListener
            }

            if (!privacyPolicyCheckBox.isChecked) {
                toast(getString(R.string.privacy_policy_checked_required))
                return@setOnClickListener
            }

            apply.isEnabled = false

            val preloader = DialogPreloader()
            preloader.show(supportFragmentManager, "")
            AccountManager.register(email.text.toString(), password.text.toString())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doAfterTerminate { preloader.dismissAllowingStateLoss() }
                    .subscribe({
                        apply.isEnabled = true
                            val isAuthorization =
                                if (it.containsKey("result")) it["result"] as Boolean else false
                        var user_id  = 0
                        var access_token = ""
                        var user_email = ""

                            if(isAuthorization) {
                                val user = it["user"] as Map<String, Any>
                                access_token = (user.get("token") as String).toString()
                                user_id = (user.get("id") as Double).toInt()
                                user_email = user.get("email") as String
                                sayNeedFinish()
                                finish()
                            } else {
                                val message_error = it["message"] as String
                                toast(message_error)
                            }
                        UserInfo.bulk {
                            registered = isAuthorization
                            id = user_id
                            token = access_token
                            email = user_email
                        }
                    }, {
                        toast(getString(R.string.error_auth))
                        it.printStackTrace()
                        apply.isEnabled = true
                    })
        }


        alreadyRegistered.setOnClickListener {
            startActivityForResult<LoginEmailActivity>(666)
            sayNeedFinish()
        }

        privacyPolicy.setOnClickListener {
            startActivity<TextActivity>(
                    "what" to "PRIVACY_POLICY"
            )
        }

/*        socialLogin.setOnClickListener {
            startActivityForResult<LoginEmailActivity>(666)//TODO social SocialLoginActivity
            sayNeedFinish()
        }*/

        cancel.setOnClickListener { onBackPressed() }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 666) {
            if (resultCode == Activity.RESULT_OK) {
                sayNeedFinish()
                finish()
            }
        }
    }

}
