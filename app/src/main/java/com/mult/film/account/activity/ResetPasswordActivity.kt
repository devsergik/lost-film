package com.mult.film.account.activity

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.graphics.drawable.DrawableCompat
import android.support.v7.app.AppCompatActivity
import android.util.Patterns
import com.mult.film.R
import com.mult.film.common.*
import com.mult.film.common.api.RestApi
import com.mult.film.dialogs.DialogPreloader
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_reset_password.*
import org.jetbrains.anko.toast

/**
 * Created by user on 18.10.2017.
 */
class ResetPasswordActivity: AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_password)

        setBackgroundTintWithCornersSate(apply, Color.WHITE)
        email.setCompoundDrawablesWithIntrinsicBounds(
                ContextCompat.getDrawable(this, R.drawable.ic_mail),
                null, null, null
        )


        apply.setOnClickListener {

            if (email.text?.isEmpty()!! || !Patterns.EMAIL_ADDRESS.matcher(email.text).matches()) {
                toast(getString(R.string.correct_address_required))
                return@setOnClickListener
            }

            apply.isEnabled = false
            val preloader = DialogPreloader()
            preloader.show(supportFragmentManager, "")

            RestApi.resetPassword(email.text.toString().trim())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doAfterTerminate { preloader.dismissAllowingStateLoss() }
                    .subscribe({
                        apply.isEnabled = true
                        email.setText("")
                        toast(getString(R.string.password_reset_check_inbox))
                        finish()
                    }, {
                        apply.isEnabled = true
                        it.printStackTrace()
                        toast(getString(R.string.password_reset_failed))
                    })
        }

        cancel.setOnClickListener { onBackPressed() }
    }


}