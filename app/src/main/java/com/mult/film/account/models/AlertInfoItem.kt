package com.mult.film.account.models

import com.squareup.moshi.Json

/**
 * Created by user on 02.10.2017.
 */
class AlertInfoItem(
        @Json(name = "id") val eventID: Int,
        @Json(name = "names") val eventName: String,
        @Json(name = "code") val logoLink: String
)