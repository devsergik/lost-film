package com.mult.film.account.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.chibatching.kotpref.blockingBulk
import com.chibatching.kotpref.bulk
import com.facebook.login.LoginManager
import com.mult.film.BuildConfig
import com.mult.film.R
import com.mult.film.account.adapter.AccountMenuAdapter
import com.mult.film.account.delegates.AuthenticatedDelegate
import com.mult.film.account.delegates.CommonItemsDelegate
import com.mult.film.account.delegates.UnauthenticatedDelegate
import com.mult.film.common.*
import com.mult.film.common.api.RestApi
import com.mult.film.common.customtabs.BrowserActivity
import com.mult.film.data.db.DataBaseRoom
import com.mult.film.dialogs.DialogPreloader
import com.mult.film.main.view.MainScreen
import com.vk.api.sdk.VK
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_account.*
import org.jetbrains.anko.*


/**
 * Created by user on 28.09.2017.
 */
class AccountActivity : AppCompatActivity(),
        CommonItemsDelegate, AuthenticatedDelegate, UnauthenticatedDelegate, SigningListener {
    override fun proceed(registered: Boolean) {
    }

    override fun additional() {
        startActivity<BrowserActivity>(
                "website" to UserInfo.additionalMenuLink,
                "name" to UserInfo.additionalMenuTitle
        )
    }


    private var adapter = AccountMenuAdapter(
            this,
            this,
            this,
            UserInfo.registered
    )


    override fun logout() {
        alert(R.string.logout_alert) {
            yesButton {
                val preloader = DialogPreloader()

                preloader.show(supportFragmentManager, "")
                AccountManager.logout()
                        .map {
                            val result =
                                    if (it.containsKey("result")) it["result"] as Boolean else false
                            if(result) {
                                DataBaseRoom.getInstance().favoritesUserDao().deleteFavUser()
                                UserParamsUpdate.update()
                            }
                            result
                        }
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doAfterSuccess { preloader.dismissAllowingStateLoss() }
                        .subscribe({
                            if(it) {
                                UserInfo.blockingBulk {
                                    registered = false
                                    id = -1
                                    token = ""
                                }

                                adapter = AccountMenuAdapter(
                                        this@AccountActivity,
                                        this@AccountActivity,
                                        this@AccountActivity,
                                        UserInfo.registered
                                )
                                menu.adapter = adapter

                                LoginManager.getInstance().logOut()
                                VK.logout()
                                UserInfo.bulk {
                                    email = ""
                                    name = ""
                                }

                                setResult(666)
                            }

                        },  Throwable::printStackTrace)
                }
            noButton { }
        }.show()
    }


    private var clickCounter = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account)
        overridePendingTransition(R.anim.abc_slide_in_bottom, R.anim.stay)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
            overridePendingTransition(R.anim.stay, R.anim.abc_slide_out_bottom)
        }

        menu.layoutManager = LinearLayoutManager(this)

        checkScreenSize(menu)

        supportActionBar?.let { bar ->
            bar.setDisplayHomeAsUpEnabled(true)
            bar.setHomeAsUpIndicator(R.drawable.ic_close_white)
        }
    }

    override fun onResume() {
        super.onResume()
        AccountManager.addSigningListener(this)
        adapter = AccountMenuAdapter(
                this,
                this,
                this,
                UserInfo.registered
        )
        menu.adapter = adapter

    }

    override fun onDestroy() {
        super.onDestroy()
        AccountManager.removeSigningListener(this)
    }




    override fun login() {
        startActivityForResult<LoginEmailActivity>(222)//TODO social SocialLoginActivity
    }

    override fun aboutUs() {
        startActivity<TextActivity>(
                "what" to "ABOUT_US"
        )
    }

    override fun offerIco() {
        startActivity<SendMessageActivity>("TYPE" to "addIco")
    }

    override fun rateApp() {
        showUrlIntent("https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}")
    }

    override fun feedback() {
        startActivity<SendMessageActivity>("TYPE" to "other")
    }

    override fun advertising() {
        startActivity<SendMessageActivity>("TYPE" to "adv")
    }

    override fun privacyPolicy() {
        startActivity<TextActivity>(
                "what" to "PRIVACY_POLICY"
        )
    }

    override fun disclaimer() {
        startActivity<TextActivity>(
                "what" to "DISCLAIMER"
        )
    }
}