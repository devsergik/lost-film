package com.mult.film.account.adapter

import com.hannesdorfmann.adapterdelegates3.ListDelegationAdapter
import com.mult.film.account.models.AlertInfoItem
import com.mult.film.common.ItemClickListener
import com.mult.film.common.ListItem


/**
 * Created by user on 02.10.2017.
 */
class NotificationsSettingsAdapter(clickListener: ItemClickListener) :
        ListDelegationAdapter<MutableList<ListItem>>(), ItemClickListener by clickListener {

    init {
        items = mutableListOf()
        delegatesManager.addDelegate(NotificationsSettingsAdapterDelegate(this::invoke))
    }

    fun setData(data: List<AlertInfoItem>) {
        items.clear()
        items.addAll(data.map { ListItem.AlertItem(it) })

        notifyDataSetChanged()
    }
}