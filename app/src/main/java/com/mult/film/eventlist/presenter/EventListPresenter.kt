package com.mult.film.eventlist.presenter

import android.content.SharedPreferences
import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.mult.film.common.*
import com.mult.film.common.presenter.LoadMorePresenter
import com.mult.film.data.db.DataBaseRoom
import com.mult.film.eventlist.view.EventListView
import com.mult.film.data.models.EventsResponse
import com.mult.film.data.db.Event
import com.mult.film.repositories.EventsRepository
import com.mult.film.repositories.RequestCompleteListener
import io.reactivex.disposables.CompositeDisposable


@InjectViewState
class EventListPresenter(val key: String) : LoadMorePresenter<Event, EventListView>(), RequestCompleteListener, SigningListener {
    override fun proceed(registered: Boolean) {
        if (!registered)
            viewState.resetActivateStatus()
        else
            viewState.reloadData()
    }

    override fun onRequestComplete(request: Int, eventID: Int) {
        if (request == 1 && eventID != -1)
            viewState.changeLikedStatus(eventID, false)
    }

    private var disposables = CompositeDisposable()

    suspend override fun loadDataByPage(page: Int): EventsResponse {
        return EventsRepository.loadFromServerByKey(key, page)
    }


    init {
        val user_dao = DataBaseRoom.getInstance()
        val data_base = user_dao.categoriesDao()
        disposables += data_base.getAllFlowableCategory()
            .subscribe ({
                viewState.reloadData()
        }, Throwable::printStackTrace)
        UserInfo.bountyFilter

        EventsRepository.addRequestCompleteListener(this)
        AccountManager.addSigningListener(this)

    }


    val registerListener = SharedPreferences.OnSharedPreferenceChangeListener { sharedPreferences, key ->
        if (key == "registered") {
            Log.i("DEBUG", "Is SettingEntity registered = " + UserInfo.registered)
            if (!UserInfo.registered) {
                viewState.resetActivateStatus()
            } else {
                viewState.reloadData()
            }
        }
    }

    val bountyFilterListener = SharedPreferences.OnSharedPreferenceChangeListener { sharedPreferences, key ->
        if (key == "bountyFilter") {
            viewState.reloadData()
        }
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        loadData()
    }

    override fun attachView(view: EventListView) {
        super.attachView(view)
        UserInfo.preferences.registerOnSharedPreferenceChangeListener(registerListener)
        UserInfo.preferences.registerOnSharedPreferenceChangeListener(bountyFilterListener)
    }



    override fun onProtocolException() {
        viewState.showFilterError()
    }

    override fun onFirstPage() {
        viewState.setData(emptyList())
        viewState.hideError()
    }

    override fun detachView(view: EventListView?) {
        UserInfo.preferences.unregisterOnSharedPreferenceChangeListener(registerListener)
        UserInfo.preferences.unregisterOnSharedPreferenceChangeListener(bountyFilterListener)
        super.detachView(view)
    }

    override fun onDestroy() {
        disposables.dispose()
        AccountManager.removeSigningListener(this)
        super.onDestroy()
    }

    override fun updateDB(items: List<Event>) {
    }
}