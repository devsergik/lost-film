package com.mult.film.eventlist.view

import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.mult.film.R
import com.mult.film.common.*
import com.mult.film.common.glide.GlideApp
import com.mult.film.common.viewUtils.EndlessRecyclerViewScrollListener
import com.mult.film.data.db.Event
import com.mult.film.dialogs.AuthRequiredDialog
import com.mult.film.eventlist.adapters.EventsAdapter
import com.mult.film.eventlist.presenter.EventListPresenter
import com.mult.film.repositories.EventsRepository
import kotlinx.android.synthetic.main.fragment_events.*

class EventsFragment : MvpAppCompatFragment(), EventListView, Scrollable, Title {
    override fun showEmptyContentError() {
    }

    override fun unknownException() {
        refresh.visibility = View.INVISIBLE
        errorGroup.visibility = View.VISIBLE

        errorTitle.text = getString(R.string.something_wrong)
        errorDescription.text = getString(R.string.error_description)
        context?.let { ctx ->
            errorImage.setImageDrawable(ContextCompat.getDrawable(ctx, R.drawable.error_connection))
        }

        retryButton.visibility = View.VISIBLE
    }

    override fun internetConnectionError() {
        refresh.visibility = View.INVISIBLE
        errorGroup.visibility = View.VISIBLE

        errorTitle.text = getString(R.string.connection_error_title)
        errorDescription.text = getString(R.string.connection_error_description)
        context?.let { ctx ->
            errorImage.setImageDrawable(ContextCompat.getDrawable(ctx, R.drawable.error_connection))
        }

        retryButton.visibility = View.VISIBLE
    }

    override fun showFilterError() {
        refresh.visibility = View.INVISIBLE
        errorGroup.visibility = View.VISIBLE

        errorTitle.text = getString(R.string.filter_error_title)
        errorDescription.text = getString(R.string.filter_error_description)
        context?.let { ctx ->
            errorImage.setImageDrawable(ContextCompat.getDrawable(ctx, R.drawable.error_filter))
        }
    }


    override fun setData(data: List<Event>) {
        errorGroup.visibility = View.GONE
        refresh.visibility = View.VISIBLE
        events.post { adapter.setData(data) }
        refresh.isRefreshing = false
    }

    override fun setServerError(error: Boolean) {
        events.post { adapter.setIsServerError(error) }
    }

    override fun showFooter() {
        events.post { adapter.showFooter() }
    }

    override fun hideFooter() {
        refresh.isRefreshing = false
        events.post { adapter.hideFooter() }
    }

    override fun showErrorFooter() {
        refresh.isRefreshing = false
        events.post { adapter.showErrorFooter() }
    }

    override fun hideErrorFooter() {
        refresh.isRefreshing = false
        events.post { adapter.hideErrorFooter() }
    }

    @InjectPresenter
    lateinit var presenter: EventListPresenter

    private val adapter by lazy {

        EventsAdapter(
                reloadAction = {
                    presenter.reloadNextPage()

                },
                changeLikeState = { id, checked ->
                    EventsRepository.changeLikeStatus(id, checked) {}
                },
                userRegistered = { type ->
                    UserInfo.registered.also {
                        if (!it) AuthRequiredDialog()
                                .apply {
                                    arguments = Bundle().apply {
                                        putString("types", type)
                                    }
                                }
                                .showAllowingStateLoss(childFragmentManager, "")
                    }
                },
                glideRequest = GlideApp.with(context!!)
        )
    }


    override fun resetActivateStatus() {
        if (!UserInfo.registered)
            events?.post { adapter.resetAll() }
    }

    override fun reloadData() {
        presenter.loadData()
    }

    override fun appendData(data: List<Event>) {
        events.post { adapter.appendData(data) }
        errorGroup.visibility = View.GONE
        refresh.visibility = View.VISIBLE
    }

    override fun hideError() {
        refresh.visibility = View.VISIBLE
        errorGroup.visibility = View.INVISIBLE
    }

    @ProvidePresenter
    fun provideEventListPresenter() =
            EventListPresenter(arguments?.getString(EVENTS_KEY) ?: TAB_MULT)

    private fun checkNeedReload() {
        if (adapter.isError())
            presenter.reloadLastPage()
    }

    override fun changeLikedStatus(id: Int, liked: Boolean) {
        events.post { adapter.checkLikedStatus(id, liked) }
    }

    companion object {
        private var instanceCount = 0

        private val cache = mapOf(
                TAB_MULT to EventsFragment.makeInstance(TAB_MULT)
        )

        fun getInstance(key: String): Fragment = cache[key]
                ?: throw IllegalArgumentException("argument 'period' must be in arrayOf(TAB_TV, FUTURE, PAST)")

        private fun makeInstance(key: String): Fragment = EventsFragment()
                .apply { arguments = Bundle().apply { putString(EVENTS_KEY, key) } }
                .also { Log.i("EvFr", "Instance news: ${instanceCount++}") }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
            inflater.inflate(R.layout.fragment_events, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //events.setHasFixedSize(true)
        events.layoutManager = LinearLayoutManager(context)
        events.adapter = adapter

        val footerViewType = adapter.getFooterViewType()

        val scrollListener =
                object : EndlessRecyclerViewScrollListener(events.layoutManager as LinearLayoutManager) {
                    override fun getFooterViewType(defaultNoFooterViewType: Int) = footerViewType

                    override fun onLoadMore(page: Int, totalItemsCount: Int) {
                        Log.i("ERVSL", "requested page: $page")
                        presenter.loadMore(page = page)
                    }

                }
        events.addOnScrollListener(scrollListener)


        // events.addItemDecoration(BackgroundItemDecoration())

        Log.i(javaClass.simpleName, presenter.key)

        retryButton.setOnClickListener {
            presenter.loadData()
            EventsFragment.cache
                    .values
                    .filter { it != this }
                    .forEach {
                        (it as? EventsFragment)?.checkNeedReload()
                    }
            retryButton.visibility = View.GONE
        }

        refresh.setOnRefreshListener { presenter.loadData() }
    }

    override fun scrollToPosition(position: Int) {
        events?.let { list ->
            (list.layoutManager as LinearLayoutManager?)?.let { lManager ->
                val targetPos = lManager.findLastVisibleItemPosition() - 2
                if (targetPos >= 0)
                    list.smoothScrollToPosition(targetPos)

                Handler().postDelayed({
                    list.scrollToPosition(0)
                }, 300)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        val start = (events.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
        val end = (events.layoutManager as LinearLayoutManager).findLastVisibleItemPosition()

        adapter.notifyItemRangeChanged(start, end - start, "timer")
    }

    override val title: String
        get() {
            val category = getString(R.string.now_title)
            return category + " (${EventsRepository.requestItemsCount[category]})"
        }

    override fun setScrollPosition(currentScroll: Int) {
        super.setScrollPosition(currentScroll)

        Handler().postDelayed({
            if (currentScroll >= 0)
                events.scrollToPosition(currentScroll)
        }, 100)
    }
}