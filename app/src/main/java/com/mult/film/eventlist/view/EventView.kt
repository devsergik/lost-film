package com.mult.film.eventlist.view

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

/**
 * Created by user on 15.09.2017.
 */
interface EventView : MvpView {
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun setFollowState(follow: Boolean)
}