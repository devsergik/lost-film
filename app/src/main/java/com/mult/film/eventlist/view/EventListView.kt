package com.mult.film.eventlist.view

import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.mult.film.common.view.LoadMoreView
import com.mult.film.data.models.ParamsModel
import com.mult.film.data.db.Event

/**
 * Created by user on 08.09.2017.
 */
@StateStrategyType(OneExecutionStateStrategy::class)
interface EventListView : LoadMoreView<Event> {
    fun changeLikedStatus(id: Int, liked: Boolean)
    fun showFilterError()
    fun resetActivateStatus()
    fun reloadData()
}