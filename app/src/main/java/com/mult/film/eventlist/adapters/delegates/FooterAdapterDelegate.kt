package com.mult.film.eventlist.adapters.delegates

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ProgressBar
import android.widget.TextView
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import com.mult.film.R
import com.mult.film.common.Action
import com.mult.film.common.ListItem
import com.mult.film.common.inflate
import org.jetbrains.anko.layoutInflater

/**
 * Created by user on 30.10.2017.
 */
class FooterAdapterDelegate(
        val reloadAction: Action
) : AdapterDelegate<MutableList<ListItem>>() {
    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return FooterHolder(parent.context.layoutInflater.inflate(R.layout.partial_footer_loading, parent, false), reloadAction)
    }

    override fun onBindViewHolder(items: MutableList<ListItem>, position: Int, holder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
        val (waitMore, serverError) = (items[position] as ListItem.FooterItem)
        (holder as FooterHolder).bind(waitMore, serverError)
    }

    override fun isForViewType(items: MutableList<ListItem>, position: Int)
            = items[position] is ListItem.FooterItem

    class FooterHolder(containerView: View, val reloadAction: Action) : RecyclerView.ViewHolder(containerView) {

        private val retry = containerView.findViewById<ImageButton>(R.id.retryButton)
        private val card = containerView.findViewById<CardView>(R.id.card)
        private val progressBar = containerView.findViewById<ProgressBar>(R.id.progressBar)
        private val errorText = containerView.findViewById<TextView>(R.id.errorText)

        fun bind(waitingMore: Boolean, serverError: Boolean) {
            Log.i("FOOTER", "I'm footer!")

            retry.setOnClickListener { reloadAction() }

            card.setOnClickListener { retry.performClick() }

            if (waitingMore) {
                card.visibility = View.GONE
                progressBar.visibility = View.VISIBLE
            } else {
                card.visibility = View.VISIBLE
                progressBar.visibility = View.GONE
                errorText.setText(
                        if (serverError) R.string.server_dialog_title else R.string.network_dialog_title
                )
            }


        }
    }
}