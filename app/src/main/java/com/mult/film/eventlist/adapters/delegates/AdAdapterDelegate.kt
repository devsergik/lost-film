package com.mult.film.eventlist.adapters.delegates

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import com.mult.film.R
import com.mult.film.common.ListItem
import com.mult.film.common.TAB_MULT
import com.mult.film.common.api.RestApi
import com.mult.film.common.glide.GlideApp
import com.mult.film.common.inflate
import com.mult.film.data.db.Event
import kotlinx.android.synthetic.main.list_item_ad.view.*

/**
 * Created by user on 30.10.2017.
 */
class AdAdapterDelegate : AdapterDelegate<MutableList<ListItem>>() {

    var period: String = TAB_MULT

    override fun isForViewType(items: MutableList<ListItem>, position: Int)
            = items[position] is ListItem.EventItem

    override fun onCreateViewHolder(parent: ViewGroup) = AdViewHolder(parent.inflate(R.layout.list_item_ad))

    override fun onBindViewHolder(items: MutableList<ListItem>, position: Int, holder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
        (holder as AdViewHolder).let {
            val adv = (items[position] as ListItem.EventItem).event
            it.bind(adv, period, position)
        }
    }


    class AdViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(event: Event, period: String, position: Int) {
            var base_url = RestApi.test
            GlideApp.with(itemView.context)
                    .load(base_url + event.id)
                    .into(itemView.logo)

/*
            GlideApp.with(itemView.context)
                    .load(base_url + event.url_picture)
                    .into(itemView.picture)
*/

            itemView.title.text = event.title
          //  itemView.moreInfo.text = event.button_text

/*            itemView.moreInfo.setOnClickListener {
             //   itemView.context.openSite(event.button_link, null)
              //  RestApi.advertisingCounter(event.id.ico)
            }*/
            itemView.setOnClickListener { itemView.moreInfo.performClick() }

        }

    }
}