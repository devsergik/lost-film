package com.mult.film.eventlist.adapters.delegates

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import com.mult.film.eventDetails.view.EventDetailsActivity
import com.mult.film.R
import com.mult.film.common.*
import com.mult.film.common.api.RestApi
import com.mult.film.common.glide.GlideApp
import com.mult.film.common.glide.GlideRequests
import com.mult.film.data.db.Event
import org.jetbrains.anko.*
import com.bumptech.glide.load.resource.bitmap.CenterCrop


@SuppressLint("CheckResult")
/**
 * Created by user on 30.10.2017.
 */
class EventAdapterDelegate(
        private val changeLikeState: (Int, Boolean) -> Unit,
        private val userRegistered: (String) -> Boolean,
        private val glideRequests: GlideRequests
) : AdapterDelegate<MutableList<ListItem>>() {

    var period: String = TAB_MULT

    override fun onViewDetachedFromWindow(holder: RecyclerView.ViewHolder?) {
        super.onViewDetachedFromWindow(holder)
        if (holder is EventViewHolder) {
            holder.item = null
        }
    }

    override fun onViewAttachedToWindow(holder: RecyclerView.ViewHolder) {
        super.onViewAttachedToWindow(holder)
        if (holder is EventViewHolder) {
        }
    }

    override fun onViewRecycled(viewHolder: RecyclerView.ViewHolder) {
        super.onViewRecycled(viewHolder)
        try {
            if (viewHolder is EventViewHolder) {
                //GlideApp.with(viewHolder.itemView.context).clear(viewHolder.headerImage)
            }
        } catch (e: Exception) {
        }
    }

    override fun isForViewType(items: MutableList<ListItem>, position: Int)
            = items[position] is ListItem.EventItem

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return EventViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_event_card, parent, false), glideRequests)
    }

    override fun onBindViewHolder(items: MutableList<ListItem>, position: Int, holder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
        (holder as EventViewHolder).let {
            val event = (items[position] as ListItem.EventItem).event
            it.bind(event)
        }
    }


    inner class EventViewHolder(val view: View, private val glideRequests: GlideRequests) : RecyclerView.ViewHolder(view) {

        internal val follow = view.findViewById<ImageView>(R.id.follow)
        internal val logo = view.findViewById<ImageView>(R.id.logo)
        internal val nameFilm = view.findViewById<TextView>(R.id.nameFilm)
        internal val category = view.findViewById<TextView>(R.id.category)
        internal val count_movies = view.findViewById<TextView>(R.id.count_movies)
        var item: Event? = null



        fun bind(event: Event) {
            item = event
            event.let { item ->
                var requestOptions = RequestOptions()
                requestOptions = requestOptions.transforms(CenterCrop(), RoundedCorners(6))

                GlideApp.with(view.context)
                        .load(RestApi.base + "/mult/assets/images/"+ item.movies?.get(0) +"/0.png")
                        .apply(requestOptions)
                        .error(R.drawable.ic_default)
                        .into(logo)

                nameFilm.text= item.title
                category.text = item.category?.name

                val size_movies = item.movies?.size
                count_movies.visibility = if(size_movies!! > 1) View.VISIBLE else View.GONE
                count_movies.text = size_movies.toString()

                follow.setOnClickListener {
                    val checked = !follow.isActivated
                    if (checked) {
                        if (userRegistered("favorite")) {
                            follow.isActivated = checked
                            //item.favorite = checked

                            changeLikeState(item.id.id, checked)
                            this.itemView.context.longToast(R.string.ico_added_to_favorite)
                        } else {
                            follow.isActivated = !checked
                        }
                    } else {
                        val alert = view.context.alert(view.context.getString(R.string.unfollow_event, item.title)) {
                            yesButton {
                                //item.favorite = checked
                                follow.isActivated = checked
                                changeLikeState(item.id.id, checked)
                                this@EventViewHolder.itemView.context.longToast(R.string.ico_removed_from_favorite)
                            }
                            noButton { follow.isActivated = !checked }
                        }.build()

                        alert.setCancelable(false)
                        alert.show()
                    }
                }


                follow.isActivated = item.id.favorite


                view.setOnClickListener {
                    showDetails(eventId = item.id.id, name = item.title!!)
                }


            }
        }

        private fun showDetails(eventId: Int, name: String = "") {
            (itemView.context).startActivity<EventDetailsActivity>(
                    "EVENT_ID" to eventId,
                    "EVENT_NAME" to (name ?: "")
            )
        }
    }
}