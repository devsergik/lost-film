package com.mult.film.eventlist.view

/**
 * Created by user on 18.09.2017.
 */
interface Scrollable {
    fun scrollToPosition(position: Int)
}