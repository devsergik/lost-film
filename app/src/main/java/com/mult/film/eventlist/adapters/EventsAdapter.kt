package com.mult.film.eventlist.adapters

import android.graphics.Color
import com.hannesdorfmann.adapterdelegates3.ListDelegationAdapter
import com.mult.film.common.Action
import com.mult.film.common.ListItem
import com.mult.film.common.glide.GlideRequests
import com.mult.film.common.view.HighlightAdapter
import com.mult.film.eventlist.adapters.delegates.AdAdapterDelegate
import com.mult.film.eventlist.adapters.delegates.EventAdapterDelegate
import com.mult.film.eventlist.adapters.delegates.FooterAdapterDelegate
import com.mult.film.data.models.ParamsModel
import com.mult.film.data.db.Event

/**
 * Created by user on 30.10.2017.
 */
class EventsAdapter(
        reloadAction: Action,
        changeLikeState: (Int, Boolean) -> Unit,
        userRegistered: (String) -> Boolean,
        glideRequest: GlideRequests
) : ListDelegationAdapter<MutableList<ListItem>>(), HighlightAdapter {

    override fun needHighlight(position: Int) = isPinned(position)

    private val eventAdapterDelegate = EventAdapterDelegate(
            changeLikeState,
            userRegistered,
            glideRequest
    )

    private val footerAdapterDelegate = FooterAdapterDelegate(
            reloadAction
    )

    private val advAdapterDelegate = AdAdapterDelegate()

    init {
        items = mutableListOf()
        delegatesManager
                .addDelegate(eventAdapterDelegate)
                .addDelegate(advAdapterDelegate)
                .addDelegate(footerAdapterDelegate)


    }

    override fun getHighlightColor(position: Int) = Color.parseColor("#00c853")


    fun showFooter() {
        if (items.isNotEmpty() && items.last() is ListItem.FooterItem) return

        items.add(ListItem.FooterItem(true, false))
        notifyItemInserted(itemCount)
    }

    fun hideFooter() {
        if (itemCount <= 0) return
        if (items[itemCount - 1] !is ListItem.FooterItem) return
        items.removeAt(itemCount - 1)
        notifyItemRemoved(itemCount - 1)
    }

    fun showErrorFooter() {
        if (itemCount <= 0) return
        if (items[itemCount - 1] !is ListItem.FooterItem) return

        (items[itemCount - 1] as ListItem.FooterItem).apply {
            waitingMore = false
            serverError = false
        }
        notifyItemChanged(itemCount - 1)
    }

    fun setIsServerError(error: Boolean) {
        if (itemCount <= 0) return
        if (items[itemCount - 1] !is ListItem.FooterItem) return
        (items[itemCount - 1] as ListItem.FooterItem).apply {
            waitingMore = false
            serverError = error
        }
        notifyItemChanged(itemCount - 1)
    }

    fun hideErrorFooter() {
        if (itemCount <= 0) return
        if (items[itemCount - 1] !is ListItem.FooterItem) return
        items.removeAt(itemCount - 1)
        notifyItemRemoved(itemCount - 1)
    }

    fun setData(data: List<Event>) {
        items.clear()
        items.addAll(data.map { ListItem.EventItem(it) })
        notifyDataSetChanged()
    }

    fun appendData(data: List<Event>) {
        items.addAll(data.map {ListItem.EventItem(it) })
        notifyItemRangeInserted(items.size - data.size, data.size)
    }

    fun checkLikedStatus(id: Int, liked: Boolean) {
        val index = items.indexOfFirst { it is ListItem.EventItem && it.event.id.id == id }
        if (index != -1) {
            val item = (items[index] as ListItem.EventItem).event
            item.id.favorite = !item.id.favorite
            notifyItemChanged(index, "liked" to liked)
        }
    }

    private fun isPinned(position: Int) = false //TODO за что это отвечает
/*            if (position < items.size && position >= 0 && items[position] is ListItem.EventItem)
                (items[position] as ListItem.EventItem).event.is_pinned else false*/

    fun isError(): Boolean {
        return itemCount == 1 &&
                (items.last() is ListItem.FooterItem && !(items.last() as ListItem.FooterItem).waitingMore)
    }

    fun getFooterViewType(): Int {
        return delegatesManager.getViewType(footerAdapterDelegate)
    }

    fun resetAll() {
        items
                .filter { it is ListItem.EventItem }
                .filter { (it as ListItem.EventItem).event.id.favorite}
                .map { (it as ListItem.EventItem).event }
                .forEach {
                    it.id.favorite = true

                    checkLikedStatus(it.id.id, false)
                }
    }
}