package com.mult.film.search.view

import com.mult.film.common.view.LoadMoreView
import com.mult.film.data.db.Event

/**
 * Created by user on 04.10.2017.
 */
interface SearchView : LoadMoreView<Event>