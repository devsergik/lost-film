package com.mult.film.search.adapter

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.mult.film.R
import com.mult.film.common.Action
import com.mult.film.common.UserInfo
import com.mult.film.common.api.RestApi
import com.mult.film.common.glide.GlideApp
import com.mult.film.data.db.Event
import com.mult.film.eventDetails.view.EventDetailsActivity
import org.jetbrains.anko.*
import java.text.DateFormat
import java.util.*

/**
 * Created by user on 05.10.2017.
 */
class SearchAdapter(
        private val changeLikeState: (Int, Boolean) -> Unit,
        private val userRegistered: (String) -> Boolean,
        val reloadAction: Action) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val items = mutableListOf<Event>()


    enum class ItemType(val type: Int) {
        Normal(0),
        LoadingFooter(666)
    }

    private var waitingMore = false
    var error = false
    var serverError = false

    fun showFooter() {
        waitingMore = true
        if (!error)
            notifyItemInserted(itemCount)
        else
            notifyItemChanged(itemCount - 1)
    }

    fun hideFooter() {
        waitingMore = false
        notifyItemRemoved(itemCount - 1)
    }

    fun showErrorFooter() {
        error = true
        waitingMore = false
        notifyItemChanged(itemCount - 1)
    }

    fun hideErrorFooter() {
        waitingMore = false

        error = false
        notifyItemRemoved(itemCount - 1)
    }

    fun setData(data: List<Event>) {
        items.clear()
        items.addAll(data)
        notifyDataSetChanged()
    }

    fun appendData(data: List<Event>) {
        items.addAll(data)
        notifyItemRangeInserted(items.size - data.size, data.size)
    }

    override fun getItemViewType(position: Int) = when {
        (waitingMore || error) && position == itemCount - 1 -> ItemType.LoadingFooter.type
        else -> ItemType.Normal.type
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            ItemType.LoadingFooter.type -> FooterHolder(inflater.inflate(R.layout.partial_footer_loading, parent, false))
            else -> ResultViewHolder(userRegistered, changeLikeState, inflater.inflate(R.layout.item_search_result, parent, false))
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int, payloads: MutableList<Any>) {
        if (payloads != null && items.size > 0 && payloads.size > 0) {
            Log.i("ChListener", "${items[position].title}:" + payloads.toString())
        }
        super.onBindViewHolder(holder, position, payloads)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ResultViewHolder -> holder.bind(items[position])
            is SearchAdapter.FooterHolder -> holder.bind()
        }
    }

    override fun getItemCount() = items.size + if (waitingMore || error) 1 else 0

    companion object {
        private val dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale(UserInfo.language.literal, UserInfo.sysLocal))
    }

    class ResultViewHolder(val userRegistered: (String) -> Boolean, val changeLikeState: (Int, Boolean) -> Unit, val view: View) : RecyclerView.ViewHolder(view) {

        internal val follow = view.findViewById<ImageView>(R.id.follow)
        internal val logo = view.findViewById<ImageView>(R.id.logo)
        internal val nameFilm = view.findViewById<TextView>(R.id.nameFilm)
        internal val category = view.findViewById<TextView>(R.id.category)
        internal val count_movies = view.findViewById<TextView>(R.id.count_movies)
        var item: Event? = null



        fun bind(event: Event) {
            item = event
            event.let { item ->
                var requestOptions = RequestOptions()
                requestOptions = requestOptions.transforms(CenterCrop(), RoundedCorners(6))

                GlideApp.with(view.context)
                        .load(RestApi.base + "/mult/assets/images/"+ item.movies?.get(0) +"/0.png")
                        .apply(requestOptions)
                        .error(R.drawable.ic_default)
                        .into(logo)

                nameFilm.text= item.title
                category.text = item.category?.name

                val size_movies = item.movies?.size
                count_movies.visibility = if(size_movies!! > 1) View.VISIBLE else View.GONE
                count_movies.text = size_movies.toString()

                follow.setOnClickListener {
                    val checked = !follow.isActivated
                    if (checked) {
                        if (userRegistered("favorite")) {
                            follow.isActivated = checked
                            //item.favorite = checked

                            changeLikeState(item.id.id, checked)
                            this.itemView.context.longToast(R.string.ico_added_to_favorite)
                        } else {
                            follow.isActivated = !checked
                        }
                    } else {
                        val alert = view.context.alert(view.context.getString(R.string.unfollow_event, item.title)) {
                            yesButton {
                                //item.favorite = checked
                                follow.isActivated = checked
                                changeLikeState(item.id.id, checked)
                                this@ResultViewHolder.itemView.context.longToast(R.string.ico_removed_from_favorite)
                            }
                            noButton { follow.isActivated = !checked }
                        }.build()

                        alert.setCancelable(false)
                        alert.show()
                    }
                }

                follow.isActivated = item.id.favorite


                view.setOnClickListener {
                    showDetails(eventId = item.id.id, name = item.title!!)
                }


            }
        }

        private fun showDetails(eventId: Int, name: String = "") {
            (itemView.context).startActivity<EventDetailsActivity>(
                    "EVENT_ID" to eventId,
                    "EVENT_NAME" to (name)
            )
        }
    }

    inner class FooterHolder(containerView: View) : RecyclerView.ViewHolder(containerView) {

        private val retry = containerView.findViewById<ImageButton>(R.id.retryButton)
        private val card = containerView.findViewById<CardView>(R.id.card)
        private val progressBar = containerView.findViewById<ProgressBar>(R.id.progressBar)
        private val errorText = containerView.findViewById<TextView>(R.id.errorText)

        fun bind() {
            Log.i("FOOTER", "I'm footer!")

            retry.setOnClickListener { reloadAction() }

            card.setOnClickListener { retry.performClick() }

            if (waitingMore) {
                card.visibility = View.GONE
                progressBar.visibility = View.VISIBLE
            } else {
                card.visibility = View.VISIBLE
                progressBar.visibility = View.GONE
                errorText.setText(
                        if (serverError) R.string.server_dialog_title else R.string.network_dialog_title
                )
            }


        }
    }
}