package com.mult.film.search.presenter

import android.util.Log
import com.arellomobile.mvp.InjectViewState
import com.mult.film.common.UserInfo
import com.mult.film.common.api.RestApi
import com.mult.film.common.presenter.LoadMorePresenter
import com.mult.film.data.db.Event
import com.mult.film.data.models.ListResponse
import com.mult.film.search.view.SearchView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import ru.gildor.coroutines.retrofit.awaitResult
import ru.gildor.coroutines.retrofit.getOrThrow

/**
 * Created by user on 04.10.2017.
 */
@InjectViewState
class SearchPresenter : LoadMorePresenter<Event, SearchView>() {
    suspend override fun loadDataByPage(page: Int): ListResponse<Event> {
        return RestApi.search(page, query).awaitResult().getOrThrow()
    }

    private var query = ""
    private val periods = mutableListOf<String>()

    override fun updateDB(items: List<Event>) {
        Log.i("SearchP", "${items.size}")
    }

    /*suspend override fun loadDataByPage(page: Int): Call<SearchResponse> {
        return RestApi.search(query, page, UserInfo.language.literal, periods)
    }*/

    fun setQuery(query: String, periods: List<String> = listOf()) {
        if (query.isEmpty()) {
            emptyData()
        } else {
            if (query != "}|{")
                this.query = query

            if (!this.periods.toTypedArray().contentEquals(periods.toTypedArray())) {
                if (periods.isNotEmpty() && periods.first() != "skip") {
                    this.periods.clear()
                    this.periods.addAll(periods)
                }

                loadData()
            }
        }
    }

    fun setQueryObservable(queryObservable: Observable<CharSequence>) {
        queryObservable
                .switchMap { Observable.just(it.toString()) }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    setQuery(query = it, periods = listOf("skip"))
                }, {
                    it.printStackTrace()
                })
    }

}