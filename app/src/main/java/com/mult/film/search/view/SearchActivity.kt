package com.mult.film.search.view

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.mult.film.R
import com.mult.film.common.LanguageContextWrapper
import com.mult.film.common.UserInfo
import com.mult.film.common.showAllowingStateLoss
import com.mult.film.common.viewUtils.EndlessRecyclerViewScrollListener
import com.mult.film.data.db.Event
import com.mult.film.dialogs.AuthRequiredDialog
import com.mult.film.repositories.EventsRepository
import com.mult.film.search.adapter.SearchAdapter
import com.mult.film.search.presenter.SearchPresenter
import com.jakewharton.rxbinding2.widget.queryTextChanges
import kotlinx.android.synthetic.main.activity_search.*
import org.jetbrains.anko.find
import java.util.concurrent.TimeUnit

/**
 * Created by user on 04.10.2017.
 */
class SearchActivity : MvpAppCompatActivity(), SearchView {

    private val adapter = SearchAdapter(
            { id, checked ->
                EventsRepository.changeLikeStatus(id, checked) {}
            },
            { type ->
                UserInfo.registered.also {
                    if (!it) AuthRequiredDialog()
                            .apply {
                                arguments = Bundle().apply {
                                    putString("types", type)
                                }
                            }
                            .showAllowingStateLoss(this.supportFragmentManager, "")
                }
            },
            {presenter.setQuery("}|{", listOf("skip"))    })

    override fun setData(data: List<Event>) {
        emptyContent.visibility = if(data.isEmpty()) View.VISIBLE  else  View.GONE
        resultList.post { adapter.setData(data) }


    }

    override fun showEmptyContentError() {
        emptyContent.visibility = View.VISIBLE
    }

    override fun appendData(data: List<Event>) {

        resultList.post { adapter.appendData(data) }
    }

    override fun setServerError(error: Boolean) {
        adapter.serverError = true
        adapter.showErrorFooter()
        resultList.post { adapter.notifyItemChanged(adapter.itemCount - 1) }
    }

    override fun showFooter() {
        resultList.post { adapter.showFooter() }
    }

    override fun hideFooter() {
        resultList.post { adapter.hideFooter() }
    }

    override fun showErrorFooter() {
        resultList.post { adapter.showErrorFooter() }
    }

    override fun hideErrorFooter() {
        resultList.post { adapter.hideErrorFooter() }
    }

    @InjectPresenter
    lateinit var presenter: SearchPresenter

    private var lastQuery = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.title = ""
        toolbar.setNavigationOnClickListener { onBackPressed() }


        resultList.layoutManager = LinearLayoutManager(this)
        resultList.adapter = adapter

        val scrollListener =
                object : EndlessRecyclerViewScrollListener(resultList.layoutManager as LinearLayoutManager) {
                    override fun getFooterViewType(defaultNoFooterViewType: Int) = 666

                    override fun onLoadMore(page: Int, totalItemsCount: Int) {
                        presenter.loadMore(page = page)
                    }

                }
        resultList.addOnScrollListener(scrollListener)
        searchBar.findViewById<View>(R.id.search_button)?.performClick()
        searchBar.onActionViewExpanded()

        val queryObservable = searchBar
                .queryTextChanges()
                .skipInitialValue()
                .debounce(200, TimeUnit.MILLISECONDS)
                .doOnNext { lastQuery = it.toString() }

        presenter.setQueryObservable(queryObservable)

        searchBar.setOnCloseListener {
            searchBar.findViewById<View>(R.id.search_button)?.performClick()
            true
        }


        val searchViewPlateId = searchBar.context.resources.getIdentifier("android:id/search_src_text", null, null)
        searchBar.find<EditText>(searchViewPlateId).setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH){
                searchBar.setQuery(lastQuery, true)
                true
            } else false
        }
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.app_bar_search -> {
                true
            }
            else -> false
        }
    }
}