package com.mult.film.filter.view

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.mult.film.data.models.CategorySetting

/**
 * Created by user on 18.09.2017.
 */
interface FilterView : MvpView {
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun setData(categories: List<CategorySetting>)

    fun enableApply(enable: Boolean)
}