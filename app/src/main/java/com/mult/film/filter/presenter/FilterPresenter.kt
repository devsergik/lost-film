package com.mult.film.filter.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.mult.film.common.UserInfo
import com.mult.film.data.db.DataBaseRoom
import com.mult.film.data.models.CategorySetting
import com.mult.film.filter.view.FilterView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

/**
 * Created by user on 18.09.2017.
 */
@InjectViewState
class FilterPresenter : MvpPresenter<FilterView>() {

    private val checkedCategories = mutableSetOf<Int>()
    private val categories = mutableSetOf<CategorySetting>()
    private val initialState = mutableSetOf<Int>()
    private val dataBaseRoom = DataBaseRoom.getInstance()
    private var hasBounty = false


    fun loadCategories() {
        GlobalScope.launch(Dispatchers.IO) {

            val checked_list =  dataBaseRoom.categoriesDao().checkedIds()
            val category_list =  dataBaseRoom.categoriesDao().getAllCategories().map{
                CategorySetting(it.id, it.name, it.checked)

            }
            withContext(Dispatchers.Main){
                checkedCategories.clear()
                categories.clear()

                checkedCategories.addAll(checked_list)
                initialState.addAll(checkedCategories)

                viewState.setData(category_list)
                categories.addAll(category_list)
                viewState.enableApply(false)
            }

        }
    }

    fun changeFilterState(categoryId: Int, checked: Boolean) {
        checkedCategories.apply {
            if (checked) add(categoryId) else remove(categoryId)
        }

        checkApplyEnable()
    }

    private fun checkApplyEnable() {
        viewState.enableApply(
                !checkedCategories.toTypedArray().contentEquals(initialState.toTypedArray()))
    }

    fun applyChanges() {
        UserInfo.bountyFilter = hasBounty
        GlobalScope.launch(Dispatchers.IO) {
            val category=  dataBaseRoom.categoriesDao()
            category.updateReset()
            checkedCategories.forEach {
                category.updateCategory(it)
            }
        }
    }


    fun reset() {
        checkedCategories.clear()
        categories.forEach { it.checked = false }
        hasBounty = false
        viewState.setData(categories.toList())
        checkApplyEnable()
    }
}