package com.mult.film.filter.view

import android.graphics.Color
import android.os.Bundle
import android.os.Parcelable
import android.support.design.widget.BottomSheetDialog
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.mult.film.R
import com.mult.film.common.view.MvpBottomSheetDialogFragment
import com.mult.film.filter.adapter.FilterAdapter
import com.mult.film.filter.presenter.FilterPresenter
import kotlinx.android.synthetic.main.fragment_category_filter.*
import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.CoordinatorLayout
import android.support.v4.app.DialogFragment
import android.widget.FrameLayout
import android.support.v4.content.ContextCompat
import android.view.MotionEvent
import com.mult.film.common.viewUtils.UserLockBottomSheetBehavior
import com.mult.film.data.models.CategorySetting
import com.jakewharton.rxbinding2.widget.checkedChanges
import org.jetbrains.anko.textColor


/**
 * Created by user on 18.09.2017.
 */
class FilterFragment : MvpBottomSheetDialogFragment(), FilterView {

    @InjectPresenter
    lateinit var presenter: FilterPresenter
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<out View>
    var applyListener: () -> Unit = {}

    private var scrollState: Parcelable? = null

    override fun setData(categories: List<CategorySetting>) {
        val adapter = FilterAdapter(categories, { id, isChecked ->
            presenter.changeFilterState(categoryId = id, checked = isChecked)
        }, colors = ContextCompat.getColor(context!!, R.color.grey) to ContextCompat.getColor(context!!, R.color.green))

        filters.adapter = adapter
        adapter.notifyDataSetChanged()

        scrollState?.let { filters.layoutManager?.onRestoreInstanceState(it) }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_category_filter, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.AppTheme)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        filters.layoutManager = LinearLayoutManager(context)
        presenter.loadCategories()

        val container = view.findViewById<View>(R.id.container)

        container?.layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                activity?.resources?.displayMetrics?.heightPixels ?: 0 - statusBarHeight)
        container?.setOnTouchListener { v, event ->
            v.parent.requestDisallowInterceptTouchEvent(true)
            when (event.action and MotionEvent.ACTION_MASK) {
                MotionEvent.ACTION_UP -> v.parent.requestDisallowInterceptTouchEvent(false)
            }
            false
        }
        container?.invalidate()



        dismiss.setOnClickListener {
            dismissAllowingStateLoss()
        }
        cancel.setOnClickListener {dismiss.performClick() }

        reset.setOnClickListener {
            scrollState = (filters.layoutManager as LinearLayoutManager).onSaveInstanceState()
            presenter.reset()
            presenter.applyChanges()
            applyListener.invoke()
            dismissAllowingStateLoss()
        }

        apply.setOnClickListener {
            presenter.applyChanges()
            applyListener.invoke()
            dismissAllowingStateLoss()
        }


    }

    override fun enableApply(enable: Boolean) {
        apply.isEnabled = enable
    }


    private val statusBarHeight: Int
        get() {
            var result = 0
            val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
            if (resourceId > 0) {
                result = resources.getDimensionPixelSize(resourceId)
            }
            return result
        }



    override fun onResume() {
        super.onResume()

        val d = dialog as BottomSheetDialog
        val bottomSheet = d.findViewById<FrameLayout>(android.support.design.R.id.design_bottom_sheet) as FrameLayout
        (bottomSheet.layoutParams as CoordinatorLayout.LayoutParams).behavior = UserLockBottomSheetBehavior<FrameLayout>()

        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
        bottomSheetBehavior.apply {
            state = BottomSheetBehavior.STATE_EXPANDED
            setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
                override fun onSlide(bottomSheet: View, slideOffset: Float) {
                    //Log.i("BottomSH", "I'm sliding")
                }

                override fun onStateChanged(bottomSheet: View, newState: Int) {
                    if (newState == BottomSheetBehavior.STATE_HIDDEN)
                        dialog.cancel()


                }
            })
            this.skipCollapsed = true
        }


    }


}