package com.mult.film.filter.view

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.os.Parcelable
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.mult.film.R
import com.mult.film.common.LanguageContextWrapper
import com.mult.film.common.UserInfo
import com.mult.film.common.checkScreenSize
import com.mult.film.filter.adapter.FilterAdapter
import com.mult.film.filter.presenter.FilterPresenter
import com.mult.film.data.models.CategorySetting
import com.jakewharton.rxbinding2.widget.checkedChanges
import kotlinx.android.synthetic.main.fragment_category_filter.*
import org.jetbrains.anko.textColor

/**
 * Created by user on 13.12.2017.
 */
class FilterActivity : MvpAppCompatActivity(), FilterView {

    @InjectPresenter
    lateinit var presenter: FilterPresenter

    private var scrollState: Parcelable? = null

    override fun setData(categories: List<CategorySetting>) {
        val adapter = FilterAdapter(categories, { id, isChecked ->
            presenter.changeFilterState(categoryId = id, checked = isChecked)
        }, colors = ContextCompat.getColor(this, R.color.grey) to ContextCompat.getColor(this, R.color.green))

        filters.adapter = adapter
        adapter.notifyDataSetChanged()

        scrollState?.let { filters.layoutManager?.onRestoreInstanceState(it) }
    }

    override fun enableApply(enable: Boolean) {
        apply.isEnabled = enable
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_category_filter)

        overridePendingTransition(R.anim.abc_slide_in_bottom, R.anim.stay)

        filters.layoutManager = LinearLayoutManager(this)
        presenter.loadCategories()



        dismiss.setOnClickListener {
            overridePendingTransition(R.anim.stay, R.anim.abc_slide_out_bottom)
            finish()
        }
        cancel.setOnClickListener { dismiss.performClick() }

        reset.setOnClickListener {
            scrollState = (filters.layoutManager as LinearLayoutManager).onSaveInstanceState()
            presenter.reset()
            presenter.applyChanges()
            finish()
        }

        apply.setOnClickListener {
            presenter.applyChanges()
            finish()
        }

        checkScreenSize(content)
    }



    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.stay, R.anim.abc_slide_out_bottom)
    }
}