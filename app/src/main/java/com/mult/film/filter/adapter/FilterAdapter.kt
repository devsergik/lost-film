package com.mult.film.filter.adapter

import android.graphics.Color
import android.graphics.PorterDuff
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.ImageView
import android.widget.TextView
import com.mult.film.R
import com.mult.film.data.models.CategorySetting
import org.jetbrains.anko.find
import org.jetbrains.anko.layoutInflater
import org.jetbrains.anko.textColor

/**
 * Created by user on 18.09.2017.
 */
class FilterAdapter(
        private val categories: List<CategorySetting>,
        val checkListener: (Int, Boolean) -> Unit,
        val colors: Pair<Int, Int>) : RecyclerView.Adapter<FilterAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.context.layoutInflater.inflate(R.layout.item_filter_list, parent, false))
    }

    override fun getItemCount() = categories.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(categories[position])
    }


    override fun onViewDetachedFromWindow(holder: ViewHolder) {
        super.onViewDetachedFromWindow(holder)
        holder.category.setOnCheckedChangeListener(null)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val category = view.find<CheckBox>(R.id.category)
        val logo = view.find<ImageView>(R.id.logo)
        val name = view.find<TextView>(R.id.name)

        fun bind(category: CategorySetting) {
            this.name.text = category.name

/*
            GlideApp.with(itemView.context)
                    .load(RestApi.test + category.picture)
                    .into(object : SimpleTarget<Drawable>() {
                        override fun onResourceReady(resource: Drawable, transition: Transition<in Drawable>?) {
                            resource.setColorFilter(colors.first, android.graphics.PorterDuff.Mode.MULTIPLY)
                            logo.setImageDrawable(resource)
                            performState(category.checked!!, this@ViewHolder.category)
                        }

                    })
*/


            itemView.setOnClickListener { this.category.performClick() }
            this.category.setOnCheckedChangeListener { checkBox, isChecked ->
                performState(isChecked, checkBox)
                checkListener(category.id!!, isChecked)
                categories.find { it == category }?.checked = isChecked
            }
            this.category.isChecked = category.checked!!
        }

        private fun performState(isChecked: Boolean, checkBox: CompoundButton) {
            when (isChecked) {
                true -> {
                    val color = colors.second
                    logo.drawable.setColorFilter(color, PorterDuff.Mode.MULTIPLY)
                    name.textColor = color
                }
                else -> {
                    val color = colors.first
                    logo.drawable.setColorFilter(color, PorterDuff.Mode.MULTIPLY)
                    name.textColor = Color.BLACK
                }
            }
        }

    }
}