package com.mult.film.data.models

import com.mult.film.common.CategoryParams
import com.mult.film.common.FavParams


data class EventResponse(
        @FavParams var id: ParamsModel,
        val information: Information,
        val movies : List<VideoModel> = emptyList()
)

data class Information(
        val title: String? = "",
        val desc: String? = "",
        @CategoryParams
        val category: CategorySetting?,
        val series: Int? = 0
)


data class VideoModel(
        val id: Int? = 0,
        val title: String? = "",
        val movie: String? = "",
        val duration: Int?  = 0,
        val rating: Int? = 0,
        val like : Int? = 0
)



