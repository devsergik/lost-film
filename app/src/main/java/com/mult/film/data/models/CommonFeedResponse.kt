package com.mult.film.data.models

/**
 * Created by user on 15.11.2017.
 */
class CommonNewsItem(
        val ico: IcoData,
        override val id: Int = -1,
        override val link: String = "",
        override val name: String = "",
        override val picture: String = "",
        override val datetime: Int = 0,
        override var state: String = "default"
) : NewsItem(id, link, name, picture, datetime, state = state)


class IcoData(
        val id: Int = 0,
        val name: String? = null,
        val logo: String? = null
)

class CommonFeedResponse(
        override val items: List<CommonNewsItem>,
        override val pages: Int = 0,
        override val page: Int = 0,
        override val size: Int = 0
) : ListResponse<CommonNewsItem>(items, page, pages,size)