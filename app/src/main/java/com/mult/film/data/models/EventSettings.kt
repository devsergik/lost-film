package com.mult.film.data.models


data class EventSettings (
        val auth: AuthResponse,
        val categories: List<Category>?  = listOf(),
        val message: String? = ""
)

class Category(
        var id: Int? = 0,
        var name: String? = "",
        var checked: Boolean? = false
)