package com.mult.film.data.db

import com.mult.film.common.CategoryParams
import com.mult.film.common.FavParams
import com.mult.film.data.models.CategorySetting
import com.mult.film.data.models.ParamsModel

/**
 * Created by user on 11.09.2017.FavParams
 */
class Event(
        @FavParams var id: ParamsModel,
        var title: String? = "",
        @CategoryParams var category: CategorySetting? = null,
        var movies: List<Int>? = listOf())