package com.mult.film.data.models

/**
 * Created by user on 04.10.2017.
 */
open class ListResponse<T>(
        open val items: List<T> = listOf(),
       // open val page: Int = 0,
        open val page: Int? = 0,
        open val pages: Int? = 0,
        open val size: Int? = 0
        )