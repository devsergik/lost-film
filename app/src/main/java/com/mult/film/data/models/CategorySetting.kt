package com.mult.film.data.models

class CategorySetting(
        var id: Int? = 0,
        var name: String? = "",
        var checked: Boolean? = false
)
