package com.mult.film.data.db.model.settings

import android.arch.persistence.room.*
import io.reactivex.Flowable

@Dao
interface CategoriesDao {

    @Query("SELECT * FROM categories")
    fun getAllCategories(): List<CategoriesBase>

    @Query("SELECT * FROM categories")
    fun getAllFlowableCategory(): Flowable<List<CategoriesBase>>


    @Query("SELECT id FROM categories WHERE checked = 1")
    fun checkedIds(): List<Int>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCategories(listCategories: List<CategoriesBase>)

    @Query("UPDATE categories SET checked= 1 WHERE id = :id")
    fun updateCategory(id: Int)

    @Query("UPDATE categories SET checked = 0")
    fun updateReset()

    @Query("DELETE FROM categories")
    fun deleteCategories()
}
