package com.mult.film.data.db.model.settings

import android.arch.persistence.room.*

@Entity(tableName = "categories")
data class CategoriesBase(
        @PrimaryKey
        @ColumnInfo(name = "id")
        val id: Int = 0,

        @ColumnInfo(name = "name")
        val name: String,

        @ColumnInfo(name = "checked")
        val checked: Boolean )
