package com.mult.film.data.models

/**
 * Created by user on 01.11.2017.
 */
class DocumentResponse(
        val content: String,
        val error: Int
)