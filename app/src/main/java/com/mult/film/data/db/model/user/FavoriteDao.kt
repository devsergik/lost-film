package com.mult.film.data.db.model.user

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query

@Dao
interface FavoriteDao {

    @Query("SELECT id FROM fav_user")
    fun getFavTypesAll(): List<Int>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFavUser(listAlertTypes: List<FavoriteBase>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFavUser(listAlertTypes: FavoriteBase)

    @Query("DELETE FROM fav_user WHERE id=:id")
    fun deleteFavUserID(id :Int)

    @Query("DELETE FROM fav_user")
    fun deleteFavUser()
}
