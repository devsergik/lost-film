package com.mult.film.data.models


data class AuthResponse (
        val result: Boolean? = false,
        val user: UserParams = UserParams(),
        val message: String? = ""
)

data class UserParams(
        val id: Int? = 0,
        val email: String? = "",
        val token: String? = "",
        val favorites: List<Int>? = listOf()
)
