package com.mult.film.data.models
import com.mult.film.data.db.Event

/**
 * Created by user on 04.10.2017.
 */

class SearchResponse(
        override val items: List<Event>,
      //  override val page: Int = 0,
        override val page: Int = 0,
        override val pages: Int = 0,
        override val size: Int = 0
) : ListResponse<Event>(items, page, pages, size)