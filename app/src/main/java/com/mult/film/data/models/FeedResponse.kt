package com.mult.film.data.models

class NewsResponse(
        override val items: List<NewsItem>,
        override val page: Int = 0,
        override val pages: Int = 0,
        override val size: Int = 0
) : ListResponse<NewsItem>(items, page, pages, size)

open class NewsItem {
    open val id: Int
    open val link: String
    open val name: String
    open val picture: String
    open val datetime: Int
    open var state: String
    var isNew: Boolean

    constructor(recordId: Int = -1, articleId: Int = -1, articleLink: String = "", title: String = "", image: String = "", publicationTime: Int = 0, type: String = "") {
        this.id = articleId
        this.link = articleLink
        this.name = title
        this.picture = image
        this.datetime = publicationTime
        isNew = false
        state = "default"
    }

    constructor(
            id: Int = -1,
            articleLink: String = "",
            title: String = "",
            image: String = "",
            publicationTime: Int = 0,
            isNew: Boolean = false,
            state: String = "default") {
        this.id = id
        this.link = articleLink
        this.name = title
        this.picture = image
        this.datetime = publicationTime
        this.isNew = isNew
        this.state = state
    }
}

data class NewsCategory(
        val name: String = "",
        val id: Int = 0
)
