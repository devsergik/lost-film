package com.mult.film.data.db.model.user

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey


@Entity(tableName = "fav_user")
data class FavoriteBase(
        @PrimaryKey
        @ColumnInfo(name = "id")
        var id: Int = 0)