package com.mult.film.data.models

class ParamsModel (
        var id: Int = -1,
        var favorite: Boolean = false
)