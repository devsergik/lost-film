package com.mult.film.data.models

/**
 * Created by user on 17.10.2017.
 */
class ReviewItem(
        val id: Int = 0,
        val link: String = "",
        val title: String = "",
        val image: String = "",
        val publicationTime: Int = 0,
        val source: String = ""
)

class ReviewResponse(
        val items: List<ReviewItem>,
        val error: Int = 0
)