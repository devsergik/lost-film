package com.mult.film.data.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import com.mult.film.data.db.model.settings.CategoriesBase
import com.mult.film.data.db.model.settings.CategoriesDao
import com.mult.film.app.MarketApp
import com.mult.film.data.db.model.user.FavoriteBase
import com.mult.film.data.db.model.user.FavoriteDao


@Database(entities = arrayOf(
        FavoriteBase::class,
        CategoriesBase::class),
        version = 1, exportSchema = false)
abstract class DataBaseRoom : RoomDatabase() {

    abstract fun categoriesDao(): CategoriesDao
    //Data Users
    abstract fun favoritesUserDao(): FavoriteDao


    companion object {
        @Volatile private var INSTANCE: DataBaseRoom? = null

        fun getInstance(): DataBaseRoom =
                INSTANCE ?: synchronized(this) {
                    INSTANCE ?: buildDatabase().also { INSTANCE = it }
                }

        private fun buildDatabase() =
                Room.databaseBuilder(MarketApp.applicationContext(),
                        DataBaseRoom::class.java, "setting.db")
                        .build()
    }
}
