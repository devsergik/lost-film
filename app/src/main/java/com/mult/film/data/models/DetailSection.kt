package com.mult.film.data.models

enum class DetailSection {
    INFORMATION,
    REVIEWS
}