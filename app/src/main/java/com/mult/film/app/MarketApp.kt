package com.mult.film.app

import android.app.Application
import android.content.Context
import android.content.pm.PackageManager
import android.support.multidex.MultiDex
import android.support.v7.app.AppCompatDelegate
import android.util.Base64
import android.util.Log
import com.appsflyer.AppsFlyerConversionListener
import com.appsflyer.AppsFlyerLib
import com.mult.film.common.AF_DEV_KEY
import com.mult.film.common.Hex
import com.twitter.sdk.android.core.DefaultLogger
import com.twitter.sdk.android.core.Twitter
import com.twitter.sdk.android.core.TwitterAuthConfig
import com.twitter.sdk.android.core.TwitterConfig
import com.vk.api.sdk.VK
import java.security.MessageDigest



class MarketApp : Application() {

    init {
        instance = this
    }

    companion object {
        private var instance: MarketApp? = null

        fun applicationContext(): Context {
            return instance!!.applicationContext
        }

        fun hex(): String {
            var h1: String? = null
            try {
                val packageInfo = instance?.getPackageManager()?.getPackageInfo(
                        instance?.getPackageName(),
                        PackageManager.GET_SIGNATURES)
                for (signature in packageInfo?.signatures!!) {
                    val md = MessageDigest.getInstance("SHA")
                    md.update(signature.toByteArray())
                    h1 = Base64.encodeToString(md.digest(), Base64.DEFAULT)
                }
            } catch (e: Exception) {
            }

            return h1!!
        }
    }


    override fun onCreate() {
        super.onCreate()
        initVk()
        initTwitter()
        initAppsFlyer()

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

    }


    private fun initVk() {
        VK.initialize(this)
    }


    private fun initTwitter() {
        val config = TwitterConfig.Builder(this)
                .logger(DefaultLogger(Log.DEBUG))
                .twitterAuthConfig(TwitterAuthConfig("Q6loy8AJOVJ1hMcV39dvGDVSO", "fTN2SJLXbGUtTrbDlIVSNcgTjnMchuE0bRnSAq2CzCPjXUHSas"))
                // .debug(true)
                .build()

        Twitter.initialize(config)
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    fun initAppsFlyer() {
        val conversionDataListener = object : AppsFlyerConversionListener {
            override fun onAttributionFailure(errorMessage: String?) {

            }

            override fun onInstallConversionDataLoaded(conversionData: MutableMap<String, String>?) {
            }

            override fun onInstallConversionFailure(errorMessage: String?) {
            }

            override fun onAppOpenAttribution(conversionData: MutableMap<String, String>?) {
            }

        }

        AppsFlyerLib.getInstance().init(AF_DEV_KEY, conversionDataListener)
        AppsFlyerLib.getInstance().enableUninstallTracking("962195180527")
        AppsFlyerLib.getInstance().startTracking(this)
    }
}