package com.mult.film.dialogs

import android.graphics.Color
import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.BottomSheetDialog
import android.support.design.widget.BottomSheetDialogFragment
import android.support.design.widget.CoordinatorLayout
import android.support.v4.content.ContextCompat
import android.support.v4.graphics.drawable.DrawableCompat
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.mult.film.R
import com.mult.film.account.activity.LoginEmailActivity
import com.mult.film.account.activity.SocialLoginActivity
import com.mult.film.common.viewUtils.UserLockBottomSheetBehavior
import kotlinx.android.synthetic.main.dialog_regestration_required.*
import org.jetbrains.anko.support.v4.startActivity

/**
 * Created by user on 09.10.2017.
 */
class AuthRequiredDialog : BottomSheetDialogFragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_regestration_required, container, false)
    }

    private val statusBarHeight: Int
        get() {
            var result = 0
            val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
            if (resourceId > 0) {
                result = resources.getDimensionPixelSize(resourceId)
            }
            return result
        }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val container = view.findViewById<View>(R.id.container)

        container?.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, activity?.resources?.displayMetrics?.heightPixels ?: 0)
        container?.setOnTouchListener { v, event ->
            v.parent.requestDisallowInterceptTouchEvent(true)
            when (event.action and MotionEvent.ACTION_MASK) {
                MotionEvent.ACTION_UP -> v.parent.requestDisallowInterceptTouchEvent(false)
            }
            false
        }
        container?.invalidate()

        DrawableCompat.setTint(authButton.background, Color.WHITE)
        authButton.setOnClickListener {
            startActivity<LoginEmailActivity>() // TODO social SocialLoginActivity
            dismiss()
        }

        cancel.setOnClickListener { dismiss() }

        arguments?.let { args ->
            if (args.getString("types") == "alert") {
                context?.let { ctx ->
                    star.setImageDrawable(ContextCompat.getDrawable(ctx, R.drawable.big_alert))
                }
                title.text = getString(R.string.registration_required_title_alert)
            }
        }

    }

    override fun onResume() {
        super.onResume()
        val d = dialog as BottomSheetDialog
        val bottomSheet = d.findViewById<FrameLayout>(android.support.design.R.id.design_bottom_sheet) as FrameLayout
        (bottomSheet.layoutParams as CoordinatorLayout.LayoutParams).behavior = UserLockBottomSheetBehavior<FrameLayout>()

        val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
        bottomSheetBehavior.apply {
            state = BottomSheetBehavior.STATE_EXPANDED
            setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
                override fun onSlide(bottomSheet: View, slideOffset: Float) {
                    //Log.i("BottomSH", "I'm sliding")
                }

                override fun onStateChanged(bottomSheet: View, newState: Int) {
                    if (newState == BottomSheetBehavior.STATE_HIDDEN)
                        dialog.cancel()


                }
            })
            this.skipCollapsed = true
        }


    }
}