package com.mult.film.dialogs

import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import com.mult.film.R
import com.mult.film.common.setBackgroundTintWithCornersSate
import kotlinx.android.synthetic.main.dialog_feedback.*

/**
 * Created by user on 20.11.2017.
 */
class DialogFeedback : DialogFragment() {
    private var sendClickListener: () -> Unit = {}
    private var cancelClickListener: () -> Unit = {}

    fun send(action: () -> Unit) {
        sendClickListener = action
    }

    fun cancel(action: () -> Unit) {
        cancelClickListener = action
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
            inflater.inflate(R.layout.dialog_feedback, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sendButton.setOnClickListener { sendClickListener() }
        cancelButton.setOnClickListener { cancelClickListener() }
        context?.setBackgroundTintWithCornersSate(cancelButton, Color.WHITE)

    }

    val feedbackText: String
        get() = feedback.text.toString()

    override fun onDismiss(dialog: DialogInterface?) {
        super.onDismiss(dialog)
        cancelClickListener()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.window.requestFeature(Window.FEATURE_NO_TITLE)
        return dialog
    }
}