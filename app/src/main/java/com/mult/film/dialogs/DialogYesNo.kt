package com.mult.film.dialogs

import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Button
import android.widget.TextView
import com.mult.film.R
import com.mult.film.common.setBackgroundTintWithCornersSate
import org.jetbrains.anko.find

/**
 * Created by user on 20.11.2017.
 */
class DialogYesNo: DialogFragment() {

    private var yesClickListener: () -> Unit = {}
    private var noClickListener: () -> Unit = {}
    private var dismissListener: () -> Unit = {}
    private var title = ""
    private var yesText = ""
    private var noText = ""

    fun setTitle(text: String){
        title = text
    }

    fun yes(text: String, action: () -> Unit){
        yesText = text
        yesClickListener = action
    }

    fun no(text: String, action: () -> Unit){
        noText = text
        noClickListener = action
    }

    fun doOnDismiss(action: () -> Unit){
        dismissListener = action
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.window.requestFeature(Window.FEATURE_NO_TITLE)
        return dialog
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
            inflater.inflate(R.layout.dialog_yes_no, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.find<TextView>(R.id.title).apply {
            text = title
        }

        view.find<Button>(R.id.yesButton).apply {
            text = yesText
            setOnClickListener {
                yesClickListener()
                dismiss()
            }

        }

        view.find<Button>(R.id.noButton).apply {
            text = noText
            setOnClickListener {
                noClickListener()
                dismiss()
            }
            context.setBackgroundTintWithCornersSate(this, Color.WHITE)
        }
    }

    override fun onDismiss(dialog: DialogInterface?) {
        super.onDismiss(dialog)
        dismissListener()
    }
}