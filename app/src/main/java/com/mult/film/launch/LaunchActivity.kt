package com.mult.film.launch

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Bundle
import android.support.v4.app.NotificationCompat
import android.support.v7.app.AppCompatActivity
import com.mult.film.R
import com.mult.film.common.LanguageContextWrapper
import com.mult.film.common.UserInfo
import com.mult.film.common.UserLanguage
import com.mult.film.eventDetails.view.EventDetailsActivity
import com.mult.film.main.view.MainScreen
import org.jetbrains.anko.configuration
import org.jetbrains.anko.startActivity

class LaunchActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sendNotification("", null)
    }


    override fun onStart() {
        super.onStart()
        val i = if (UserInfo.sawOnboarding) {
            Intent(this@LaunchActivity, MainScreen::class.java)
        } else {
            Intent(this@LaunchActivity, OnboardingActivity::class.java)
        }
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)



        if (intent.extras != null && intent.extras.keySet().contains("types"))
            intent.extras?.let {
                when (it["types"].toString().toLowerCase()) {
                    "alert" -> {
                        startActivity(i)
                        val eventId = it["eventId"].toString().toInt()
                        // AmplitudeHelper.notification("AlertCheck", eventId.toString())
                        startActivity<EventDetailsActivity>(
                                "FROM_NOTIFICATION" to true,
                                "EVENT_ID" to eventId,
                                "PAGE" to 0
                        )
                    }
                    "count" -> {
                        val eventId = if (it.containsKey("eventId")) it["eventId"].toString().toInt() else 0
                        //i.putExtra("PAGE", 3)
                        // AmplitudeHelper.notification("News", eventId)
                        startActivity(i)
                        startActivity<EventDetailsActivity>(
                                "FROM_NOTIFICATION" to true,
                                "EVENT_ID" to eventId,
                                "PAGE" to 1
                        )
                    }
                    "info" -> {
                        startActivity(i)
                    }
                    else -> {
                    }
                }
            }
        else {
            startActivity(i)
        }

    }

    fun sendNotification(title: String, intent: Intent?) {
        val pendingIntent =
                if (intent != null)
                    PendingIntent.getActivity(
                            this,
                            0,
                            intent, PendingIntent.FLAG_ONE_SHOT)
                else PendingIntent.getActivity(
                        this,
                        0,
                        Intent(),
                        PendingIntent.FLAG_UPDATE_CURRENT)


        val channelId = "fcm_default_channel"
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setContentText(title)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build())
    }

}
