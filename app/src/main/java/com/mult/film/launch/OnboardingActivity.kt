package com.mult.film.launch

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.graphics.drawable.DrawableCompat
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.mult.film.R
import com.mult.film.account.activity.LoginEmailActivity
import com.mult.film.account.activity.SocialLoginActivity
import com.mult.film.common.AccountManager
import com.mult.film.common.SigningListener
import com.mult.film.common.UserInfo
import kotlinx.android.synthetic.main.activity_onboarding.*
import org.jetbrains.anko.find
import org.jetbrains.anko.startActivityForResult

/**
 * Created by user on 16.10.2017.
 */
class OnboardingActivity : AppCompatActivity(), SigningListener {

    private var lastPage = -1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_onboarding)

        if (savedInstanceState == null) {
            holder.adapter = Adapter(supportFragmentManager)
            ind_1.isActivated = true

            nextPage.setOnClickListener {
                holder.currentItem += 1
            }

            DrawableCompat.setTint(nextPage.background, Color.WHITE)

            holder.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                override fun onPageScrollStateChanged(state: Int) {
                }

                override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                }

                override fun onPageSelected(position: Int) {
                    if (position == 2) {
                        nextPage.visibility = View.GONE
                        ind_1.visibility = View.GONE
                        ind_2.visibility = View.GONE
                        ind_3.visibility = View.GONE
                    } else {
                        nextPage.visibility = View.VISIBLE
                        ind_1.visibility = View.VISIBLE
                        ind_2.visibility = View.VISIBLE
                        ind_3.visibility = View.VISIBLE
                    }

                    when (position) {
                        0 -> {
                            ind_1.isActivated = true
                            ind_2.isActivated = false
                        }
                        1 -> {
                            ind_1.isActivated = false
                            ind_2.isActivated = true
                        }
                    }
                    lastPage = position
                }

            })

            holder.currentItem = 0
        }
    }

    override fun proceed(registered: Boolean) {
        if (registered){
            UserInfo.sawOnboarding = true
            finish()
        }
    }

    fun signOrLogin() {
        startActivityForResult<LoginEmailActivity>(222) // TODO social SocialLoginActivity
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 222 && resultCode == 333) {
            /*UserInfo.sawOnboarding = true
            AmplitudeHelper.endWelcome3Screen()

            finish()*/
        }
    }

    override fun onResume() {
        super.onResume()
        AccountManager.addSigningListener(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        AccountManager.removeSigningListener(this)
    }

    fun asGuest() {
        UserInfo.sawOnboarding = true
        finish()
    }

    inner class Adapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {
        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> Slide1()
                1 -> Slide2()
                2 -> Slide3()
                else -> Fragment()
            }
        }

        override fun getCount() = 3

    }

    class Slide1 : Fragment() {
        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            return inflater.inflate(R.layout.fragment_onboarding_slide_1, container, false)
        }
    }

    class Slide2 : Fragment() {
        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            return inflater.inflate(R.layout.fragment_onboarding_slide_2, container, false)
        }
    }

    class Slide3 : Fragment() {
        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            return inflater.inflate(R.layout.fragment_onboarding_slide_3, container, false)
        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)

            view.find<Button>(R.id.signOrLogin).background?.let {
                DrawableCompat.setTint(it, Color.WHITE)
            }

            view.find<TextView>(R.id.asGuest).background?.let {
                DrawableCompat.setTint(it, Color.WHITE)
            }

            view.find<Button>(R.id.signOrLogin).setOnClickListener {
                (activity as OnboardingActivity).signOrLogin()
            }

            view.find<TextView>(R.id.asGuest).setOnClickListener {
                (activity as OnboardingActivity).asGuest()
            }
        }
    }
}