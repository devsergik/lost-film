package com.mult.film.main.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.chibatching.kotpref.bulk
import com.mult.film.common.UserInfo
import com.mult.film.common.UserParamsUpdate
import com.mult.film.common.api.RestApi
import com.mult.film.data.db.DataBaseRoom
import com.mult.film.data.db.model.settings.*
import com.mult.film.main.view.MainScreenView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import com.mult.film.data.db.model.user.FavoriteBase

/**
 * Created by user on 08.09.2017.
 */
@InjectViewState
class MainScreenPresenter : MvpPresenter<MainScreenView>() {

    val user_dao = DataBaseRoom.getInstance()

    init {
        GlobalScope.launch(Dispatchers.IO) {
            settingsApp()
            viewState.checkNeedSyncFavorite()
        }
    }

    fun checkHighlight() {
        GlobalScope.launch(Dispatchers.IO) {
            /*val count_checked = user_dao.authorsDao().checkedIds().count() TODO решить проблему
            var is_checked = count_checked > 0
            UserInfo.needHighlightFilter = is_checked*/
            withContext(Dispatchers.Main) {
                viewState.highlightFilter(UserInfo.needHighlightFilter)
            }
        }
    }

    fun settingsApp(){
        RestApi.getSettings(token = UserInfo.token)
                .map {settings ->settings.auth.let {
                    var isAuthorization = false
                    var id_user = 0
                    var token_response = ""
                    var email_response = ""
                    if (it != null)
                        isAuthorization = it.result!!
                    val fav_user_dao = user_dao.favoritesUserDao()
                    fav_user_dao.deleteFavUser()
                    if (isAuthorization) {
                        id_user = it.user?.id!!
                        token_response = it.user.token!!
                        email_response = it.user.email!!

                        val fav = it.user.favorites?.map{
                            FavoriteBase(it)
                        }.orEmpty()
                        fav_user_dao.insertFavUser(fav)

                    }

                    val category_dao = user_dao.categoriesDao()
                    category_dao.deleteCategories()
                    val categories = settings.categories?.map{
                        CategoriesBase(it.id!!, it.name!!, it.checked!!)
                    }.orEmpty()
                    category_dao.insertCategories(categories)
                    UserParamsUpdate.update()

                    UserInfo.bulk {
                        registered = isAuthorization
                        id = id_user
                        this.token = token_response
                        this.email = email_response
                    }
                    }
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    viewState.recreateMainScreen()
                }, {
                    it.printStackTrace() })}


    fun sendFeedback(feedback: String) {
        RestApi.sendNegativeRate(feedback)
    }
}