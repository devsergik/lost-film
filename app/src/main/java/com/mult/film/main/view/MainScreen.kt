package com.mult.film.main.view

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.LayerDrawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatDelegate
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.android.installreferrer.api.InstallReferrerClient
import com.android.installreferrer.api.InstallReferrerStateListener
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.mult.film.BuildConfig
import com.mult.film.R
import com.mult.film.account.activity.AccountActivity
import com.mult.film.account.activity.LoginEmailActivity
import com.mult.film.common.*
import com.mult.film.eventlist.view.EventsFragment
import com.mult.film.eventlist.view.Scrollable
import com.mult.film.filter.view.FilterActivity
import com.mult.film.main.presenter.MainScreenPresenter
import com.mult.film.repositories.EventsRepository
import com.mult.film.repositories.RequestCompleteListener
import com.mult.film.search.view.SearchActivity
import com.mult.film.subscriptions.view.SubscriptionsFragment
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.*
import java.util.*

/**
 * Created by user on 08.09.2017.
 */
class MainScreen : MvpAppCompatActivity(), MainScreenView, RequestCompleteListener, SigningListener, InstallReferrerStateListener {

    override fun proceed(registered: Boolean) {
        runOnUiThread {
            navigationListener(currentTabId)
        }
    }

    private var referrerClient: InstallReferrerClient? = null

    override fun onInstallReferrerSetupFinished(responseCode: Int) {
        when (responseCode) {
            InstallReferrerClient.InstallReferrerResponse.OK -> {
                try {
                    referrerClient?.let { client ->
                        client.endConnection()
                        UserInfo.installInfoSent = true
                    }
                } catch (e: Exception) {

                }
            }
        }
    }

    override fun onInstallReferrerServiceDisconnected() { }

    @InjectPresenter
    lateinit var presenter: MainScreenPresenter
    private var menu: Menu? = null

    private var currentFragment: Fragment? = null
    private var currentTabId = R.id.navigation_now

    init {
        EventsRepository.addRequestCompleteListener(this)
    }

    override fun highlightFilter(highlight: Boolean) {
        menu?.findItem(R.id.action_category_filter)
                ?.let {
                    it.icon as LayerDrawable
                }
                ?.findDrawableByLayerId(R.id.ic_indicator)
                ?.alpha = if (highlight) 255 else 0

    }

    private val fragments = mutableListOf<Fragment>()
    private val fragments2Clear = mutableListOf<Fragment?>()
    private var lastScreen = ""

    private var isFromNotification = false

    private val navigationListener: (Int) -> Unit = { id: Int ->
        menu?.findItem(R.id.action_category_filter)?.isVisible = true
        menu?.findItem(R.id.action_search)?.isVisible = true

        if (currentTabId != -1)
            bottomNavigation.getTabWithId(currentTabId).removeBadge()

        currentTabId = id
        updateTitle(id)

        showTab(id)

        supportFragmentManager.executePendingTransactions()

        if (supportFragmentManager.executePendingTransactions() && currentFragment is Reloadable) {
            (currentFragment as Reloadable).reload()
        }
    }

    override fun onPause() {
        super.onPause()
        AccountManager.removeSigningListener(this)
    }

    override fun onResume() {
        super.onResume()
        presenter.checkHighlight()
        AccountManager.addSigningListener(this)
    }

    private fun updateTitle(id: Int): Boolean {
        val (key, title) = when (id) {
            R.id.navigation_now -> TAB_MULT to getString(R.string.now_title)
            R.id.navigation_subscriptions -> "FAVORITE" to getString(R.string.favorite)
            else -> "" to ""
        }

        var count = EventsRepository.requestItemsCount[key] ?: 0
        supportActionBar?.title = title
        if (key == "FAVORITE" && !UserInfo.registered) count = 0
        bottomNavigation.getTabWithId(id).removeBadge()
        bottomNavigation.getTabWithId(id).setBadgeCount(count)
        Log.i("HAHA", "now $count TV")
        return key.isNotEmpty()
    }

    val flCallbacks = object : FragmentManager.FragmentLifecycleCallbacks() {
        override fun onFragmentCreated(fm: FragmentManager, f: Fragment, savedInstanceState: Bundle?) {
            super.onFragmentCreated(fm, f, savedInstanceState)
            fragments2Clear.add(f)
        }
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //checkIntents()
        window.setBackgroundDrawable(ColorDrawable(Color.WHITE))

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        bottomNavigation.setOnTabSelectListener(navigationListener, false)
        bottomNavigation.setOnTabReselectListener {
            when (it) {
                R.id.navigation_now -> {
                    tabs[tabKeys[0]]
                }
                else -> null
            }?.let { (it as Scrollable).scrollToPosition(0) }
        }
        bottomNavigation.setTabTitleTextAppearance(R.style.bottomBarTextAppearance)
        bottomNavigation.setBadgesHideWhenActive(false)
        bottomNavigation.setBadgeBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryDark))

        fragments.clear()
        fragments.addAll(listOf(
                EventsFragment.getInstance(TAB_MULT),
                SubscriptionsFragment.getInstance()
        ))

        supportFragmentManager.registerFragmentLifecycleCallbacks(flCallbacks, true)

        if (savedInstanceState == null) {
            tabs = createNewFragments()
            supportFragmentManager
                    .beginTransaction()
                    .add(R.id.container, tabs[tabKeys[0]]!!, tabKeys[0])
                    .add(R.id.container, tabs[tabKeys[1]]!!, tabKeys[1])
                    .commit()
        } else {
            tabs = findFragments()
        }

        navigationListener(R.id.navigation_now)

        intent.getIntExtra("PAGE", -1).takeIf { it != -1 }?.let {
            bottomNavigation.selectTabAtPosition(2)
            currentTabId = R.id.navigation_subscriptions
            isFromNotification = true
        }

        checkScreenSize(container, ContextCompat.getColor(this, R.color.card_detailing))

        supportActionBar?.let { bar ->
            bar.setDisplayHomeAsUpEnabled(true)
            bar.setHomeAsUpIndicator(R.drawable.ic_hamburger)
        }

/*        if (UserInfo.needToShowFeedbackDialog()) TODO вызов оценки приложения
            showFeedbackSteps()*/
    }

    override fun checkNeedSyncFavorite() {
        if (UserInfo.oldFavoriteInfoAlreadyShowed) {
            alert(R.string.ask_sync_favorite) {
                positiveButton(R.string.sign_in_or_register) {
                    startActivity<LoginEmailActivity>() // TODO social SocialLoginActivity
                }
            }.show()
            UserInfo.oldFavoriteInfoAlreadyShowed = true
        }
    }

    private lateinit var tabs: HashMap<String, Fragment?>
    private val tabKeys = listOf(
            tabIdToFragmentTag(R.id.navigation_now),
            tabIdToFragmentTag(R.id.navigation_subscriptions)
    )

    private fun tabIdToFragmentTag(id: Int) = "tab_$id"
    private fun createNewFragments(): HashMap<String, Fragment?> = hashMapOf(
            tabKeys[0] to EventsFragment.getInstance(TAB_MULT),
            tabKeys[1] to SubscriptionsFragment.getInstance()
    )

    private fun findFragments(): HashMap<String, Fragment?> = hashMapOf(
            tabKeys[0] to supportFragmentManager.findFragmentByTag(tabKeys[0]),
            tabKeys[1] to supportFragmentManager.findFragmentByTag(tabKeys[1])
    )

    private fun showTab(id: Int) {
        supportFragmentManager.beginTransaction()
                .hide(tabs[tabIdToFragmentTag(R.id.navigation_now)]!!)
                .hide(tabs[tabIdToFragmentTag(R.id.navigation_subscriptions)]!!)
                .show(tabs[tabIdToFragmentTag(id)]!!)
                .commit()

        when (id) {
            R.id.navigation_now -> {
                lastScreen = TAB_MULT
            }
            R.id.navigation_subscriptions -> {
                lastScreen = "SUBS"
                menu?.findItem(R.id.action_category_filter)?.isVisible = false
                menu?.findItem(R.id.action_search)?.isVisible = false
                SubscriptionsFragment.getInstance()
            }
            else -> {
            }
        }
    }

    override fun onStop() {
        super.onStop()
        fragments.clear()
        Log.i("MS", "MainScreen onStop")
        supportFragmentManager.unregisterFragmentLifecycleCallbacks(flCallbacks)
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i("MS", "MainScreen onDestroy")
    }

    override fun onRestart() {
        super.onRestart()
        Log.i("MS", "MainScreen onRestart")
        when (lastRequestCode) {
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        this.menu = menu
        menu.findItem(R.id.action_category_filter).icon = ContextCompat.getDrawable(this, R.drawable.layer_list_filter)
        presenter.checkHighlight()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_category_filter -> {
            showFilterDialog()
            true
        }
        android.R.id.home -> {
            overridePendingTransition(0, 0)
            startActivityForResult<AccountActivity>(666)
            true
        }
        R.id.action_search -> {
            startActivity<SearchActivity>()
            true
        }
        else -> false
    }

    private fun showFilterDialog() {
        overridePendingTransition(0, 0)
        startActivityForResult<FilterActivity>(555)
    }

    override fun onRequestComplete(request: Int, eventID: Int) {
        if (request == 0)
            runOnUiThread {
                currentTabId.takeIf { it > 0 }?.let {
                    //navigationListener(currentTabId)
                    updateTitle(currentTabId)
                }
            }
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        Log.i("MS", "MainScreen onResumeFragments")
    }


    var lastRequestCode = -1
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        lastRequestCode = requestCode
        when (requestCode) {
            666 -> if (::tabs.isInitialized) tabs.values.forEach {
                when (it) {
                    is EventsFragment -> {
                        it.resetActivateStatus()

                    }
                    is SubscriptionsFragment -> {
                        if(UserInfo.registered)
                            it.presenter.loadFavoriteList()
                    }
                }
            }
/*            555 -> {
                fragments.forEach {
                    (it as? EventsFragment)?.presenter?.loadData()
                }
                this@MainScreen.presenter.checkHighlight()
            }*/

            444 -> {
                recreate()
            }


        }

    }

    override fun recreateMainScreen(){
        tabs.values.forEach {
            when (it) {
                is EventsFragment -> {
                    it.reloadData()

                }
                is SubscriptionsFragment -> {
                    if(UserInfo.registered)
                        it.presenter.loadFavoriteList()
                }
            }
        }
    }

    private fun showFeedbackSteps() { // TODO оценка приложения
        supportFragmentManager.themedAlert(getString(R.string.enjoying_im)) {
            yes(this@MainScreen.getString(R.string.yes)) {
                supportFragmentManager.themedAlert(this@MainScreen.getString(R.string.review_pm)) {
                    yes(this@MainScreen.getString(R.string.yes_sure)) {
                        //https://play.google.com/store/apps/details?id=com.EtherGaming.PocketRogues
                        showUrlIntent("https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}")
                        UserInfo.feedbackCompleted()
                    }
                    no(this@MainScreen.getString(R.string.no_thanks)) {
                        UserInfo.feedbackUnCompleted()
                        UserInfo.feedbackUncompletedCount++
                    }
                    doOnDismiss {
                        UserInfo.feedbackUnCompleted()
                    }
                }
            }

            no(this@MainScreen.getString(R.string.not_really)) {
                supportFragmentManager.feedbackDialog {
                    send {
                        presenter.sendFeedback(this.feedbackText)
                        UserInfo.feedbackCompleted()
                        dismiss()
                    }
                    cancel {
                        UserInfo.feedbackUncompletedCount++
                        UserInfo.feedbackUnCompleted(); dismiss()
                    }
                }

            }
            doOnDismiss { UserInfo.feedbackUnCompleted() }
        }
    }
}

