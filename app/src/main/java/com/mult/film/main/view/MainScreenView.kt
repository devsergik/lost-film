package com.mult.film.main.view

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

/**
 * Created by user on 08.09.2017.
 */
interface MainScreenView : MvpView {
    fun highlightFilter(highlight: Boolean)
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun checkNeedSyncFavorite()
    fun recreateMainScreen()
}