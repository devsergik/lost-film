package com.mult.film.eventDetails.adapter.delegates

import android.graphics.drawable.Drawable
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import com.mult.film.R
import com.mult.film.common.ListItem
import com.mult.film.common.glide.GlideApp
import com.mult.film.common.inflate
import org.jetbrains.anko.find
import org.jetbrains.anko.textColor

/**
 * Created by user on 17.11.2017.
 */
class TitleWithButtonAdapterDelegate : AdapterDelegate<MutableList<ListItem>>() {
    override fun onCreateViewHolder(parent: ViewGroup)
            = TitleViewHolder(parent.inflate(R.layout.list_item_title_with_button))

    override fun onBindViewHolder(items: MutableList<ListItem>, position: Int, holder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
        (holder as TitleViewHolder).bind(items[position] as ListItem.TitleWithButtonItem)
    }

    override fun isForViewType(items: MutableList<ListItem>, position: Int)
            = items[position] is ListItem.TitleWithButtonItem

    class TitleViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val title = view.find<TextView>(R.id.title)
        private val buttonTitle = view.find<TextView>(R.id.buttonTitle)
        private val icon = view.find<ImageView>(R.id.icon)

        fun bind(item: ListItem.TitleWithButtonItem) {
            title.text = item.title
            title.gravity = item.align

            buttonTitle.text = item.buttonTitle.toUpperCase()
            buttonTitle.setOnClickListener { item.action() }

            GlideApp.with(itemView.context)
                    .load(item.buttonIcon)
                    .into(object : SimpleTarget<Drawable>() {
                        override fun onResourceReady(resource: Drawable, transition: Transition<in Drawable>?) {
                            resource?.setColorFilter(buttonTitle.currentTextColor, android.graphics.PorterDuff.Mode.MULTIPLY)
                            icon.setImageDrawable(resource)
                        }

                    })
        }
    }
}