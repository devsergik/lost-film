package com.mult.film.eventDetails.adapter

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.view.ViewGroup
import com.mult.film.R
import com.mult.film.eventDetails.view.fragments.*
import com.mult.film.data.models.*

class DetailsPagerAdapter(
        fm: FragmentManager,
        val context: Context) : FragmentStatePagerAdapter(fm) {


    private val fragments = mutableMapOf<String, Fragment>()
    var eventID: Int = 0

    private var sections = calcSections()

    private fun calcSections(): MutableList<DetailSection> {
        return DetailSection.values().toMutableList()
    }

    init {
        fragments.clear()
    }

    override fun notifyDataSetChanged() {
        sections = calcSections()
        super.notifyDataSetChanged()
    }

    private val INFORMATION = DetailSection.INFORMATION.name
    private val REVIEWS = DetailSection.REVIEWS.name

    override fun getItem(position: Int): Fragment {
        return getBySection(section = sections[position])
    }

    fun getSection(position: Int) = sections[position]

    private fun getBySection(section: DetailSection): Fragment {
        return when (section) {
            DetailSection.INFORMATION -> fragments[INFORMATION] /*?:
                    fm.findFragmentByTag(getTag(DetailSection.values().indexOf(DetailSection.INFORMATION)))
                            .takeIf { it is EventInformation }
                            .also { Log.i("DPA", "${it?.toString() ?: 0}") }*/ ?:
                    //EventInformation.newInstance(eventID)
                    EventInformation.newInstance(eventID)
                            .also { fragments.put(INFORMATION, it) }

            DetailSection.REVIEWS -> fragments[REVIEWS] /*?:
                    fm.findFragmentByTag(getTag(DetailSection.values().indexOf(DetailSection.REVIEWS)))
                            .takeIf { it is Reviews }
                            .also { Log.i("DPA", "${it?.toString() ?: 0}") }*/ ?:
                    Reviews.newInstance(eventID)
                            .also { fragments.put(REVIEWS, it) }
        }
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        return try {
            super.instantiateItem(container, position)
        } catch (t: Throwable){
            t.printStackTrace()
        }
    }

    override fun getCount(): Int {
        return sections.size
    }

    fun setInformationData(eventResponse: EventResponse) {
        (getBySection(DetailSection.INFORMATION) as EventInformation)
                .setData(eventResponse)
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (sections[position]) {
            DetailSection.INFORMATION -> context.getString(R.string.information)
            DetailSection.REVIEWS -> context.getString(R.string.reviews)
        }
    }

    fun setReviews(reviews: List<VideoModel>) {
        (getBySection(DetailSection.REVIEWS) as Reviews).setReviews(reviews)
    }

}