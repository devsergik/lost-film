package com.mult.film.eventDetails.informationAdapter.delegates

import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import com.mult.film.R
import com.mult.film.common.*
import com.mult.film.common.viewUtils.px
import com.mult.film.eventDetails.informationAdapter.InformationItem
import kotlinx.android.synthetic.main.item_ico_info_subtitle_with_button.view.*
import org.jetbrains.anko.*

/**
 * Created by user on 07.12.2017.
 */
class TitleAdapterDelegate : AdapterDelegate<MutableList<InformationItem>>() {
    override fun onBindViewHolder(items: MutableList<InformationItem>, position: Int, holder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
        (holder as TitleViewHolder).bind(items[position] as InformationItem.TitleItem)
    }

    override fun isForViewType(items: MutableList<InformationItem>, position: Int): Boolean {
        return items[position] is InformationItem.TitleItem
    }

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return TitleViewHolder(
                TitleView()
                        .createView(AnkoContext.Companion.create(parent.context, parent))

        )
    }

    class TitleViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(item: InformationItem.TitleItem) {
            (itemView as TextView).text = item.title
        }
    }

    class TitleView : AnkoComponent<ViewGroup> {

        override fun createView(ui: AnkoContext<ViewGroup>): View {
            return with(ui) {
                textView("") {
                    typeface = medium
                    textColor = ContextCompat.getColor(context, R.color.primaryDarkText)
                    textSize = 16f
                }.also {
                    val lp = ViewGroup.MarginLayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT
                    )
                    with(lp) {
                        topMargin = 8.px
                        bottomMargin = 4.px
                        leftMargin = 16.px
                    }
                    it.layoutParams = lp
                }
            }
        }
    }
}