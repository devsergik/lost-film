package com.mult.film.eventDetails.view.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.youtube.player.YouTubeApiServiceUtil
import com.google.android.youtube.player.YouTubeInitializationResult
import com.mult.film.R
import com.mult.film.account.activity.SendMessageActivity
import com.mult.film.common.tryInvoke
import com.mult.film.eventDetails.adapter.ReviewAdapter
import com.mult.film.data.models.VideoModel
import com.mult.film.repositories.EventsRepository
import kotlinx.android.synthetic.main.fragment_event_news.*
import com.mult.film.eventDetails.view.VideoViewActivity
import org.jetbrains.anko.support.v4.longToast
import org.jetbrains.anko.support.v4.startActivity

/**
 * Created by user on 17.10.2017.
 */
class Reviews : Fragment() {
    companion object {
        fun newInstance(eventID: Int): Reviews {
            val fragment = Reviews()
            val args = Bundle()
            args.putInt("EVENT_ID", eventID)
            fragment.arguments = args
            return fragment
        }
    }

    private val adapter by lazy {
        ReviewAdapter(
                arguments?.getInt("EVENT_ID")?:0,
                context!!,
                { title ->
                    startActivity<VideoViewActivity>(
                            "movie" to title
                    )

                },
                {
                    startActivity<SendMessageActivity>(
                            "TYPE" to "add_video",
                            "EVENT_ID" to (arguments?.getInt("EVENT_ID") ?: -1)
                    )
                }
        )
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_event_news, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val linearLayoutManager = LinearLayoutManager(context)
        linearLayoutManager.isItemPrefetchEnabled = false
        news.layoutManager = linearLayoutManager
        news.adapter = adapter

        onViewCreated?.tryInvoke()

    }

    private var onViewCreated: (() -> Any)? = null

    fun setReviews(reviews: List<VideoModel>) {
        onViewCreated = {
            adapter.setTitles(getString(R.string.review_count, reviews.size), getString(R.string.suggest))
            adapter.setData(reviews)
            Unit
        }

        onViewCreated?.tryInvoke()
    }
}