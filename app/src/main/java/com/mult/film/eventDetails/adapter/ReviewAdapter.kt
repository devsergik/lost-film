package com.mult.film.eventDetails.adapter

import android.content.Context
import com.hannesdorfmann.adapterdelegates3.ListDelegationAdapter
import com.mult.film.R
import com.mult.film.common.Action
import com.mult.film.common.ListItem
import com.mult.film.common.uriToResource
import com.mult.film.eventDetails.adapter.delegates.ReviewAdapterDelegate
import com.mult.film.eventDetails.adapter.delegates.TitleWithButtonAdapterDelegate
import com.mult.film.data.models.VideoModel

/**
 * Created by user on 17.10.2017.
 */
class  ReviewAdapter(
        id: Int,
        val context: Context,
        showReview: (String) -> Unit,
        private val titleClick: Action) : ListDelegationAdapter<MutableList<ListItem>>() {

    init {
        items = mutableListOf()
        delegatesManager
                .addDelegate(TitleWithButtonAdapterDelegate())
                .addDelegate(ReviewAdapterDelegate(id, showReview))
    }

    private var _title = ""
    private var buttonTitle = ""
    fun setTitles(title: String, bTitle: String = "") {
        _title = title
        buttonTitle = bTitle
    }

    fun setData(data: List<VideoModel>) {
        items.clear()
        items.addAll(
                listOf(ListItem.TitleWithButtonItem(
                        _title,
                        buttonTitle = buttonTitle,
                        buttonIcon = context.uriToResource(R.drawable.ic_add_circle),
                        action = titleClick))
                        + data.map { ListItem.ReviewItem(it) }
        )
        notifyDataSetChanged()
    }


}