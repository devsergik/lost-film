package com.mult.film.eventDetails.view

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.mult.film.data.models.*

/**
 * Created by user on 15.09.2017.
 */
@StateStrategyType(AddToEndStrategy::class)
interface EventDetailView : MvpView {
    fun initAdapter()
    fun setInformation(eventResponse: EventResponse)
    fun setReviews(reviews: List<VideoModel>)
    fun setTitle(title: String)
    fun showInternetRequired()
    fun showEventName(eventName: String)
}