package com.mult.film.eventDetails.view

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.view.View
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.mult.film.eventDetails.adapter.DetailsPagerAdapter
import com.mult.film.eventDetails.presenter.EventDetailPresenter
import kotlinx.android.synthetic.main.activity_event_details.*
import me.imid.swipebacklayout.lib.Utils
import me.imid.swipebacklayout.lib.app.SwipeBackActivityBase
import me.imid.swipebacklayout.lib.app.SwipeBackActivityHelper
import com.mult.film.R
import com.mult.film.common.checkScreenSize
import com.mult.film.data.models.*

class EventDetailsActivity : MvpAppCompatActivity(), EventDetailView, /*SwipeBackLayout.SwipeBackListener,*/ SwipeBackActivityBase {


    @InjectPresenter
    lateinit var presenter: EventDetailPresenter

    private var detailsAdapter: DetailsPagerAdapter? = null

    private var sIn: Bundle? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sIn = savedInstanceState
        setContentView(R.layout.activity_event_details)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener { onBackPressed() }

        container.adapter = detailsAdapter

        tabs.tabMode = TabLayout.MODE_SCROLLABLE

        container.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
                lastPage = detailsAdapter?.getSection(position)
            }

        })

        errorGroup.visibility = View.GONE

        presenter.loadEventDetails(eventID = intent.getIntExtra("EVENT_ID", -1))

        retryButton.setOnClickListener {
            errorGroup.visibility = View.GONE
            progressBar.visibility = View.VISIBLE
            presenter.loadEventDetails(eventID = intent.getIntExtra("EVENT_ID", -1))
        }
        checkScreenSize(container)

        toolbarTitle = intent.getStringExtra("EVENT_NAME") ?: ""

        if (toolbarTitle.isNotEmpty())
            supportActionBar?.title = toolbarTitle
    }

    override fun showInternetRequired() {
        errorGroup.visibility = View.VISIBLE
        tabs.visibility = View.GONE

        errorTitle.text = getString(R.string.something_wrong)
        errorDescription.text = getString(R.string.error_description)
        errorImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.error_connection))
        progressBar.visibility = View.GONE
    }


    private var lastPage: DetailSection? = null

    //аналитика заставляет покрывать код говном
    private var isFromNotification = false

    override fun initAdapter() {
        detailsAdapter = DetailsPagerAdapter(
                supportFragmentManager,
                this)
                .apply { eventID = intent.getIntExtra("EVENT_ID", 0) }

        container.adapter = detailsAdapter

        val page = intent.getIntExtra("PAGE", -1)
        page.takeIf { it >= 0 }?.let {
            container.currentItem = it

            isFromNotification = intent.getBooleanExtra("FROM_NOTIFICATION", false)
        }

        lastPage = DetailSection.values()[page.takeIf { it > -1 } ?: 0]


        container.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))
        tabs.setupWithViewPager(container)

        progressBar.visibility = View.GONE
    }

    private var toolbarTitle = ""

    override fun setTitle(title: String) {
        toolbar.title = title
    }

    override fun setReviews(reviews: List<VideoModel>) {
        detailsAdapter?.setReviews(reviews)
    }


    override fun setInformation(eventResponse: EventResponse) {
        tabs.visibility = View.VISIBLE
        detailsAdapter?.setInformationData(eventResponse)
    }

    override fun showEventName(eventName: String) {
        toolbar.title = eventName
        toolbarTitle = eventName
    }


    private var mHelper: SwipeBackActivityHelper? = null
    override fun scrollToFinishActivity() {
        Utils.convertActivityToTranslucent(this)
        swipeBackLayout?.scrollToFinishActivity()
    }

    override fun setSwipeBackEnable(enable: Boolean) {
        getSwipeBackLayout()?.setEnableGesture(enable)
    }

    override fun getSwipeBackLayout(): me.imid.swipebacklayout.lib.SwipeBackLayout? {
        return mHelper?.swipeBackLayout
    }
}
