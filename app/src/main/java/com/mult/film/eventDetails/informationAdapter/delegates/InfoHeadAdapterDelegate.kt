package com.mult.film.eventDetails.informationAdapter.delegates

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import com.mult.film.R
import com.mult.film.common.api.RestApi
import com.mult.film.common.glide.GlideApp
import com.mult.film.common.inflate
import com.mult.film.eventDetails.informationAdapter.InformationItem
import com.mult.film.eventDetails.informationAdapter.InformationItem.HeadItem
import kotlinx.android.synthetic.main.item_ico_info_head.view.*

/**
 * Created by user on 07.12.2017.
 */
class InfoHeadAdapterDelegate : AdapterDelegate<MutableList<InformationItem>>() {
    override fun isForViewType(items: MutableList<InformationItem>, position: Int)
            = items[position] is HeadItem

    override fun onCreateViewHolder(parent: ViewGroup)
            = InformationHeadViewHolder(parent.inflate(R.layout.item_ico_info_head))

    override fun onBindViewHolder(items: MutableList<InformationItem>, position: Int, holder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
        (holder as InformationHeadViewHolder).bind((items[position] as HeadItem))
    }

    class InformationHeadViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(item: HeadItem) {
            itemView.name.text = item.name
            itemView.categoryLabel.text = item.category.name

            GlideApp.with(itemView)
                    .load(RestApi.base+"/mult/assets/images/"+item.id+"/1.png")
                    .error(R.drawable.shape_grey_rect)
                    .into(itemView.logo)
        }
    }
}