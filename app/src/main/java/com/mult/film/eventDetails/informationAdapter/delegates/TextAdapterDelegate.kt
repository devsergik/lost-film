package com.mult.film.eventDetails.informationAdapter.delegates

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import com.mult.film.R
import com.mult.film.common.light
import com.mult.film.common.medium
import com.mult.film.common.viewUtils.px
import com.mult.film.eventDetails.informationAdapter.InformationItem
import org.jetbrains.anko.AnkoComponent
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.textColor
import org.jetbrains.anko.textView

/**
 * Created by user on 07.12.2017.
 */
class TextAdapterDelegate: AdapterDelegate<MutableList<InformationItem>>() {
    override fun onBindViewHolder(items: MutableList<InformationItem>, position: Int, holder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
        (holder as TextViewHolder).bind(items[position] as InformationItem.TextItem)
    }

    override fun isForViewType(items: MutableList<InformationItem>, position: Int): Boolean {
        return items[position] is InformationItem.TextItem
    }

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return TextViewHolder(
                TextView()
                        .createView(AnkoContext.Companion.create(parent.context, parent))

        )
    }

    class TextViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(item: InformationItem.TextItem) {
            (itemView as android.widget.TextView).text = item.text
        }
    }

    class TextView : AnkoComponent<ViewGroup> {

        override fun createView(ui: AnkoContext<ViewGroup>): View {
            return with(ui) {
                textView("") {
                    typeface = light
                    textColor = ContextCompat.getColor(context, R.color.primaryDarkText)
                    textSize = 14f
                }.also {
                    val lp = ViewGroup.MarginLayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT
                    )
                    with(lp) {
                        topMargin = 8.px
                        bottomMargin = 8.px
                        leftMargin = 16.px
                        rightMargin = 16.px
                    }
                    it.layoutParams = lp
                }
            }
        }
    }
}