package com.mult.film.eventDetails.view

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.android.youtube.player.YouTubeBaseActivity
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import kotlinx.android.synthetic.main.activity_vide_view.*
import com.mult.film.R

/**
 * Created by user on 25.10.2017.
 */
class VideoViewActivity : YouTubeBaseActivity() {

    lateinit var youtubePlayerInit: YouTubePlayer.OnInitializedListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vide_view)
        var movie = intent.getStringExtra("movie")
        initUI(movie)
        youtubePlayer.initialize(getString(R.string.youtube_api_key), youtubePlayerInit)
    }

    private fun initUI(movie: String){
        youtubePlayerInit = object  : YouTubePlayer.PlayerStateChangeListener, YouTubePlayer.OnInitializedListener {
            override fun onInitializationSuccess(p0: YouTubePlayer.Provider?, youTubePlayer: YouTubePlayer?, p2: Boolean) {
                youTubePlayer?.setFullscreen(true)
                youTubePlayer?.loadVideo(movie)
            }

            override fun onInitializationFailure(p0: YouTubePlayer.Provider?, p1: YouTubeInitializationResult?) {
                Toast.makeText(applicationContext, "Something went wront!!", Toast.LENGTH_SHORT).show()
            }

            override fun onAdStarted() {
                Log.d("sdfsdfsdfsdfsd", "onAdStarted")
            }

            override fun onLoading() {
                Log.d("sdfsdfsdfsdfsd", "onLoading")
            }

            override fun onVideoStarted() {
                Log.d("sdfsdfsdfsdfsd", "onVideoStarted")
            }

            override fun onLoaded(p0: String?) {
                Log.d("sdfsdfsdfsdfsd", p0)
            }

            override fun onVideoEnded() {
                Log.d("sdfsdfsdfsdfsd", "onVideoEnded")
            }

            override fun onError(p0: YouTubePlayer.ErrorReason?) {
                Log.d("sdfsdfsdfsdfsd", "Error")
            }
        }
    }
}