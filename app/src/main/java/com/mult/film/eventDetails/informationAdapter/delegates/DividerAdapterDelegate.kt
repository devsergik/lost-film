package com.mult.film.eventDetails.informationAdapter.delegates

import android.graphics.drawable.ColorDrawable
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import com.mult.film.R
import com.mult.film.common.viewUtils.px
import com.mult.film.eventDetails.informationAdapter.InformationItem
import org.jetbrains.anko.AnkoComponent
import org.jetbrains.anko.AnkoContext
import org.jetbrains.anko.view

/**
 * Created by user on 08.12.2017.
 */
class DividerAdapterDelegate : AdapterDelegate<MutableList<InformationItem>>() {
    override fun onCreateViewHolder(parent: ViewGroup) =
            //ShortInfoViewHolder(parent.inflate(R.layout.item_ico_info_short))
            DividerViewHolder(
                    DividerView().createView(AnkoContext.Companion.create(parent.context, parent)))

    override fun isForViewType(items: MutableList<InformationItem>, position: Int) =
            items[position] is InformationItem.DividerItem

    override fun onBindViewHolder(items: MutableList<InformationItem>, position: Int, holder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
    }

    class DividerViewHolder(view: View) : RecyclerView.ViewHolder(view)

    class DividerView : AnkoComponent<ViewGroup> {
        override fun createView(ui: AnkoContext<ViewGroup>): View {
            return with(ui) {
                view {
                    background = ColorDrawable(ContextCompat.getColor(context, R.color.menu_divider))
                }.also {
                    val lp = ViewGroup.MarginLayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT, 1.px
                    )
                    with(lp) {
                        /*topMargin = 8.px
                        bottomMargin = 8.px*/
                        leftMargin = 16.px
                        rightMargin = 16.px
                    }
                    it.layoutParams = lp
                }
            }
        }

    }
}