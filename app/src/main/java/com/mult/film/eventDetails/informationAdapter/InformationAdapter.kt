package com.mult.film.eventDetails.informationAdapter

import com.hannesdorfmann.adapterdelegates3.ListDelegationAdapter
import com.mult.film.eventDetails.informationAdapter.delegates.*

/**
 * Created by user on 07.12.2017.
 */
class InformationAdapter : ListDelegationAdapter<MutableList<InformationItem>>() {

    init {
        items = mutableListOf()

        delegatesManager
                .addDelegate(InfoButtonsAdapterDelegate())
                .addDelegate(InfoHeadAdapterDelegate())
                .addDelegate(TextAdapterDelegate())
                .addDelegate(TitleAdapterDelegate())
                .addDelegate(DividerAdapterDelegate())
    }

    fun setData(data: List<InformationItem>) {
        items.clear()
        items.addAll(data)
        notifyDataSetChanged()
    }


    fun followActivated(checked: Boolean) {
        items.find { it is InformationItem.ButtonsItem }.let {
            (it as InformationItem.ButtonsItem).followActivated = checked
            notifyItemChanged(items.indexOf(it))
        }
    }

}