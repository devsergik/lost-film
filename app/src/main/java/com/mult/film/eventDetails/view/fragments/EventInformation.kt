package com.mult.film.eventDetails.view.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.mult.film.R
import com.mult.film.account.activity.SendMessageActivity
import com.mult.film.common.*
import com.mult.film.dialogs.AuthRequiredDialog
import com.mult.film.eventDetails.informationAdapter.InformationAdapter
import com.mult.film.eventDetails.informationAdapter.InformationItem
import com.mult.film.data.models.*
import com.mult.film.repositories.EventsRepository
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_event_information_r.*
import org.jetbrains.anko.support.v4.longToast
import org.jetbrains.anko.support.v4.startActivity


/**
 * Created by user on 07.12.2017.
 */
class EventInformation : Fragment() {

    var eventID = 0
    private val disposable = CompositeDisposable()

    private var onViewCreated: (() -> Unit)? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        eventID = arguments?.getInt("EVENT_ID") ?: 0
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?) =
            inflater.inflate(R.layout.fragment_event_information_r, container, false)
                    .also {
                        retainInstance = true
                    }


    companion object {
        fun newInstance(eventID: Int): EventInformation {
            val fragment = EventInformation()
            val args = Bundle()
            args.putInt("EVENT_ID", eventID)
            fragment.arguments = args
            return fragment
        }
    }

    private val adapter by lazy {
        InformationAdapter()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onViewCreated?.invoke()
        recycler.layoutManager = LinearLayoutManager(context)
        recycler.adapter = adapter
    }

    override fun onDestroyView() {
        super.onDestroyView()
        disposable.clear()
    }

    fun setData(eventResponse: EventResponse) {
        onViewCreated = {
            var information = eventResponse.information
            adapter.setData(listOf(
                    InformationItem.HeadItem(
                            eventResponse.movies.get(0).id?: 0,
                            information.title ?: "",
                            information.category ?: CategorySetting()
                    ),
                    InformationItem.ButtonsItem(
                            followActivated = eventResponse.id.favorite,
                            onFollowClick = {
                                val follow = it as ImageView
                                val checked = !follow.isActivated
                                if (!follow.isActivated) {
                                    if (UserInfo.registered) {
                                        adapter.followActivated(checked)
                                        EventsRepository.changeLikeStatus(eventID, checked){
                                            longToast(if(it) R.string.ico_added_to_favorite else R.string.network_dialog_title)
                                        }
                                    } else {
                                        showAuthRequired()
                                    }
                                } else {
                                    adapter.followActivated(checked)
                                    EventsRepository.changeLikeStatus(eventID, checked){
                                        longToast(if(it) R.string.ico_removed_from_favorite else R.string.network_dialog_title)
                                    }
                                }
                            },
                            onShareClick = {
                                context?.shareText(
                                        "${information.title}\n" +
                                                "${getString(R.string.text_source)}: http://icomarket.io/"
                                )
                            }
                    ),
                    InformationItem.TitleItem(getString(R.string.full_description))) +
                    (if (!information.desc.isNullOrEmpty()) listOf(
                            InformationItem.TextItem(information.desc!!))
                    else emptyList())
            )
        }



        onViewCreated?.tryInvoke()
    }
    private fun showAuthRequired(type: String = "favorite") {
        AuthRequiredDialog().apply {
            arguments = Bundle().apply {
                putString("types", type)
            }
        }.showAllowingStateLoss(childFragmentManager, "")
    }

}


