package com.mult.film.eventDetails.informationAdapter.delegates

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import com.mult.film.R
import com.mult.film.common.UserInfo
import com.mult.film.common.inflate
import com.mult.film.common.setBackgroundTintWithCornersSate
import com.mult.film.eventDetails.informationAdapter.InformationItem
import com.mult.film.eventDetails.informationAdapter.InformationItem.ButtonsItem
import kotlinx.android.synthetic.main.item_ico_info_buttons.view.*

/**
 * Created by user on 07.12.2017.
 */
class InfoButtonsAdapterDelegate : AdapterDelegate<MutableList<InformationItem>>() {
    override fun onCreateViewHolder(parent: ViewGroup) =
            InfoButtonsViewHolder(parent.inflate(R.layout.item_ico_info_buttons))

    override fun isForViewType(items: MutableList<InformationItem>, position: Int) =
            items[position] is ButtonsItem

    override fun onBindViewHolder(items: MutableList<InformationItem>, position: Int, holder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
        (holder as InfoButtonsViewHolder).bind(items[position] as ButtonsItem)
    }

    class InfoButtonsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(buttons: ButtonsItem) {
            itemView.follow.setOnClickListener { buttons.onFollowClick(it) }
            itemView.follow.isActivated = buttons.followActivated

            itemView.share.setOnClickListener { buttons.onShareClick(it) }

        }
    }
}