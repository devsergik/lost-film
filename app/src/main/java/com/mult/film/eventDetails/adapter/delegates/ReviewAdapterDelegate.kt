package com.mult.film.eventDetails.adapter.delegates

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.load.MultiTransformation
import com.bumptech.glide.request.RequestOptions
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import com.mult.film.R
import com.mult.film.common.ListItem
import com.mult.film.common.glide.GlideApp
import com.mult.film.data.models.VideoModel
import jp.wasabeef.glide.transformations.BlurTransformation
import jp.wasabeef.glide.transformations.ColorFilterTransformation
import kotlinx.android.synthetic.main.fragment_category_filter.view.*
import com.mult.film.common.api.RestApi
import org.jetbrains.anko.find
import org.jetbrains.anko.include
import org.jetbrains.anko.layoutInflater
import java.util.*

/**
 * Created by user on 03.11.2017.
 */
class ReviewAdapterDelegate(val id: Int, private val showReview: (String) -> Unit) : AdapterDelegate<MutableList<ListItem>>() {    override fun isForViewType(items: MutableList<ListItem>, position: Int)
            = items[position] is ListItem.ReviewItem

    override fun onBindViewHolder(items: MutableList<ListItem>, position: Int, holder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
        (holder as ReviewHolder).bind(id, (items[position] as ListItem.ReviewItem).reviewItem)
    }

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return ReviewHolder(parent.context.layoutInflater.inflate(R.layout.item_review, parent, false), showReview)
    }

    class ReviewHolder(view: View, private val showReview: (String) -> Unit) : RecyclerView.ViewHolder(view) {
        private val title = view.find<TextView>(R.id.title)
        private val preview = view.find<ImageView>(R.id.preview)


        fun bind(id: Int, review: VideoModel ) {
            if (review.movie!!.isNotEmpty()) {
                GlideApp.with(itemView.context)
                        .load(RestApi.base+"/mult/assets/images/"+review.id+"/1.png")
                        .error(R.drawable.ic_none_big)
                        .apply(RequestOptions.bitmapTransform(MultiTransformation(
                                BlurTransformation(2, 1),
                                ColorFilterTransformation(ContextCompat.getColor(itemView.context, R.color.primaryDarkAlpha)))
                        ))
                        .into(preview)
            } else
                preview.visibility = View.GONE

         //   val dateStr = if (review.datetime!!.toInt() != 0) " - " + dateFormatter.format(Date(review.datetime!!.toLong() * 1000)) else ""
            //sourceAndDate.text = review.channel_name + dateStr

            title.text = review.title

            itemView.setOnClickListener {
                // showReview(count.articleId, count.types, count.link)
                showReview(review.movie.toString())
            }
        }
    }
}