package com.mult.film.eventDetails.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.mult.film.eventDetails.view.EventDetailView
import com.mult.film.repositories.EventsRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.lang.Exception

/**
 * Created by user on 15.09.2017.
 */
@InjectViewState
class EventDetailPresenter : MvpPresenter<EventDetailView>() {

    fun loadEventDetails(eventID: Int) {
        EventsRepository.loadEventInfo(eventID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    try {
                        viewState.initAdapter()

                        viewState.setInformation(it)

                        viewState.setReviews(reviews = it.movies)
                    } catch (exception: Exception){
                        viewState.showInternetRequired()
                    }
                }, {
                    it.printStackTrace()
                    viewState.showInternetRequired()
                })
    }
}