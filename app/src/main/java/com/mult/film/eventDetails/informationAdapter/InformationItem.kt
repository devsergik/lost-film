package com.mult.film.eventDetails.informationAdapter

import com.mult.film.common.Action
import com.mult.film.common.ViewAction
import com.mult.film.data.models.CategorySetting

/**
 * Created by user on 07.12.2017.
 */
sealed class InformationItem {
    class HeadItem(
            val id: Int,
            val name: String,
            val category: CategorySetting
    ) : InformationItem()

    class ButtonsItem(
            var followActivated: Boolean,
            val onFollowClick: ViewAction = {},
            val onShareClick: ViewAction = {}
    ) : InformationItem()

    class TitleItem(val title: String) : InformationItem()
    class TextItem(val text: String, val prettify: Boolean = false) : InformationItem()
    class SingleButtonItem(val title: String, val action: Action = {}) : InformationItem()
    class DividerItem : InformationItem()
}